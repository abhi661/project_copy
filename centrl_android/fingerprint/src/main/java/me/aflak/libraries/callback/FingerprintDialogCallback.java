package me.aflak.libraries.callback;

public interface FingerprintDialogCallback {
    void onAuthenticationSucceeded();
    void onAuthenticationCancel();
}
