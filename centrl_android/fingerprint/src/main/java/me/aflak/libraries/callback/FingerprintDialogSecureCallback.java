package me.aflak.libraries.callback;

import me.aflak.libraries.utils.FingerprintToken;

public interface FingerprintDialogSecureCallback {
    void onAuthenticationSucceeded();
    void onAuthenticationCancel();
    void onNewFingerprintEnrolled(FingerprintToken token);
}
