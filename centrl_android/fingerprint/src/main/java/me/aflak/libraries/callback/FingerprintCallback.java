package me.aflak.libraries.callback;

public interface FingerprintCallback {
    void onAuthenticationSucceeded();
    void onAuthenticationFailed();
    void onAuthenticationError(int errorCode, String error);
    void onAuthenticationHelp(int errorCode, String error);
}
