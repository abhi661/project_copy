package com.android.centrl.ui.dialog

import android.Manifest
import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.graphics.Typeface
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.Settings
import android.telephony.TelephonyManager
import android.text.InputType
import android.view.View
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import com.android.centrl.R
import com.android.centrl.adapter.CustomSpinnerAdapter
import com.android.centrl.databinding.DialogAddChannelBinding
import com.android.centrl.notificationservice.NotificationData
import com.android.centrl.persistence.entity.GroupChannelsEntity
import com.android.centrl.persistence.repositry.ChatsRepository
import com.android.centrl.persistence.repositry.GroupChannelsRepository
import com.android.centrl.utils.AppConstant
import com.android.centrl.utils.CentrlSnackBar
import com.android.centrl.utils.CentrlUtil
import com.android.centrl.utils.LogUtil
import com.google.i18n.phonenumbers.NumberParseException
import com.google.i18n.phonenumbers.PhoneNumberUtil
import com.google.i18n.phonenumbers.Phonenumber.PhoneNumber
import java.util.*

/**
 * Add new channels dialog
 */
class DialogAddNewChannels : DialogFragment() {

    private var dialogAddIdentityListener: DialogAddChannelsListener? = null
    /**
     * Add new channels interface
     */
    interface DialogAddChannelsListener {
        fun onAddIdentity(groupKey: String, packCode: Int)
    }

    private val mIdentityNameList = arrayListOf(
        "Please select channel",
        "Discord",
        "Hangouts",
        "ICQ New",
        "Instagram",
        "KakaoTalk",
        "Kik",
        "Line",
        "Messenger",
        "QQ",
        "Reddit",
        "SMS",
        "Skype",
        "Slack",
        "Telegram",
        "Viber",
        "WeChat",
        "WhatsApp"
    )
    private val mIdentityMailNameList = arrayListOf("Please select channel", "Gmail", "Outlook")

    private lateinit var binding: DialogAddChannelBinding
    private var groupChannelsRepository: GroupChannelsRepository? = null
    private var chatsRepository: ChatsRepository? = null
    private var userName: String? = null
    private lateinit var groupKey: String
    private lateinit var mChannelName: String
    private var packCode = 0
    private var isChatMSG = false
    private var mCountryCode: String? = null
    private var mCountryID: String? = null
    private var notiType: String? = null

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = Dialog(requireActivity(), R.style.DialogBlackTransparent)
        binding = DataBindingUtil.inflate(dialog.layoutInflater, R.layout.dialog_add_channel, null, false)
        dialog.setContentView(binding.root)
        groupKey = requireArguments().getString("group_key").toString()
        isChatMSG = requireArguments().getBoolean("ischat")
        groupChannelsRepository = GroupChannelsRepository(requireActivity())
        chatsRepository = ChatsRepository(requireActivity())

        binding.snackbarLL.snackbarCl.bringToFront()
        binding.countryCodePicker.visibility = View.GONE

        val manager = requireActivity().getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager
        mCountryID = manager.simCountryIso.toUpperCase(Locale.ROOT)
        mCountryCode = binding.countryCodePicker.selectedCountryCodeWithPlus
        binding.countryCodePicker.setOnCountryChangeListener {
            mCountryCode = binding.countryCodePicker.selectedCountryCodeWithPlus
        }
        if (isChatMSG) {
            notiType = AppConstant.CHAT_SCREEN
            val spinnerArrayAdapter = ArrayAdapter(requireActivity(), android.R.layout.simple_spinner_item, mIdentityNameList)
            spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item) // The drop down view
            binding.spinnerIdentityType.adapter = CustomSpinnerAdapter(requireActivity(), mIdentityNameList)
        } else {
            notiType = AppConstant.MAIL_SCREEN
            val spinnerArrayAdapter = ArrayAdapter(requireActivity(), android.R.layout.simple_spinner_item, mIdentityMailNameList)
            spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item) // The drop down view
            binding.spinnerIdentityType.adapter = CustomSpinnerAdapter(requireActivity(), mIdentityMailNameList)
        }
        binding.spinnerIdentityType.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>?, view: View, position: Int, id: Long) {
                try {
                    if (isChatMSG) {
                        mChannelName = mIdentityNameList[position]
                    } else {
                        mChannelName = mIdentityMailNameList[position]
                    }

                    if (mChannelName != resources.getString(R.string.channel_select)) {
                        binding.spinnerTitle.visibility = View.VISIBLE
                        if (mChannelName == resources.getString(R.string.sms)) {
                            binding.countryCodePicker.visibility = View.VISIBLE
                            binding.userNameIl.hint = resources.getString(R.string.channel_phone)
                            binding.edtIdentityUserName.text!!.clear()
                            binding.edtIdentityUserName.setTypeface(null, Typeface.BOLD)
                            binding.edtIdentityUserName.inputType = InputType.TYPE_CLASS_PHONE
                        } else {
                            binding.countryCodePicker.visibility = View.GONE
                            binding.userNameIl.hint = resources.getString(R.string.channel_uname)
                            binding.edtIdentityUserName.text!!.clear()
                            binding.edtIdentityUserName.setTypeface(null, Typeface.BOLD)
                            binding.edtIdentityUserName.inputType = InputType.TYPE_CLASS_TEXT
                        }
                        if (CentrlUtil.getChannelsPackageCodeHashMap()[mChannelName] != null) {
                            packCode = CentrlUtil.getChannelsPackageCodeHashMap()[mChannelName]!!
                        }
                    } else {
                        binding.spinnerTitle.visibility = View.GONE
                        binding.edtIdentityUserName.text!!.clear()
                        binding.edtIdentityUserName.clearFocus()
                        binding.userNameIl.hint = resources.getString(R.string.channel_uname)
                        binding.countryCodePicker.visibility = View.GONE
                        binding.edtIdentityUserName.setTypeface(null, Typeface.NORMAL)
                        binding.edtIdentityUserName.inputType = InputType.TYPE_CLASS_TEXT
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
                packCode = 0
            }
        }
        binding.closeAddIdentity.setOnClickListener { dismiss() }

        binding.btnAddIdentity.setOnClickListener(View.OnClickListener {
            val rand = Random()
            val randomId = rand.nextInt(100000)
            var iconPath = ""
            val mEditTextString = binding.edtIdentityUserName.text.toString()
            if (mChannelName.equals(resources.getString(R.string.channel_select), ignoreCase = true)) {
                CentrlSnackBar.showSnackBar(binding.snackbarLL.snackbarCl, resources.getString(R.string.channel_select_error))
                return@OnClickListener
            }
            if (mEditTextString.isEmpty()) {
                if (packCode == AppConstant.SMS_CODE) {
                    CentrlSnackBar.showSnackBar(binding.snackbarLL.snackbarCl, resources.getString(R.string.channel_number_enter))
                } else {
                    CentrlSnackBar.showSnackBar(binding.snackbarLL.snackbarCl, resources.getString(R.string.channel_uname_enter))
                }
            } else if (packCode == AppConstant.SMS_CODE) {
                userName = "$mCountryCode $mEditTextString"
                LogUtil.mLOGE("SMS number...",""+userName)
                if(!checkPermission()){
                    requestContacts()
                    return@OnClickListener
                }
                if (!validatePhoneNumberWithCode(mCountryCode, mEditTextString)) {
                    CentrlSnackBar.showSnackBar(binding.snackbarLL.snackbarCl, resources.getString(R.string.channel_enter_correct))
                    return@OnClickListener
                }
                val groupDetails = groupChannelsRepository!!.getGroupDetails(groupKey)
                val contact = CentrlUtil.getContactDetailsByNumber(userName, activity)
                if (contact != null) {
                    userName = contact.name
                    iconPath = if(contact.profilePath != null){
                        CentrlUtil.getContactImage(activity, contact.profilePath, contact.id)
                    } else {
                        ""
                    }
                }
                val uniKey = NotificationData.md5("${userName!!.toLowerCase(Locale.getDefault())}@0$packCode")
                val groupTitle = groupChannelsRepository!!.getGroupTitle(groupKey, packCode)
                if (groupTitle.packageCode == packCode) {
                    CentrlSnackBar.showSnackBar(binding.snackbarLL.snackbarCl, resources.getString(R.string.channel_already_added))
                    return@OnClickListener
                } else {
                    val groupChat =
                        GroupChannelsEntity(
                            randomId,
                            groupKey,
                            uniKey,
                            userName!!,
                            packCode,
                            iconPath,
                            groupDetails.groupIconPath,
                            groupDetails.groupTitle,
                            mEditTextString,
                        0,
                            notiType!!,
                            0,
                            0
                        )
                    val getChats = groupChannelsRepository!!.getGroupKey(uniKey, packCode)
                    if (getChats.chatKey.isEmpty()) {
                        groupChannelsRepository!!.insertGroup(groupChat)
                    } else {
                        groupChannelsRepository!!.updateGroup(uniKey, groupKey, iconPath, groupDetails.groupIconPath, groupDetails.groupTitle, userName!!)
                        chatsRepository!!.updateChat(uniKey, groupKey)
                    }
                    dialogAddIdentityListener!!.onAddIdentity(groupKey, packCode)
                    dismiss()
                }
            } else {
                val groupDetails = groupChannelsRepository!!.getGroupDetails(groupKey)
                userName = mEditTextString
                val uniKey = NotificationData.md5("${mEditTextString.toLowerCase(Locale.getDefault())}@0$packCode")
                val groupTitle = groupChannelsRepository!!.getGroupTitle(groupKey, packCode)
                if (groupTitle.packageCode == packCode) {
                    CentrlSnackBar.showSnackBar(binding.snackbarLL.snackbarCl, resources.getString(R.string.channel_already_added))
                    return@OnClickListener
                } else {
                    val groupChat =
                        GroupChannelsEntity(
                            randomId,
                            groupKey,
                            uniKey,
                            userName!!,
                            packCode,
                            iconPath,
                            groupDetails.groupIconPath,
                            groupDetails.groupTitle,
                            groupDetails.number,
                            0,
                            notiType!!,
                            0,
                            0
                        )
                    val getChats = groupChannelsRepository!!.getGroupKey(uniKey, packCode)
                    if (getChats.chatKey.isEmpty()) {
                        groupChannelsRepository!!.insertGroup(groupChat)
                    } else {
                        groupChannelsRepository!!.updateGroup(uniKey, groupKey, iconPath, groupDetails.groupIconPath, groupDetails.groupTitle, userName!!)
                        chatsRepository!!.updateChat(uniKey, groupKey)
                    }
                    dialogAddIdentityListener!!.onAddIdentity(groupKey, packCode)
                    dismiss()
                }
            }
        })
        return dialog
    }
    /**
     * Validate the phone number
     */
    private fun validatePhoneNumberWithCode(countryCode: String?, phNumber: String): Boolean {
        val phoneNumberUtil = PhoneNumberUtil.getInstance()
        val isoCode = phoneNumberUtil.getRegionCodeForCountryCode(countryCode!!.toInt())
        var phoneNumber: PhoneNumber? = null
        try {
            phoneNumber = phoneNumberUtil.parse(phNumber, isoCode)
        } catch (e: NumberParseException) {
            e.printStackTrace()
        }
        if(phoneNumber == null){
            return false
        } else{
            return phoneNumberUtil.isValidNumber(phoneNumber)
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        dialogAddIdentityListener = activity as DialogAddChannelsListener?
    }

    private fun checkPermission(): Boolean {
        return Build.VERSION.SDK_INT < Build.VERSION_CODES.M || requireActivity().checkSelfPermission(
            Manifest.permission.READ_CONTACTS) == PackageManager.PERMISSION_GRANTED
    }

    private fun requestContacts() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && requireActivity().checkSelfPermission(
                Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(
                    requireActivity(),
                    Manifest.permission.READ_CONTACTS
                )
            ) {
                // sees the explanation, try again to request the permission.
                val builder = AlertDialog.Builder(requireActivity())
                builder.setTitle(resources.getString(R.string.permission))
                builder.setMessage(resources.getString(R.string.contact_txt))
                builder.setCancelable(false)
                builder.setPositiveButton(resources.getString(R.string.continue_txt)) { _: DialogInterface?, _: Int ->
                    openPermissionSettings(PERMISSIONS_REQUEST_READ_CONTACTS)
                }
                val dialog = builder.create()
                dialog.show()
                dialog.getButton(AlertDialog.BUTTON_POSITIVE).isAllCaps = false
                dialog.getButton(AlertDialog.BUTTON_NEGATIVE).isAllCaps = false
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    dialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(resources.getColor(R.color.continue_color, null))
                } else {
                    dialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(Color.parseColor("#4FC3B8"))
                }
            } else {
                requestPermissions(arrayOf(Manifest.permission.READ_CONTACTS, Manifest.permission.WRITE_CONTACTS),
                    PERMISSIONS_REQUEST_READ_CONTACTS
                )
            }
        }
    }

    private fun openPermissionSettings(requestCode: Int) {
        if (requestCode == PERMISSIONS_REQUEST_READ_CONTACTS) {
            val intent =
                Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
            val uri = Uri.parse("package:" + requireActivity().packageName)
            intent.data = uri
            if (intent.resolveActivity(requireActivity().packageManager) != null) {
                requireActivity().startActivity(intent)
            }
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        if (requestCode == PERMISSIONS_REQUEST_READ_CONTACTS && checkPermission()) {
            //
        }
    }


    companion object {
        var TAG = "DialogAddNewIdentity"
        private const val PERMISSIONS_REQUEST_READ_CONTACTS = 100
    }
}