package com.android.centrl.persistence.repositry

import android.content.Context
import com.android.centrl.persistence.dao.GroupChannelsDao
import com.android.centrl.persistence.database.CentrlDatabase
import com.android.centrl.persistence.entity.GroupChannelsEntity
import io.reactivex.Completable
import io.reactivex.CompletableObserver
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import org.jetbrains.anko.doAsyncResult

/**
 * Access point for managing group details.
 */
class GroupChannelsRepository(context: Context) {

    private val groupChannelsDao: GroupChannelsDao

    init {
        val centrlDatabase = CentrlDatabase.getInstance(context)
        groupChannelsDao = centrlDatabase.groupChannelsDao()
    }

    /**
     * @param groupChannelsEntity the groups details to be inserted.
     */
    fun insertGroup(groupChannelsEntity: GroupChannelsEntity) {
        try {
            Completable.fromAction {
                groupChannelsDao.insertGroup(groupChannelsEntity)
            }.observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(object : CompletableObserver {
                    override fun onSubscribe(d: Disposable) {}
                    override fun onComplete() {}
                    override fun onError(e: Throwable) {}
                })
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    /**
     * update the groups details.
     */
    fun updateGroup(
        chatKey: String,
        groupId: String,
        notiIcon: String,
        groupIcon: String,
        groupTitle: String,
        title: String
    ) {
        try {
            Completable.fromAction {
                groupChannelsDao.updateGroup(chatKey, groupId, notiIcon, groupIcon, groupTitle, title)
            }.observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(object : CompletableObserver {
                    override fun onSubscribe(d: Disposable) {}
                    override fun onComplete() {}
                    override fun onError(e: Throwable) {}
                })
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    /**
     * update the icons details.
     */
    fun updateIcon(groupId: String, notiIcon: String, groupIcon: String, groupTitle: String, title: String) {
        try {
            Completable.fromAction {
                groupChannelsDao.updateIcon(groupId, notiIcon, groupIcon, groupTitle, title)
            }.observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(object : CompletableObserver {
                    override fun onSubscribe(d: Disposable) {}
                    override fun onComplete() {}
                    override fun onError(e: Throwable) {}
                })
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    /**
     * merge the groups.
     */
    fun mergeGroup(groupId: String, groupTitle: String, chatKey: String) {
        try {
            Completable.fromAction {
                groupChannelsDao.mergeGroup(groupId, groupTitle, chatKey)
            }.observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(object : CompletableObserver {
                    override fun onSubscribe(d: Disposable) {}
                    override fun onComplete() {}
                    override fun onError(e: Throwable) {}
                })
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    /**
     * update the mute in groups.
     */
    fun updateMuteChat(muteChat: Int, groupId: String) {
        try {
            Completable.fromAction {
                groupChannelsDao.updateMuteChat(muteChat, groupId)
            }.observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(object : CompletableObserver {
                    override fun onSubscribe(d: Disposable) {}
                    override fun onComplete() {}
                    override fun onError(e: Throwable) {}
                })
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    /**
     * update group profile.
     */
    fun updateEditProfileGroup(groupId: String, groupTitle: String, groupIcon: String) {
        try {
            Completable.fromAction {
                groupChannelsDao.updateEditProfileGroup(groupId, groupTitle, groupIcon)
            }.observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(object : CompletableObserver {
                    override fun onSubscribe(d: Disposable) {}
                    override fun onComplete() {}
                    override fun onError(e: Throwable) {}
                })
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    /**
     * update single profile.
     */
    fun updateEditSingleProfile(
        groupId: String,
        groupTitle: String,
        groupIcon: String,
        iconPath: String,
        isEdit: Int
    ) {
        try {
            Completable.fromAction {
                groupChannelsDao.updateEditSingleProfile(
                    groupId,
                    groupTitle,
                    groupIcon,
                    iconPath,
                    isEdit
                )
            }.observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(object : CompletableObserver {
                    override fun onSubscribe(d: Disposable) {}
                    override fun onComplete() {}
                    override fun onError(e: Throwable) {}
                })
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    /**
     * update single profile.
     */
    fun updateTitleGroup(title: String, groupId: String, chatKey: String, phoneNumber: String) {
        try {
            Completable.fromAction {
                groupChannelsDao.updateTitleGroup(title, groupId, chatKey, phoneNumber)
            }.observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(object : CompletableObserver {
                    override fun onSubscribe(d: Disposable) {}
                    override fun onComplete() {}
                    override fun onError(e: Throwable) {}
                })
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    /**
     * @return the chatkey from the table
     */
    fun getChatKey(chatKey: String): String {
        return getChatKeyAsyncTask(chatKey)
    }

    /**
     * @return the edited profile picture from table
     */
    fun getIsEdited(groupId: String): Int {
        return getIsEditedAsyncTask(groupId)
    }

    /**
     * @return the reply chatkey from the table
     */
    fun getReplyChatKey(groupId: String, packageCode: Int): String {
        return getReplyChatKeyAsyncTask(packageCode, groupId)
    }

    /**
     * @return the group key from the table
     */
    fun getGroupKey(chatKey: String, packageCode: Int): GroupChannelsEntity {
        return getGroupKeyAsyncTask(packageCode, chatKey)
    }

    /**
     * @return the group title from the table
     */
    fun getGroupTitle(groupId: String, packageCode: Int): GroupChannelsEntity {
        return getGroupTitleAsyncTask(packageCode, groupId)
    }

    /**
     * @return the group details from the table
     */
    fun getGroupDetails(groupId: String): GroupChannelsEntity {
        return getGroupDetailsAsyncTask(groupId)
    }

    /**
     * @return the all group details from the table
     */
    fun getGroupList(groupId: String): ArrayList<GroupChannelsEntity> {
        return getAllGroupListAsyncTask(groupId)
    }

    /**
     * Delete group details from table.
     */
    fun deleteGroup(groupId: String) {
        try {
            Completable.fromAction {
                groupChannelsDao.deleteGroup(groupId)
            }.observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(object : CompletableObserver {
                    override fun onSubscribe(d: Disposable) {}
                    override fun onComplete() {}
                    override fun onError(e: Throwable) {}
                })
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    /**
     * AsyncTask for get chat key from database
     */
    private fun getChatKeyAsyncTask(groupId: String): String {
        val result= doAsyncResult(null,{
            groupChannelsDao.getChatKey(groupId)
        })
        if(result.get() == null){
            return ""
        }
        return result.get()
    }

    /**
     * AsyncTask for get edited profile picture from database
     */
    private fun getIsEditedAsyncTask(groupId: String): Int {
        val result = doAsyncResult(null,{
            groupChannelsDao.getIsEdited(groupId)
        })
        if(result.get() == null){
            return 0
        }
        return result.get()
    }

    /**
     * AsyncTask for get reply chat key from database
     */

    private fun getReplyChatKeyAsyncTask(packCode: Int, groupId: String): String {
        val result = doAsyncResult(null,{
            groupChannelsDao.getReplyChatKey(groupId, packCode)
        })
        if(result.get() == null){
            return ""
        }
        return result.get()
    }

    /**
     * AsyncTask for get group key from database
     */
    private fun getGroupKeyAsyncTask(packCode: Int, chatKey: String): GroupChannelsEntity {
        val groupChannelsEntity = doAsyncResult(null,{
            groupChannelsDao.getGroupKey(chatKey, packCode)
        })
        if(groupChannelsEntity.get() == null){
           return GroupChannelsEntity(0, "", "", "", 0, "", "", "", "", 0, "", 0, 0)
        }
        return groupChannelsEntity.get()
    }

    /**
     * AsyncTask for get group title from database
     */
    private fun getGroupTitleAsyncTask(packCode: Int, groupId: String): GroupChannelsEntity {
        val groupChannelsEntity = doAsyncResult(null,{
            groupChannelsDao.getGroupTitle(groupId, packCode)
        })
        if(groupChannelsEntity.get() == null){
            return GroupChannelsEntity(0, "", "", "", 0, "", "", "", "", 0, "", 0, 0)
        }
        return groupChannelsEntity.get()
    }

    /**
     * AsyncTask for get group details from database
     */
    private fun getGroupDetailsAsyncTask(groupId: String): GroupChannelsEntity {
        val groupChannelsEntity = doAsyncResult(null,{
            groupChannelsDao.getGroupDetails(groupId)
        })
        if(groupChannelsEntity.get() == null){
            return GroupChannelsEntity(0, "", "", "", 0, "", "", "", "", 0, "", 0, 0)
        }
        return groupChannelsEntity.get()
    }

    /**
     * AsyncTask for get group details from database
     */
    private fun getAllGroupListAsyncTask(groupId: String): ArrayList<GroupChannelsEntity> {
        val result = doAsyncResult(null,{
            groupChannelsDao.getGroupList(groupId) as ArrayList<GroupChannelsEntity>
        })
        return result.get()
    }
}