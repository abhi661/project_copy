package com.android.centrl.models

import com.google.android.gms.ads.formats.UnifiedNativeAd

class AdsObject : ListObject() {
    lateinit var ads: UnifiedNativeAd

    override fun getType(userId: Int): Int {
        return TYPE_ADS
    }
}