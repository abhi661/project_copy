package com.android.centrl.ui.fragment

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.centrl.R
import com.android.centrl.adapter.ChatContactListAdapter
import com.android.centrl.databinding.FragmentChatBinding
import com.android.centrl.interfaces.ChatContactSelectedListener
import com.android.centrl.interfaces.MultiSelectChatList
import com.android.centrl.interfaces.RecyclerViewClickListener
import com.android.centrl.persistence.database.CentrlDatabase
import com.android.centrl.persistence.entity.ChatsEntity
import com.android.centrl.ui.activity.ChatsRoomActivity
import com.android.centrl.ui.views.RecyclerViewTouchListener
import com.android.centrl.utils.*
import com.android.centrl.viewmodel.SearchViewModel
import com.google.android.gms.ads.AdView
import kotlin.collections.ArrayList

/**
 * Chat Listing fragment
 */
class ChatFragment : BaseFragment(), ChatContactListAdapter.FilterSearchInterface {

    companion object {
        private const val chatScreen = "chat"
    }

    private lateinit var binding: FragmentChatBinding
    private var multiSelectChatList: MultiSelectChatList? = null
    private var chatContactListAdapter: ChatContactListAdapter? = null
    private var chatContactSelectedListener: ChatContactSelectedListener? = null
    private var mChatList = ArrayList<ChatsEntity>()
    private var mChatBadgeGroupList = ArrayList<String>()
    private var notificationReceiver: NotificationReceiverFragment? = null
    private lateinit var appDatabase: CentrlDatabase

    private var mAdView: AdView? = null
    private var mContext: Context? = null

    private var isMultiSelect = false
    private var isCheckboxMultiSelect = false
    private var isBlockedContactSelected = false
    private var isSameChannelSelected = false
    private var isGroupChatSelected = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        appDatabase = CentrlDatabase.getInstance(requireActivity())
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater,R.layout.fragment_chat, container, false)
        val root: View = binding.root
        binding.noResultFoundTxt.visibility = View.GONE

        val linearLayoutManager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
        val divider = DividerItemDecoration( binding.chatListRecyclerView.context, DividerItemDecoration.VERTICAL)
        divider.setDrawable(ContextCompat.getDrawable(requireContext(), R.drawable.layer)!!)
        binding.chatListRecyclerView.addItemDecoration(divider)
        binding.chatListRecyclerView.layoutManager = linearLayoutManager
        chatContactListAdapter = ChatContactListAdapter(requireActivity(), mChatList, this)
        binding.chatListRecyclerView.adapter = chatContactListAdapter

        binding.chatListRecyclerView.addOnItemTouchListener(RecyclerViewTouchListener(activity, binding.chatListRecyclerView, object : RecyclerViewClickListener {
                    override fun onClick(view: View, position: Int) {
                        try {
                            val chatContactDetails = chatContactListAdapter!!.chatArrayList[position]
                            if (!isCheckboxMultiSelect) {
                                if (chatContactListAdapter!!.lastSelectedPosition.size == 0) {
                                    isMultiSelect = false
                                }
                                if (chatContactListAdapter!!.mBlockedContactList.size == 0) {
                                    isBlockedContactSelected = false
                                }
                                if (chatContactListAdapter!!.mSameChannelList.size == 0) {
                                    isSameChannelSelected = false
                                }
                                if (chatContactListAdapter!!.mGroupChatList.size == 0) {
                                    isGroupChatSelected = false
                                }
                            }
                            if (isMultiSelect) {
                                chatContactListAdapter!!.multiSelectList(position)
                                isBlockedContactSelected = when (chatContactListAdapter!!.mBlockedContactList.size) {
                                    0 -> {
                                        false
                                    }
                                    else -> {
                                        true
                                    }
                                }
                                isGroupChatSelected = when (chatContactListAdapter!!.mGroupChatList.size) {
                                    0 -> {
                                        false
                                    }
                                    else -> {
                                        true
                                    }
                                }
                                isSameChannelSelected = CentrlUtil.checkDuplicateUsingSet(chatContactListAdapter!!.mSameChannelList)

                                multiSelectChatList!!.onMultiSelect(true, isCheckboxMultiSelect, chatContactListAdapter!!.mChatGroupId, isBlockedContactSelected, isSameChannelSelected, isGroupChatSelected)
                            } else {
                                if(isMultiSelect){
                                    chatContactListAdapter!!.selectList(-1)
                                }
                                val intent = Intent(activity, ChatsRoomActivity::class.java)
                                intent.putExtra(AppConstant.GROUP_KEY, chatContactDetails.groupID)
                                intent.putExtra(AppConstant.PACKAGE_CODE_KEY, chatContactDetails.packageCode)
                                intent.putExtra(AppConstant.CHAT_KEY, chatContactDetails.chatKey)
                                intent.putExtra(AppConstant.IS_FROM_NOTIFICATION, false)
                                intent.putExtra(AppConstant.SHARED_TEXT_KEY, "")
                                activity!!.startActivity(intent)
                                chatContactSelectedListener!!.chatContactSelected()
                            }
                        }catch (e: Exception){
                            e.printStackTrace()
                        }
                    }

                    override fun onLongClick(view: View, position: Int) {
                        try{
                            if (!isCheckboxMultiSelect) {
                                isMultiSelect = true
                                chatContactListAdapter!!.multiSelectList(position)
                                isBlockedContactSelected = when (chatContactListAdapter!!.mBlockedContactList.size) {
                                    0 -> {
                                        false
                                    }
                                    else -> {
                                        true
                                    }
                                }
                                multiSelectChatList!!.onMultiSelect(isMultiSelect, isCheckboxMultiSelect, chatContactListAdapter!!.mChatGroupId, isBlockedContactSelected, isSameChannelSelected, isGroupChatSelected)
                            }
                        }catch (e: Exception){
                            e.printStackTrace()
                        }
                    }
                })
        )
        binding.adViewContainerChat.adViewContainer.post {
            mAdView = AdsLoading.loadBanner(requireActivity(), binding.adViewContainerChat.adViewContainer)
        }
        return root
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        ViewModelProvider(requireActivity()).get(SearchViewModel::class.java).chat!!.observe(requireActivity(), Observer { message: String? ->
            if (message!!.isNotEmpty()) {
                chatContactListAdapter!!.filter.filter(message)
            } else {
                binding.noResultFoundTxt.visibility = View.GONE
                mChatsViewModel!!.getChatContactsList(AppConstant.CHAT_SCREEN)
            }
        }
        )
        ViewModelProvider(requireActivity()).get(SearchViewModel::class.java).refresh!!.observe(requireActivity(), Observer {page ->
            if(page == 0){
                mChatsViewModel!!.getChatContactsList(AppConstant.CHAT_SCREEN)
                if (isMultiSelect) {
                    chatContactListAdapter!!.selectList(-1)
                    chatContactListAdapter!!.lastSelectedPosition.clear()
                    chatContactListAdapter!!.mSameChannelList.clear()
                    chatContactListAdapter!!.mBlockedContactList.clear()
                    chatContactListAdapter!!.mGroupChatList.clear()
                    isMultiSelect = false
                    isBlockedContactSelected = false
                    isSameChannelSelected = false
                }
            }
        })
        ViewModelProvider(requireActivity()).get(SearchViewModel::class.java).merge!!.observe(requireActivity(), Observer { integer: Int ->
            if (integer == 1) {
                isCheckboxMultiSelect = true
                isMultiSelect = true
                isBlockedContactSelected = false
                isSameChannelSelected = false
                chatContactListAdapter!!.mSameChannelList.clear()
                chatContactListAdapter!!.mBlockedContactList.clear()
                chatContactListAdapter!!.mGroupChatList.clear()
                chatContactListAdapter!!.chooseMergeOption(true)
            } else {
                isCheckboxMultiSelect = false
                isMultiSelect = false
                isBlockedContactSelected = false
                isSameChannelSelected = false
                chatContactListAdapter!!.chooseMergeOption(false)
                chatContactListAdapter!!.selectList(-1)
                chatContactListAdapter!!.mSameChannelList.clear()
                chatContactListAdapter!!.mBlockedContactList.clear()
                chatContactListAdapter!!.lastSelectedPosition.clear()
                chatContactListAdapter!!.mGroupChatList.clear()
            }
        })

        mChatsViewModel!!.mChatContactsList.observe(requireActivity(), Observer { result ->
            try{
                mChatBadgeGroupList.clear()
                mChatList = result
                if (mChatList.size == 0) {
                    binding.chatListRecyclerView.visibility = View.GONE
                    binding.emptyImageChat.visibility = View.VISIBLE
                    mChatBadgeGroupList.clear()
                } else {
                    mChatList.sortWith(Comparator sort@{ o1: ChatsEntity, o2: ChatsEntity ->
                        try {
                            return@sort DateUtil.convertDateFormat(o2.timeStamp).compareTo(DateUtil.convertDateFormat(o1.timeStamp))
                        } catch (e: Exception) {
                            e.printStackTrace()
                            return@sort 0
                        }
                    })
                    chatContactListAdapter!!.updateList(mChatList)
                    binding.chatListRecyclerView.visibility = View.VISIBLE
                    binding.emptyImageChat.visibility = View.GONE
                    for (chat in mChatList) {
                        val count = mMutiChannelViewModel!!.getCount(chat.groupID)
                        if (count != 0) {
                            val groupId = chat.groupID
                            if (!mChatBadgeGroupList.contains(groupId)) {
                                mChatBadgeGroupList.add(chat.groupID)
                            }
                        }
                    }
                }
                multiSelectChatList!!.onReceived(0, mChatBadgeGroupList)
            } catch (e: Exception){
                e.printStackTrace()
            }

        })
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        multiSelectChatList = context as MultiSelectChatList
        chatContactSelectedListener = context as ChatContactSelectedListener
        mContext = context

        if (notificationReceiver == null) {
            notificationReceiver = NotificationReceiverFragment()
            val filter = IntentFilter()
            filter.addAction(AppConstant.ACTION_BROADCAST_RECEIVER)
            context.registerReceiver(notificationReceiver, filter)
        }
    }

    override fun onStart() {
        super.onStart()
        mChatsViewModel!!.getChatContactsList(AppConstant.CHAT_SCREEN)
    }

    override fun onResume() {
        super.onResume()
        if (mAdView != null) {
            AdsLoading.resumeAds(requireActivity(), mAdView, binding.adViewContainerChat.adViewContainer)
        }
    }

    override fun onPause() {
        if (mAdView != null) {
            mAdView!!.pause()
        }
        super.onPause()
    }

    override fun onDestroyView() {
        if (mAdView != null) {
            mAdView!!.destroy()
        }
        super.onDestroyView()
    }

    override fun onDestroy() {
        if (mAdView != null) {
            mAdView!!.destroy()
        }
        super.onDestroy()
    }

    override fun onDetach() {
        super.onDetach()
        try {
            if (notificationReceiver != null) {
                mContext!!.unregisterReceiver(notificationReceiver)
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    /**
     * Search filter result
     */
    override fun filterResult(size: Int) {
        if (size == 0) {
            binding.noResultFoundTxt.visibility = View.VISIBLE
            binding.chatListRecyclerView.visibility = View.GONE
        } else {
            binding.noResultFoundTxt.visibility = View.GONE
            binding.chatListRecyclerView.visibility = View.VISIBLE
        }
    }
    /**
     * Notification receiver for update the UI
     */
    internal inner class NotificationReceiverFragment : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            try {
                val groupKey = intent.getStringExtra(AppConstant.GROUP_KEY)
                val receivedType = intent.getStringExtra(AppConstant.NOTIFICATION_TYPE)
                if(receivedType == chatScreen) {
                    val chatLast = mChatsViewModel!!.getLastChat(groupKey!!, AppConstant.CHAT_SCREEN)
                    if (chatLast.groupID.isNotEmpty()) {
                        var position = -1
                        for (i in mChatList.indices) {
                            if (mChatList[i].groupID.equals(chatLast.groupID, ignoreCase = true)) {
                                position = i
                            }
                        }
                        if (position != -1) {
                            mChatList.removeAt(position)
                            mChatList.add(0, chatLast)
                            chatContactListAdapter!!.updateList(mChatList)
                        } else {
                            if (mChatList.size == 0) {
                                mChatsViewModel!!.getChatContactsList(AppConstant.CHAT_SCREEN)
                            } else {
                                mChatList.add(0, chatLast)
                                chatContactListAdapter!!.updateList(mChatList)
                            }
                        }
                        for (chat in mChatList) {
                            val count = mMutiChannelViewModel!!.getCount(chat.groupID)
                            if (count != 0) {
                                val groupId = chat.groupID
                                if (!mChatBadgeGroupList.contains(groupId)) {
                                    mChatBadgeGroupList.add(chat.groupID)
                                }
                            }
                        }
                        multiSelectChatList!!.onReceived(0, mChatBadgeGroupList)
                    }
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }
}