package com.android.centrl.ui.fragment

import android.annotation.SuppressLint
import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.android.centrl.interfaces.PinLockInterface
import com.android.centrl.R
import com.android.centrl.databinding.FragmentPinLockFirstBinding
import com.android.centrl.ui.views.pinlockview.IndicatorDots
import com.android.centrl.ui.views.pinlockview.PinLockListener
import com.android.centrl.utils.AppConstant
import com.google.android.material.snackbar.Snackbar

/**
 * Setup pin lock fragment
 */
class PinLockFirstFragment : Fragment() {

    private lateinit var binding: FragmentPinLockFirstBinding
    private var mEnteredPin = ""
    private var pinLockInterface: PinLockInterface? = null
    private var editor: SharedPreferences.Editor? = null
    /**
     * Pin lock listener
     */
    private val mPinLockListener: PinLockListener = object : PinLockListener {
        override fun onComplete(pin: String) {
            mEnteredPin = pin
            binding.pinConfirm.setBackgroundResource(R.drawable.allow_btn_bg)
            binding.pinConfirm.isEnabled = true
        }

        override fun onEmpty() {
            mEnteredPin = ""
            binding.pinConfirm.setBackgroundResource(R.drawable.disallow_btn_bg)
            binding.pinConfirm.isEnabled = false
        }

        override fun onPinChange(pinLength: Int, intermediatePin: String) {}
    }

    @SuppressLint("CommitPrefEdits")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val preferences = requireActivity().getSharedPreferences(AppConstant.PREF_NAME, Context.MODE_PRIVATE)
        editor = preferences!!.edit()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_pin_lock_first, container, false)
        val view = binding.root

        binding.pinConfirm.setBackgroundResource(R.drawable.disallow_btn_bg)
        binding.pinConfirm.isEnabled = false
        binding.pinLockView.attachIndicatorDots(binding.indicatorDots)
        binding.pinLockView.setPinLockListener(mPinLockListener)
        //mPinLockView.setCustomKeySet(new int[]{2, 3, 1, 5, 9, 6, 7, 0, 8, 4});
        //mPinLockView.enableLayoutShuffling();
        binding.pinLockView.pinLength = 4
        binding.pinLockView.textColor = ContextCompat.getColor(requireActivity(), R.color.security_text)
        binding.pinLockView.deleteButtonDrawable = resources.getDrawable(R.drawable.ic_delete, null)
        binding.indicatorDots.indicatorType = IndicatorDots.IndicatorType.FILL_WITH_ANIMATION
        binding.pinConfirm.setOnClickListener {
            if (mEnteredPin.isNotEmpty()) {
                if (mEnteredPin.length == 4) {
                    pinLockInterface!!.firstPinLock(mEnteredPin)
                } else {
                    Snackbar.make(binding.pinLockView, resources.getString(R.string.enter_correct), Snackbar.LENGTH_LONG).show()
                }
            }
        }
        binding.pinCancel.setOnClickListener {
            editor!!.putBoolean(AppConstant.SETUP_FINGER, false)
            editor!!.commit()
            requireActivity().finish()
        }
        return view
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        pinLockInterface = context as PinLockInterface
    }

    companion object {
        const val TAG = "PinLockFirstFragment"
    }
}