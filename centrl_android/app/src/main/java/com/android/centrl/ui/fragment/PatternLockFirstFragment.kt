package com.android.centrl.ui.fragment

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.android.centrl.interfaces.PatternDrawInterface
import com.android.centrl.R
import com.android.centrl.databinding.FragmentPatternLockFirstBinding
import com.android.centrl.ui.views.patternlockview.PatternLockView
import com.android.centrl.ui.views.patternlockview.listener.PatternLockViewListener
import com.android.centrl.ui.views.patternlockview.utils.PatternLockUtils
import com.android.centrl.ui.views.patternlockview.utils.ResourceUtils

/**
 * Setup pattern lock fragment
 */
class PatternLockFirstFragment : Fragment() {

    private var patternDrawInterface: PatternDrawInterface? = null
    private lateinit var binding: FragmentPatternLockFirstBinding
    private var mDrawPattern: String? = null
    /**
     * Pattern lock listener
     */
    private val mPatternLockViewListener: PatternLockViewListener = object : PatternLockViewListener {
            override fun onStarted() {}
            override fun onProgress(progressPattern: List<PatternLockView.Dot>) {}
            override fun onComplete(pattern: List<PatternLockView.Dot>) {
                mDrawPattern = PatternLockUtils.patternToString(binding.patterLockView, pattern)
                binding.pattenChangeTxt.text = resources.getString(R.string.message_pattern_recorded)
                binding.patternCancel.text = resources.getString(R.string.clear)
                binding.patternContinue.setBackgroundResource(R.drawable.allow_btn_bg)
            }

            override fun onCleared() {
                binding.patternCancel.text = resources.getString(R.string.cancel)
                binding.pattenChangeTxt.text = resources.getString(R.string.dup)
                binding.patternContinue.setBackgroundResource(R.drawable.disallow_btn_bg)
            }
        }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_pattern_lock_first, container, false)
        val view = binding.root

        binding.patterLockView.dotCount = 3
        binding.patterLockView.dotNormalSize = ResourceUtils.getDimensionInPx(
            requireActivity(),
            R.dimen.pattern_lock_dot_size
        ).toInt()
        binding.patterLockView.dotSelectedSize = ResourceUtils.getDimensionInPx(
            requireActivity(),
            R.dimen.pattern_lock_dot_selected_size
        ).toInt()
        binding.patterLockView.pathWidth = ResourceUtils.getDimensionInPx(
            requireActivity(),
            R.dimen.pattern_lock_path_width
        ).toInt()
        binding.patterLockView.isAspectRatioEnabled = true
        binding.patterLockView.aspectRatio = PatternLockView.AspectRatio.ASPECT_RATIO_HEIGHT_BIAS
        binding.patterLockView.setViewMode(PatternLockView.PatternViewMode.CORRECT)
        binding.patterLockView.dotAnimationDuration = 150
        binding.patterLockView.pathEndAnimationDuration = 100
        binding.patterLockView.correctStateColor = ResourceUtils.getColor(requireActivity(), R.color.pattern_drawing)
        binding.patterLockView.isInStealthMode = false
        binding.patterLockView.isTactileFeedbackEnabled = true
        binding.patterLockView.isInputEnabled = true
        binding.patterLockView.addPatternLockListener(mPatternLockViewListener)

        binding.patternCancel.setOnClickListener {
            if (binding.patternCancel.text == resources.getString(R.string.cancel)) {
                requireActivity().finish()
            } else if (binding.patternCancel.text == resources.getString(R.string.clear)) {
                binding.patterLockView.clearPattern()
            }
        }
        binding.patternContinue.setOnClickListener {
            if (mDrawPattern != null && mDrawPattern!!.isNotEmpty()) {
                patternDrawInterface!!.firstDrawPattern(mDrawPattern!!)
            }
        }
        return view
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        patternDrawInterface = context as PatternDrawInterface
    }
}