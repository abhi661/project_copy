package com.android.centrl.viewmodel

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.android.centrl.R
import com.android.centrl.persistence.entity.ChatBackupEntity
import com.android.centrl.persistence.datasource.ChatBackupDataSource
import com.android.centrl.persistence.repositry.BackupRepository
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.plusAssign
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import timber.log.Timber

/**
 * View Model for the [@ChatsBackUpActivity]
 */
class ChatBackupViewModel(application: Application, private val mDataSource: ChatBackupDataSource) : BaseViewModel(application) {

    private val context = getApplication<Application>().applicationContext
    private val backupRepo: BackupRepository = BackupRepository(context)
    private val mDaily = MutableLiveData<Boolean>()
    private val mWeekly = MutableLiveData<Boolean>()
    private val mNever = MutableLiveData<Boolean>()
    private val mSmall = MutableLiveData<Boolean>()
    private val mMedium = MutableLiveData<Boolean>()
    private val mLarge = MutableLiveData<Boolean>()
    private val mEmail = MutableLiveData<String>()
    private val mLastBackup = MutableLiveData<String>()
    private val mDataBaseSize = MutableLiveData<String>()
    private val mChatBackupDetails = MutableLiveData<ChatBackupEntity>()
    private val insetStatus = MutableLiveData<Boolean>()

    /**
     * Get the backup details.
     *
     * @return a [Flowable] that will emit every time the backup details has been updated.
     */
    fun getBackupDetails(){
        disposables += mDataSource.getBackup()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe{chatBackup ->
                mChatBackupDetails.value = chatBackup
                mEmail.value = chatBackup.emailID
                mLastBackup.value = chatBackup.lastBackupTime
                mDataBaseSize.value = chatBackup.dbSize
                when {
                    chatBackup.backupType.equals(context.resources.getString(R.string.daily), ignoreCase = true) -> {
                       mDaily.value = true
                    }
                    chatBackup.backupType.equals(context.resources.getString(R.string.weekly), ignoreCase = true) -> {
                        mWeekly.value = true
                    }
                    chatBackup.backupType.equals(context.resources.getString(R.string.never), ignoreCase = true) -> {
                        mNever.value = true
                    }
                }
            }
    }
    /**
     * Insert or update the backup details.
     *
     * @return a [Completable] that completes when the backup details is updated
     */
    fun insertChatBackup(chatBackup: ChatBackupEntity) {
        disposables += mDataSource.insertOrUpdateBackupDetails(chatBackup)
            .subscribeOn(Schedulers.io())
            .subscribeBy(onComplete = {
                insetStatus.postValue(true)
            }, onError = {
                Timber.e(it)
                insetStatus.postValue(false)
            })
    }
    /**
     * @return a [Completable] that completes when the backup details is updated
     */
    fun updateChatBackupDetails(chatBackup: ChatBackupEntity) {
        disposables += mDataSource.updateChatBackupDetails(chatBackup)
            .subscribeOn(Schedulers.io())
            .subscribeBy(onComplete = {
                insetStatus.postValue(true)
            }, onError = {
                Timber.e(it)
                insetStatus.postValue(false)
            })
    }

    fun getBackupsDetails(): ChatBackupEntity{
        return backupRepo.getBackupDetails()
    }

    /**
     *  Set back up insert status to view
     */
    val backupInsertStatus: LiveData<Boolean>
        get() = insetStatus

    /**
     *  Set backup details to view
     */
    val mBackupDetails: LiveData<ChatBackupEntity>
        get() = mChatBackupDetails
    /**
     *  Set database size to view
     */
    val databaseSize: LiveData<String>
        get() = mDataBaseSize
    /**
     *  Set last backup time to view
     */
    val lastBackup: LiveData<String>
        get() = mLastBackup
    /**
     *  Set daily backup scheduling time to view
     */
    val daily: LiveData<Boolean>
        get() = mDaily
    /**
     *  Set weekly backup scheduling time to view
     */
    val weekly: LiveData<Boolean>
        get() = mWeekly
    /**
     *  Set no backup to view
     */
    val never: LiveData<Boolean>
        get() = mNever
    /**
     *  Set backup scheduling time to view
     */
    fun setmNever(isNever: Boolean) {
        mNever.value = isNever
    }
    /**
     *  Set font size to view
     */
    val small: LiveData<Boolean>
        get() = mSmall
    /**
     *  Set font size to view
     */
    fun setmSmall(isSmall: Boolean) {
        mSmall.value = isSmall
    }
    /**
     *  Set font size to view
     */
    val medium: LiveData<Boolean>
        get() = mMedium
    /**
     *  Set font size to view
     */
    fun setmMedium(isMedium: Boolean) {
        mMedium.value = isMedium
    }
    /**
     *  Set font size to view
     */
    val large: LiveData<Boolean>
        get() = mLarge
    /**
     *  Set font size to view
     */
    fun setmLarge(isLarge: Boolean) {
        mLarge.value = isLarge
    }
    /**
     *  Set user email to view
     */
    val email: LiveData<String>
        get() = mEmail
    /**
     *  Set user email to view
     */
    fun setmEmail(email: String) {
        mEmail.value = email
    }

}