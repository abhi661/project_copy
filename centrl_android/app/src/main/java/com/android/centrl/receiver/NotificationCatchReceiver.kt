package com.android.centrl.receiver

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import com.android.centrl.notificationservice.NotificationUtils

/**
 * Notification receiver for starting notification listener after restarting device
 */
class NotificationCatchReceiver : BroadcastReceiver() {
    override fun onReceive(context: Context, intent: Intent) {
        try {
            val action = intent.action!!
            if (action.equals("android.intent.action.BOOT_COMPLETED", ignoreCase = true) || action.equals("android.intent.action.LOCKED_BOOT_COMPLETED", ignoreCase = true)) {
                NotificationUtils.startNotiCatchService(context)
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }
}