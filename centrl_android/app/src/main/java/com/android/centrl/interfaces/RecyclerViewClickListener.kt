package com.android.centrl.interfaces

import android.view.View
/**
 * Chat and mail recycler view click listener
 */
interface RecyclerViewClickListener {
    fun onClick(view: View, position: Int)
    fun onLongClick(view: View, position: Int)
}