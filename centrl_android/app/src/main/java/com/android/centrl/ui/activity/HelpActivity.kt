package com.android.centrl.ui.activity

import android.content.Intent
import android.os.Bundle
import android.view.KeyEvent
import android.view.View
import androidx.databinding.DataBindingUtil
import com.android.centrl.R
import com.android.centrl.databinding.ActivityHelpBinding
import com.android.centrl.utils.AdsLoading
import com.google.android.gms.ads.AdView

/**
 * Help activity.
 */
class HelpActivity : BaseActivity() {

    private lateinit var binding: ActivityHelpBinding
    private var mAdView: AdView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_help)
        binding.handler = ClickHandler()
        binding.helpAds.adViewContainer.post {
            // Loading ads
            mAdView = AdsLoading.loadBanner(this@HelpActivity, binding.helpAds.adViewContainer)
        }
    }
    /**
     * View click handler class
     */
    inner class ClickHandler{
        /**
         * View click handler
         */
        fun onClick(view: View){
            when(view.id){
                R.id.help_back -> { finish() }
                R.id.faq_rl -> {
                    val intent = Intent(this@HelpActivity, FAQActivity::class.java)
                    intent.putExtra("help_title", resources.getString(R.string.faq))
                    startActivity(intent)
                }
                R.id.contact_us_rl -> {
                    val intent = Intent(this@HelpActivity, HelpPagesActivity::class.java)
                    intent.putExtra("help_title", resources.getString(R.string.contact_us))
                    //startActivity(intent)
                }
                R.id.terms_rl -> {
                    val intent = Intent(this@HelpActivity, HelpPagesActivity::class.java)
                    intent.putExtra("help_title", resources.getString(R.string.terms_policy))
                   // startActivity(intent)
                }
                R.id.about_rl -> {
                    val intent = Intent(this@HelpActivity, HelpPagesActivity::class.java)
                    intent.putExtra("help_title", resources.getString(R.string.about))
                    //startActivity(intent)
                }
            }
        }
    }


    override fun onResume() {
        super.onResume()
        if (mAdView != null) {
            AdsLoading.resumeAds(this, mAdView, binding.helpAds.adViewContainer)
        }
    }

    override fun onPause() {
        if (mAdView != null) {
            mAdView!!.pause()
        }
        super.onPause()
    }

    override fun onDestroy() {
        if (mAdView != null) {
            mAdView!!.destroy()
        }
        super.onDestroy()
    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            finish()
            return true
        }
        return super.onKeyDown(keyCode, event)
    }
}