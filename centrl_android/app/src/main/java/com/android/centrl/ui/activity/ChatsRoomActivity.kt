package com.android.centrl.ui.activity

import android.Manifest
import android.app.Activity
import android.app.NotificationManager
import android.content.*
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import android.graphics.Rect
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.ContactsContract
import android.provider.Settings
import android.util.Patterns
import android.view.KeyEvent
import android.view.MenuItem
import android.view.View
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.*
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AlertDialog
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.core.app.ActivityCompat
import androidx.core.content.pm.ShortcutInfoCompat
import androidx.core.content.pm.ShortcutManagerCompat
import androidx.core.graphics.drawable.IconCompat
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.android.centrl.R
import com.android.centrl.adapter.KeyBoardTrayChannelListAdapter
import com.android.centrl.adapter.MessageListAdapter
import com.android.centrl.appiconbadger.ShortcutBadger
import com.android.centrl.interfaces.SelectChannel
import com.android.centrl.models.AdsObject
import com.android.centrl.models.KeyboardAppTrayModel
import com.android.centrl.notificationservice.NotificationData
import com.android.centrl.notificationservice.PendingIntentManager
import com.android.centrl.notificationservice.PrefManager
import com.android.centrl.notificationservice.SendSMS
import com.android.centrl.persistence.entity.BlockedContactsEntity
import com.android.centrl.persistence.entity.ChatsEntity
import com.android.centrl.persistence.entity.GroupChannelsEntity
import com.android.centrl.ui.dialog.DialogChatFilterMessage
import com.android.centrl.utils.*
import com.bumptech.glide.Glide
import com.google.android.gms.ads.AdListener
import com.google.android.gms.ads.AdLoader
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.VideoOptions
import com.google.android.gms.ads.formats.NativeAdOptions
import com.google.android.gms.ads.formats.UnifiedNativeAd
import hani.momanii.supernova_emoji_library.Actions.EmojIconActions
import hani.momanii.supernova_emoji_library.Helper.EmojiconEditText
import java.io.File
import java.util.*
import kotlin.collections.ArrayList

/**
 * Chat room activity
 */
class ChatsRoomActivity : BaseActivity(),
    SelectChannel, PopupMenu.OnMenuItemClickListener,
    DialogChatFilterMessage.DialogFilterMessageListener, MessageListAdapter.GoToNativeAppInterface {

    companion object {
        val chatList = ArrayList<ChatsEntity>()
        var PERMISSIONS_REQUEST_SEND_SMS = 100
        private const val chatScreen = "chat"
    }

    private var nReceiver: NotificationReceiver? = null
    private lateinit var adapter: MessageListAdapter
    private var identityListAdapter: KeyBoardTrayChannelListAdapter? = null
    private var animation: Animation? = null
    private lateinit var selectIdentity: SelectChannel
    private lateinit var pendingIntentManager: PendingIntentManager
    private lateinit var chatDetails: GroupChannelsEntity
    private var mChatGroupIdList: ArrayList<ChatsEntity>? = null
    private lateinit var sendSMS: SendSMS
    private var mEmojIcon: EmojIconActions? = null

    private var isMute: Int = 0
    private var filterPackageCode: Int = 0
    private var isFilterMessage: Boolean = false
    private var isKeyboardShowing: Boolean = false
    private var groupSize: Int = 0
    private var isBlocked = false
    private var mPhoneNumber: String = ""
    private var mSharedText: String = ""
    private var isMultiSelect = false
    private var isShowedMultiSelect = false
    private var isFromBundle = false
    private var isNativeAdsShowing = true

    // The number of native ads to load.
    private val mNumberOfAds = 5

    // List of native ads that have been successfully loaded.
    private val mNativeAds: ArrayList<UnifiedNativeAd> = ArrayList()

    @BindView(R.id.outbound_edit_txt)
    @JvmField
    var msgEditTxt: EmojiconEditText? = null
    @BindView(R.id.img_chat_bg)
    @JvmField
    var mWallPaper: ImageView? = null
    @BindView(R.id.send_msg_btn)
    @JvmField
    var sendBtn: ImageView? = null
    @BindView(R.id.select_count_txt_cs)
    @JvmField
    var mMultiSelectCountTxt: TextView? = null
    @BindView(R.id.chat_header_main_ll)
    @JvmField
    var mChatHeaderRL: RelativeLayout? = null
    @BindView(R.id.multiselect_title_bar_cs)
    @JvmField
    var mMultiSelectRL: RelativeLayout? = null
    @BindView(R.id.unmerge_group_ll)
    @JvmField
    var unmergeGroupLl: LinearLayout? = null
    @BindView(R.id.chat_room_recyclerView)
    @JvmField
    var rvRecyclerView: RecyclerView? = null
    @BindView(R.id.identity_recyclerView)
    @JvmField
    var identityRecyclerView: RecyclerView? = null
    @BindView(R.id.chat_root_rl)
    @JvmField
    var rootLayout: ConstraintLayout? = null
    @BindView(R.id.relative_layout_app_container)
    @JvmField
    var appContainerLayout: RelativeLayout? = null
    @BindView(R.id.addToContactLayout)
    @JvmField
    var addToContactLayout: LinearLayout? = null
    @BindView(R.id.empty_image_chat_room)
    @JvmField
    var emptyImageChatRoom: LinearLayout? = null
    @BindView(R.id.group_chat_choice)
    @JvmField
    var groupChatChoice: ImageView? = null
    @BindView(R.id.group_chat_menu)
    @JvmField
    var groupChatMenu: ImageView? = null
    @BindView(R.id.group_chat_profile_image)
    @JvmField
    var groupChatProfileImage: ImageView? = null
    @BindView(R.id.group_chat_title_txt)
    @JvmField
    var groupTitle: TextView? = null
    @BindView(R.id.chat_room_snackar)
    @JvmField
    var snackbarCL: CoordinatorLayout? = null
    @BindView(R.id.emoji_btn_chat_room)
    @JvmField
    var emojiButton: ImageView? = null

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_chat_room)
        ButterKnife.bind(this)
        sendSMS = SendSMS(this)

        val manager: NotificationManager =
            getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        manager.cancel(0)
        selectIdentity = this
        PrefManager.getInstance(this)!!.saveBoolean(AppConstant.IS_CHAT_SCREEN, true)
        pendingIntentManager = PendingIntentManager.getInstance(this)
        val bundle = intent.extras
        if (bundle != null) {
            groupKey = bundle.getString(AppConstant.GROUP_KEY)
            chatKey = bundle.getString(AppConstant.CHAT_KEY)
            packageCode = bundle.getInt(AppConstant.PACKAGE_CODE_KEY)
            isFromBundle = bundle.getBoolean(AppConstant.IS_FROM_NOTIFICATION)
            mSharedText = bundle.getString(AppConstant.SHARED_TEXT_KEY, null)
        }
        initView()
        rootLayout!!.viewTreeObserver.addOnGlobalLayoutListener {
            val r = Rect()
            rootLayout!!.getWindowVisibleDisplayFrame(r)
            val screenHeight = rootLayout!!.rootView.height
            val keypadHeight = screenHeight - r.bottom
            if (keypadHeight > screenHeight * 0.15) { // 0.15 ratio is perhaps enough to determine keypad height.
                // keyboard is opened
                if (!isKeyboardShowing) {
                    isKeyboardShowing = true
                    onKeyboardVisibilityChanged(true)
                    setRecyclerEndPosition()
                }
            } else {
                // keyboard is closed
                if (isKeyboardShowing) {
                    isKeyboardShowing = false
                    onKeyboardVisibilityChanged(false)
                    if (isNativeAdsShowing) {
                        insertAds()
                    }
                }
            }
        }

        sendBtn!!.setOnLongClickListener {
            if (packageCode == AppConstant.SMS_CODE && sendSMS.checkDualSim()) {
                sendSMS.selectSim()
            }
            return@setOnLongClickListener true
        }

        mBlockedContactsViewModel!!.mInsertStatus.observe(this, Observer {
            PendingIntentManager.getInstance(this).getBlockedContact()
        })

        mGroupChannelsViewModel!!.mGroupDetails.observe(this, Observer { result ->
            try {
                PrefManager.getInstance(this)!!.saveStringValue(AppConstant.CHAT_GROUP_ID, groupKey)
                chatDetails = result
                groupSize = mGroupChannelsViewModel!!.getGroupList(groupKey!!).size
                isMute = chatDetails.muteChat
                mMutiChannelViewModel!!.deleteCount(groupKey!!)
                mMutiChannelViewModel!!.deleteGroupAllPackageCode(chatDetails.groupID)

                var tempCount = 0
                for (chatCount in mMutiChannelViewModel!!.getAllCount()) {
                    val counts: Int = mMutiChannelViewModel!!.getCount(chatCount.groupID)
                    tempCount += counts
                }
                ShortcutBadger.applyCount(this, tempCount)
                if (chatDetails.isGroup == 1) {
                    groupChatChoice!!.visibility = View.INVISIBLE
                    unmergeGroupLl!!.visibility = View.GONE
                } else {
                    if (groupSize > 1) {
                        groupChatChoice!!.visibility = View.VISIBLE
                        unmergeGroupLl!!.visibility = View.VISIBLE
                    } else {
                        groupChatChoice!!.visibility = View.INVISIBLE
                        unmergeGroupLl!!.visibility = View.GONE
                    }
                }
                if (isValidMobile(chatDetails.title)) {
                    if (groupSize == 1) {
                        mPhoneNumber = chatDetails.title
                        addToContactLayout!!.visibility = View.VISIBLE
                    }
                } else {
                    addToContactLayout!!.visibility = View.GONE
                }
                groupTitle!!.text = chatDetails.groupTitle
                if (chatDetails.groupIconPath.isEmpty()) {
                    groupChatProfileImage!!.setImageResource(R.drawable.default_user_image)
                } else {
                    val profileImage: Bitmap = BitmapFactory.decodeFile(chatDetails.groupIconPath)
                    groupChatProfileImage!!.setImageBitmap(profileImage)
                }

                chatList.clear()
                if (isFilterMessage) {
                    for (chat in mChatsViewModel!!.getSingleChatsList(groupKey!!, packageCode)) {
                        chatList.add(chat)
                    }
                } else {
                    for (chat in mChatsViewModel!!.getGroupChatsList(groupKey!!)) {
                        chatList.add(chat)
                    }
                }
                adapter.setDataChange(
                    CentrlUtil.groupDataIntoHashMap(chatList),
                    chatDetails.isGroup
                )
                rvRecyclerView!!.scrollToPosition(CentrlUtil.groupDataIntoHashMap(chatList).size - 1)
                refreshUI()
                setSendButton(packageCode)

                val wallpaperPath = preferences!!.getString(AppConstant.WALLPAPER_PATH, null)
                if (wallpaperPath != null) {
                    rootLayout!!.setBackgroundColor(0)
                    Glide.with(this)
                        .load(wallpaperPath)
                        .into(mWallPaper!!)
                } else {
                    rootLayout!!.setBackgroundColor(resources.getColor(R.color.bg_color, null))
                    Glide.with(this)
                        .load(R.drawable.defaut_background)
                        .into(mWallPaper!!)
                }
                if (isNativeAdsShowing) {
                    loadNativeAds()
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        })

        mGroupChannelsViewModel!!.mKeyboardApps.observe(this, Observer { packArray ->
            try {
                val keyboardTray: ArrayList<KeyboardAppTrayModel> = ArrayList()
                val activeChannels = mChatsViewModel!!.getGroupChannelsList(chatDetails.groupID)
                if (packArray.contains(0)) {
                    packArray.remove(0)
                }
                if (packArray.contains(30)) {
                    packArray.remove(30)
                }
                if (packArray.contains(31)) {
                    packArray.remove(31)
                }
                for(i in activeChannels){
                    keyboardTray.add(KeyboardAppTrayModel(i,1))
                    if(packArray.contains(i)){
                        packArray.remove(i)
                    }
                }
                for(i in packArray){
                    keyboardTray.add(KeyboardAppTrayModel(i,0))
                }
                identityListAdapter = KeyBoardTrayChannelListAdapter(keyboardTray, selectIdentity)
                identityRecyclerView!!.adapter = identityListAdapter
            } catch (e: Exception) {
                e.printStackTrace()
            }
        })

        msgEditTxt!!.setOnClickListener {
            mEmojIcon!!.hidePopup()
        }

        if (mSharedText.isNotEmpty()) {
            msgEditTxt!!.setText(mSharedText)
        }
    }

    /**
     * Initialization views
     */
    private fun initView() {
        try {
            animation = AnimationUtils.loadAnimation(this, R.anim.search_right_to_left)

            addToContactLayout!!.visibility = View.GONE
            appContainerLayout!!.visibility = View.GONE

            mEmojIcon = EmojIconActions(this, rootLayout, msgEditTxt, emojiButton)
            mEmojIcon!!.setIconsIds(R.drawable.ic_keyboard, R.drawable.ic_emoji)
            mEmojIcon!!.addEmojiconEditTextList(msgEditTxt)

            msgEditTxt!!.isActivated = true
            msgEditTxt!!.isPressed = true
            msgEditTxt!!.setSelection(0)

            val layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
            layoutManager.stackFromEnd = true
            rvRecyclerView!!.layoutManager = layoutManager
            identityRecyclerView!!.layoutManager = LinearLayoutManager(
                this,
                RecyclerView.HORIZONTAL,
                false
            )

            adapter = MessageListAdapter(this, null, preferences!!, this)
            adapter.setUser(0)
            rvRecyclerView!!.adapter = adapter

            nReceiver = NotificationReceiver()
            val filter = IntentFilter()
            filter.addAction(AppConstant.ACTION_BROADCAST_RECEIVER_CHAT)
            registerReceiver(nReceiver, filter)

        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun onStart() {
        super.onStart()
        val manager: NotificationManager =
            getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        manager.cancel(0)
        PrefManager.getInstance(this)!!.saveBoolean(AppConstant.IS_CHAT_SCREEN, true)
        mGroupChannelsViewModel!!.groupDetails(groupKey!!)
        mGroupChannelsViewModel!!.getKeyboardTrayApps()
    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        val bundle = intent!!.extras
        if (bundle != null) {
            groupKey = bundle.getString(AppConstant.GROUP_KEY)
            chatKey = bundle.getString(AppConstant.CHAT_KEY)
            packageCode = bundle.getInt(AppConstant.PACKAGE_CODE_KEY)
            isFromBundle = bundle.getBoolean(AppConstant.IS_FROM_NOTIFICATION)
        }
    }

    /**
     * Views click handler
     */
    @OnClick(
        R.id.select_delete_cs,
        R.id.select_copy_cs,
        R.id.select_close_cs,
        R.id.unmerge_group_ll,
        R.id.add_to_contact,
        R.id.group_chat_choice,
        R.id.group_chat_back_layout,
        R.id.group_chat_menu,
        R.id.send_msg_btn,
        R.id.chat_keyboard_setting,
        R.id.chat_room_header_rl
    )
    fun onClick(v: View?) {
        when (v!!.id) {
            R.id.select_delete_cs -> {
                removeChats()
            }
            R.id.select_copy_cs -> {
                try {
                    if (mChatGroupIdList!!.size >= 1) {
                        val sb = StringBuilder()
                        val str = "\n"
                        for (chat in mChatGroupIdList!!) {
                            sb.append(chat.messages)
                            sb.append(str)
                        }
                        copyText(sb.toString())
                        refresh()
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
            R.id.select_close_cs -> {
                refresh()
            }
            R.id.unmerge_group_ll -> {
                try {
                    val groupSize = mGroupChannelsViewModel!!.getGroupList(groupKey!!).size
                    if (groupSize == mChatGroupIdList!!.size) {
                        var newGroupKey = ""
                        if (mChatGroupIdList!!.size >= 1) {
                            for (chat in mChatGroupIdList!!) {
                                val rand = Random()
                                newGroupKey = rand.nextInt(10000).toString()
                                val chatsDetails = mGroupChannelsViewModel!!.getGroupKey(
                                    chat.chatKey,
                                    chat.packageCode
                                )
                                mGroupChannelsViewModel!!.updateGroup(
                                    chatsDetails.chatKey,
                                    newGroupKey,
                                    chatsDetails.iconPath,
                                    chatsDetails.iconPath,
                                    chatsDetails.title
                                )
                                mChatsViewModel!!.updateChat(chatsDetails.chatKey, newGroupKey)
                            }
                        }
                        val lastChat = mChatsViewModel!!.getLastChat(
                            newGroupKey,
                            AppConstant.CHAT_SCREEN
                        )
                        packageCode = lastChat.packageCode
                        groupKey = lastChat.groupID
                        chatKey = lastChat.chatKey
                        refresh()
                    } else {
                        if (mChatGroupIdList!!.size >= 1) {
                            for (chat in mChatGroupIdList!!) {
                                val rand = Random()
                                val groupKey = rand.nextInt(10000).toString()
                                val chatsDetails = mGroupChannelsViewModel!!.getGroupKey(
                                    chat.chatKey,
                                    chat.packageCode
                                )
                                mGroupChannelsViewModel!!.updateGroup(
                                    chatsDetails.chatKey,
                                    groupKey,
                                    chatsDetails.iconPath,
                                    chatsDetails.iconPath,
                                    chatsDetails.title
                                )
                                mChatsViewModel!!.updateChat(chatsDetails.chatKey, groupKey)
                            }
                        }
                        val getGroupSize = mGroupChannelsViewModel!!.getGroupList(groupKey!!).size
                        if (getGroupSize == 1) {
                            val chatGroup = mGroupChannelsViewModel!!.getGroupDetails(groupKey!!)
                            mGroupChannelsViewModel!!.updateGroup(
                                chatGroup.chatKey,
                                groupKey!!,
                                chatGroup.iconPath,
                                chatGroup.iconPath,
                                chatGroup.title
                            )
                            mChatsViewModel!!.updateChat(chatGroup.chatKey, groupKey!!)
                        }
                        val lastChat = mChatsViewModel!!.getLastChat(
                            groupKey!!,
                            AppConstant.CHAT_SCREEN
                        )
                        packageCode = lastChat.packageCode
                        refresh()
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
            R.id.add_to_contact -> {
                val intentInsertEdit = Intent(Intent.ACTION_INSERT_OR_EDIT)
                intentInsertEdit.type = ContactsContract.Contacts.CONTENT_ITEM_TYPE
                intentInsertEdit.putExtra(ContactsContract.Intents.Insert.PHONE, chatDetails.title)
                startActivityForResult(intentInsertEdit, 1)
            }
            R.id.group_chat_choice -> {
                val dialogFilter = DialogChatFilterMessage()
                val bundle = Bundle()
                bundle.putBoolean("isfilter", isFilterMessage)
                bundle.putString("groupId", chatDetails.groupID)
                dialogFilter.arguments = bundle
                dialogFilter.show(supportFragmentManager, DialogChatFilterMessage.TAG)
            }
            R.id.group_chat_back_layout -> {
                closeActivity()
            }
            R.id.group_chat_menu -> {
                try {
                    val popup = PopupMenu(this@ChatsRoomActivity, groupChatMenu!!)
                    popup.menuInflater.inflate(R.menu.menu, popup.menu)
                    popup.setOnMenuItemClickListener(this@ChatsRoomActivity)
                    popup.show()
                    val chat = mGroupChannelsViewModel!!.getGroupDetails(groupKey!!)
                    isMute = chat.muteChat
                    val menu = popup.menu

                    if (isMute == 0) {
                        menu.getItem(1).title = resources.getString(R.string.mute_txt)
                    } else {
                        menu.getItem(1).title = resources.getString(R.string.unmute_txt)
                    }
                    val getChatBlocked = mBlockedContactsViewModel!!.getBlockedContacts(groupKey!!)
                    if (getChatBlocked.groupID.isNotEmpty()) {
                        if (getChatBlocked.groupID.equals(chat.groupID, ignoreCase = true)) {
                            menu.getItem(2).title = resources.getString(R.string.unblock_from_txt)
                            isBlocked = true
                        } else {
                            menu.getItem(2).title = resources.getString(R.string.block_from_txt)
                            isBlocked = false
                        }
                    } else {
                        menu.getItem(2).title = resources.getString(R.string.block_from_txt)
                        isBlocked = false

                    }

                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
            R.id.send_msg_btn -> {
                sendMessage()
            }
            R.id.chat_keyboard_setting -> {
                val keyboardSettings = Intent(
                    this@ChatsRoomActivity,
                    KeyBoardSettingsActivity::class.java
                )
                startActivity(keyboardSettings)
            }
            R.id.chat_room_header_rl -> {
                val intent = Intent(this, ProfileActivity::class.java)
                intent.putExtra(AppConstant.GROUP_KEY, groupKey)
                intent.putExtra(AppConstant.PARTICIPANTS, adapter.mChatTitle)
                startActivity(intent)
            }
        }
    }

    /**
     * Remove chats alert dialog.
     */
    private fun removeChats() {
        val builder = AlertDialog.Builder(this)
        if (mChatGroupIdList!!.size == 1) {
            builder.setTitle(resources.getString(R.string.delete_chat_title))
        } else {
            builder.setTitle(
                resources.getString(R.string.delete_chat) + " ${mChatGroupIdList!!.size} " + resources.getString(
                    R.string.delete_msgs
                )
            )
        }
        builder.setCancelable(true)
        builder.setNegativeButton(resources.getString(R.string.cancel)) { dialog, _ -> dialog.dismiss() }
        builder.setPositiveButton(resources.getString(R.string.delete_chat)) { dialog, _ ->
            try {
                if (mChatGroupIdList!!.size >= 1) {
                    for (chat in mChatGroupIdList!!) {
                        mChatsViewModel!!.deleteChatMessages(chat.chatID)
                    }
                    refresh()
                    dialog.dismiss()
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
        val dialog = builder.create()
        dialog.show()
        dialog.getButton(AlertDialog.BUTTON_POSITIVE).isAllCaps = false
        dialog.getButton(AlertDialog.BUTTON_NEGATIVE).isAllCaps = false
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            dialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(
                resources.getColor(
                    R.color.continue_color,
                    null
                )
            )
            dialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(
                resources.getColor(
                    R.color.continue_color,
                    null
                )
            )
        } else {
            dialog.getButton(AlertDialog.BUTTON_POSITIVE)
                .setTextColor(Color.parseColor(AppConstant.CONTINUE_BUTTON))
            dialog.getButton(AlertDialog.BUTTON_NEGATIVE)
                .setTextColor(Color.parseColor(AppConstant.CONTINUE_BUTTON))
        }
    }

    /**
     * This is use to send message to the contact person
     */
    private fun sendMessage() {
        try {
            val getChatBlocked = mBlockedContactsViewModel!!.getBlockedContacts(groupKey!!)
            if (getChatBlocked.groupID.equals(chatDetails.groupID, ignoreCase = true)) {
                CentrlSnackBar.showSnackBar(snackbarCL!!, resources.getString(R.string.unblock_txt))
                return
            }

            if (packageCode == AppConstant.SMS_CODE) {
                if (!requestPhoneStatePermission()) {
                    return
                }
                if (requestSMSPermission()) {
                    if (msgEditTxt!!.text.toString().isEmpty()) {
                        CentrlSnackBar.showSnackBar(
                            snackbarCL!!,
                            resources.getString(R.string.type_msg_txt)
                        )
                        return
                    }
                    val chatPhone = mGroupChannelsViewModel!!.getGroupKey(
                        chatKey!!,
                        AppConstant.SMS_CODE
                    )
                    val msg: String = msgEditTxt!!.text.toString()
                    if (chatPhone.number.isNotEmpty()) {
                        val phoneNum: String = chatPhone.number
                        val sentResult = sendSMS.send(msg, phoneNum)
                        if (sentResult == 1) {
                            val chat =
                                ChatsEntity(
                                    0,
                                    groupKey!!,
                                    chatKey!!,
                                    "",
                                    msg,
                                    packageCode,
                                    "",
                                    DateUtil.currentTimeFormat(this@ChatsRoomActivity),
                                    ChatsEntity.MSG_TYPE_SENT,
                                    "",
                                    AppConstant.CHAT_SCREEN,
                                    phoneNum
                                )
                            mChatsViewModel!!.insertChat(chat)
                            if (chatList.size == 0) {
                                mGroupChannelsViewModel!!.groupDetails(groupKey!!)
                                msgEditTxt!!.text!!.clear()
                                msgEditTxt!!.hint = resources.getString(R.string.type_something_txt)
                            } else {
                                chatList.add(chat)
                                if (isNativeAdsShowing) {
                                    insertAds()
                                } else {
                                    adapter.setDataChange(
                                        CentrlUtil.groupDataIntoHashMap(chatList),
                                        chatDetails.isGroup
                                    )
                                    rvRecyclerView!!.scrollToPosition(
                                        CentrlUtil.groupDataIntoHashMap(
                                            chatList
                                        ).size - 1
                                    )
                                }
                                msgEditTxt!!.text!!.clear()
                                msgEditTxt!!.hint = resources.getString(R.string.type_something_txt)
                            }
                        } else {
                            CentrlSnackBar.showSnackBar(
                                snackbarCL!!,
                                resources.getString(R.string.sms_not_send)
                            )
                        }

                    } else {
                        val packageName = installedAppsViewModel!!.getInstalledPackageName(
                            packageCode
                        )
                        if (packageName.isNotEmpty()) {
                            pendingIntentManager.goOtherApps(
                                chatKey, installedAppsViewModel!!.getInstalledPackageName(
                                    packageCode
                                )
                            )
                            if (msgEditTxt!!.text.toString().isNotEmpty()) {
                                copyTextToOtherApp(msgEditTxt!!.text.toString())
                                msgEditTxt!!.text!!.clear()
                                msgEditTxt!!.hint = resources.getString(R.string.type_something_txt)
                            }
                        } else {
                            CentrlSnackBar.showSnackBar(
                                snackbarCL!!,
                                resources.getString(R.string.package_not_installed)
                            )
                        }
                    }

                }
            } else {
                if (pendingIntentManager.isReplyInfo(chatKey!!)) {
                    if (msgEditTxt!!.text.toString().isEmpty()) {
                        CentrlSnackBar.showSnackBar(
                            snackbarCL!!,
                            resources.getString(R.string.type_msg_txt)
                        )
                        return
                    } else {
                        val typeMessage = msgEditTxt!!.text.toString()
                        val chat =
                            ChatsEntity(
                                0,
                                groupKey!!,
                                chatKey!!,
                                "",
                                typeMessage,
                                packageCode,
                                "",
                                DateUtil.currentTimeFormat(this@ChatsRoomActivity),
                                ChatsEntity.MSG_TYPE_SENT,
                                "",
                                AppConstant.CHAT_SCREEN,
                                chatDetails.number
                            )
                        PendingIntentManager.getInstance(this@ChatsRoomActivity).reply(
                            chatKey!!,
                            typeMessage
                        )
                        mChatsViewModel!!.insertChat(chat)
                        chatList.add(chat)
                        if (isNativeAdsShowing) {
                            insertAds()
                        } else {
                            adapter.setDataChange(
                                CentrlUtil.groupDataIntoHashMap(chatList),
                                chatDetails.isGroup
                            )
                            rvRecyclerView!!.scrollToPosition(
                                CentrlUtil.groupDataIntoHashMap(
                                    chatList
                                ).size - 1
                            )
                        }
                        msgEditTxt!!.text!!.clear()
                        msgEditTxt!!.hint = resources.getString(R.string.type_something_txt)
                        if (packageCode == AppConstant.INSTAGRAM_CODE || packageCode == AppConstant.ICQ_CODE) {
                            PrefManager.getInstance(this)!!.saveBoolean(AppConstant.IS_SEND, true)
                        }
                    }
                } else {
                    val packageName = installedAppsViewModel!!.getInstalledPackageName(packageCode)
                    if (packageName.isNotEmpty()) {
                        pendingIntentManager.goOtherApps(
                            chatKey, installedAppsViewModel!!.getInstalledPackageName(
                                packageCode
                            )
                        )
                        if (msgEditTxt!!.text.toString().isNotEmpty()) {
                            copyTextToOtherApp(msgEditTxt!!.text.toString())
                            msgEditTxt!!.text!!.clear()
                            msgEditTxt!!.hint = resources.getString(R.string.type_something_txt)
                        }
                    } else {
                        CentrlSnackBar.showSnackBar(
                            snackbarCL!!,
                            resources.getString(R.string.package_not_installed)
                        )
                    }
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    /**
     * Multi select for delete, copy and unmerge
     */
    private fun onMultiSelect(chats: ArrayList<ChatsEntity>) {
        try {
            mMultiSelectCountTxt!!.text = chats.size.toString()
            mChatGroupIdList = chats
            if (chats.size >= 1) {
                if (!isShowedMultiSelect) {
                    isShowedMultiSelect = true
                    mMultiSelectRL!!.startAnimation(animation)
                    mMultiSelectRL!!.visibility = View.VISIBLE
                    mChatHeaderRL!!.visibility = View.GONE
                }
            } else {
                isShowedMultiSelect = false
                mChatHeaderRL!!.startAnimation(animation)
                mChatHeaderRL!!.visibility = View.VISIBLE
                mMultiSelectRL!!.visibility = View.GONE
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    /**
     * Refresh and update the views
     */
    private fun refresh() {
        try {
            isShowedMultiSelect = false
            mChatHeaderRL!!.startAnimation(animation)
            mChatHeaderRL!!.visibility = View.VISIBLE
            mMultiSelectRL!!.visibility = View.GONE
            adapter.selectList(-1)
            adapter.lastSelectedPosition.clear()
            isMultiSelect = false
            mGroupChannelsViewModel!!.groupDetails(groupKey!!)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    /**
     * Copy selected text from chats
     */
    private fun copyText(text: String) {
        try {
            if (text.isNotEmpty()) {
                val clipboardManager =
                    getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
                val clipData: ClipData = ClipData.newPlainText("chats", text)
                clipboardManager.setPrimaryClip(clipData)
                if (mChatGroupIdList!!.size == 1) {
                    CentrlSnackBar.showSnackBar(
                        snackbarCL!!,
                        resources.getString(R.string.msg_copied)
                    )
                } else {
                    CentrlSnackBar.showSnackBar(
                        snackbarCL!!,
                        resources.getString(R.string.msgs_copied)
                    )
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    /**
     * Copy selected text from chats
     */
    private fun copyTextToOtherApp(text: String) {
        try {
            if (text.isNotEmpty()) {
                val clipboardManager =
                    getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
                val clipData: ClipData = ClipData.newPlainText("chats", text)
                clipboardManager.setPrimaryClip(clipData)
                val msg =
                    resources.getString(R.string.msg_copied_other) + "${CentrlUtil.getChannelsNameHashMap()[packageCode]}" + resources.getString(
                        R.string.msg_copied_other1
                    )
                Toast.makeText(this, msg, Toast.LENGTH_LONG).show()
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    /**
     * Validate the phone number
     */
    private fun isValidMobile(phone: String): Boolean {
        return Patterns.PHONE.matcher(phone).matches()
    }

    /**
     * Set the list end position
     */
    private fun setRecyclerEndPosition() {
        try {
            chatList.clear()
            if (isFilterMessage) {
                for (chat in mChatsViewModel!!.getSingleChatsList(groupKey!!, packageCode)) {
                    chatList.add(chat)
                }
            } else {
                for (chat in mChatsViewModel!!.getGroupChatsList(groupKey!!)) {
                    chatList.add(chat)
                }
            }

            if (isNativeAdsShowing) {
                insertAds()
            } else {
                adapter.setDataChange(
                    CentrlUtil.groupDataIntoHashMap(chatList),
                    chatDetails.isGroup
                )
                rvRecyclerView!!.scrollToPosition(CentrlUtil.groupDataIntoHashMap(chatList).size - 1)
            }
            refreshUI()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    /**
     * Refresh UI
     */
    private fun refreshUI() {
        if (chatList.size == 0) {
            rvRecyclerView!!.visibility = View.GONE
            emptyImageChatRoom!!.visibility = View.GONE
        } else {
            rvRecyclerView!!.visibility = View.VISIBLE
            emptyImageChatRoom!!.visibility = View.GONE
        }
    }

    /**
     * Request SMS permission for receiving SMS
     */
    private fun requestSMSPermission(): Boolean {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && checkSelfPermission(Manifest.permission.SEND_SMS) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(
                    this,
                    Manifest.permission.SEND_SMS
                )
            ) {
                // sees the explanation, try again to request the permission.
                val builder = AlertDialog.Builder(this)
                builder.setTitle(resources.getString(R.string.permission))
                builder.setMessage(resources.getString(R.string.sms_txt))
                builder.setCancelable(false)
                builder.setPositiveButton(resources.getString(R.string.continue_txt)) { _, _ ->
                    openPermissionSettings(this, PERMISSIONS_REQUEST_SEND_SMS)
                }
                val dialog = builder.create()
                dialog.show()
                dialog.getButton(AlertDialog.BUTTON_POSITIVE).isAllCaps = false
                dialog.getButton(AlertDialog.BUTTON_NEGATIVE).isAllCaps = false
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    dialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(
                        resources.getColor(
                            R.color.continue_color,
                            null
                        )
                    )
                } else {
                    dialog.getButton(AlertDialog.BUTTON_POSITIVE)
                        .setTextColor(Color.parseColor(AppConstant.CONTINUE_BUTTON))
                }
            } else {
                requestPermissions(
                    arrayOf(
                        Manifest.permission.READ_SMS,
                        Manifest.permission.SEND_SMS,
                        Manifest.permission.RECEIVE_SMS
                    ), PERMISSIONS_REQUEST_SEND_SMS
                )
            }
            return false
        } else {
            return true
        }
    }

    /**
     * Request phone state permission for receiving SMS
     */
    private fun requestPhoneStatePermission(): Boolean {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && checkSelfPermission(Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(
                    this,
                    Manifest.permission.READ_PHONE_STATE
                )
            ) {
                // sees the explanation, try again to request the permission.
                val builder = AlertDialog.Builder(this)
                builder.setTitle(resources.getString(R.string.permission))
                builder.setMessage(resources.getString(R.string.phone_state_txt))
                builder.setCancelable(false)
                builder.setPositiveButton(resources.getString(R.string.continue_txt)) { _, _ ->
                    openPermissionSettings(this, PERMISSIONS_REQUEST_SEND_SMS)
                }
                val dialog = builder.create()
                dialog.show()
                dialog.getButton(AlertDialog.BUTTON_POSITIVE).isAllCaps = false
                dialog.getButton(AlertDialog.BUTTON_NEGATIVE).isAllCaps = false
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    dialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(
                        resources.getColor(
                            R.color.continue_color,
                            null
                        )
                    )
                } else {
                    dialog.getButton(AlertDialog.BUTTON_POSITIVE)
                        .setTextColor(Color.parseColor(AppConstant.CONTINUE_BUTTON))
                }
            } else {
                requestPermissions(
                    arrayOf(Manifest.permission.READ_PHONE_STATE),
                    PERMISSIONS_REQUEST_SEND_SMS
                )
            }
            return false
        } else {
            return true
        }
    }

    /**
     * Open permission forcefully
     */
    private fun openPermissionSettings(activity: Activity, requestCode: Int) {
        if (requestCode == PERMISSIONS_REQUEST_SEND_SMS) {
            val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
            val uri = Uri.parse("package:" + activity.packageName)
            intent.data = uri
            if (intent.resolveActivity(activity.packageManager) != null) {
                activity.startActivity(intent)
            }
        }
    }

    /**
     * Config keyboard visibility
     */
    private fun onKeyboardVisibilityChanged(opened: Boolean) {
        if (opened) {
            if (!isFilterMessage) {
                val animation = AnimationUtils.loadAnimation(this, R.anim.right_to_left)
                appContainerLayout!!.startAnimation(animation)
                if (chatDetails.isGroup == 1) {
                    appContainerLayout!!.visibility = View.GONE
                } else {
                    if (groupSize > 1) {
                        appContainerLayout!!.visibility = View.VISIBLE
                    } else {
                        appContainerLayout!!.visibility = View.GONE
                    }
                }
            }
        } else {
            appContainerLayout!!.visibility = View.GONE
        }
    }

    /**
     * Receiving messages from notification and update the UI
     */
    internal inner class NotificationReceiver : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            try {
                val receivedPackCode = intent.getIntExtra(AppConstant.PACKAGE_CODE_KEY, 0)
                val receivedType = intent.getStringExtra(AppConstant.NOTIFICATION_TYPE)
                if (receivedType == chatScreen) {
                    if (isFilterMessage) {
                        if (filterPackageCode == receivedPackCode) {
                            val chat = mChatsViewModel!!.getLastSingleChats(groupKey!!, packageCode)
                            chatKey = chat.chatKey
                            packageCode = chat.packageCode
                            chatList.add(chat)
                            if (isNativeAdsShowing) {
                                insertAds()
                            } else {
                                adapter.setDataChange(
                                    CentrlUtil.groupDataIntoHashMap(chatList),
                                    chatDetails.isGroup
                                )
                                rvRecyclerView!!.scrollToPosition(
                                    CentrlUtil.groupDataIntoHashMap(
                                        chatList
                                    ).size - 1
                                )
                            }
                            setSendButton(chat.packageCode)
                            mMutiChannelViewModel!!.deleteCount(groupKey!!)
                            mMutiChannelViewModel!!.deleteGroupAllPackageCode(chat.groupID)
                            loadNativeAds()
                        }
                    } else {
                        val chat = mChatsViewModel!!.getLastGroupChats(groupKey!!)
                        chatKey = chat.chatKey
                        packageCode = chat.packageCode
                        chatList.add(chat)
                        if (isNativeAdsShowing) {
                            insertAds()
                        } else {
                            adapter.setDataChange(
                                CentrlUtil.groupDataIntoHashMap(chatList),
                                chatDetails.isGroup
                            )
                            rvRecyclerView!!.scrollToPosition(
                                CentrlUtil.groupDataIntoHashMap(
                                    chatList
                                ).size - 1
                            )
                        }
                        setSendButton(chat.packageCode)
                        mMutiChannelViewModel!!.deleteCount(groupKey!!)
                        mMutiChannelViewModel!!.deleteGroupAllPackageCode(chat.groupID)
                    }
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    /**
     * Change icon for send button
     */
    private fun setSendButton(packCode: Int) {
        try {
            if (packCode != 0) {
                sendBtn!!.setImageResource(CentrlUtil.getChannelsLogo()[packCode]!!)
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun onDestroy() {
        isFilterMessage = false
        if (nReceiver !== null) {
            unregisterReceiver(nReceiver)
        }
        super.onDestroy()
    }

    override fun onStop() {
        PrefManager.getInstance(this)!!.saveBoolean(AppConstant.IS_CHAT_SCREEN, false)
        super.onStop()
    }

    /**
     * Select channel to send message from keyboard tray
     */
    override fun channelSelected(packCode: Int) {
        try {
            val getChatKey = mGroupChannelsViewModel!!.getReplyChatKey(groupKey!!, packCode)
            if (getChatKey.isNotEmpty()) {
                packageCode = packCode
                chatKey = getChatKey
                sendMessage()
                setSendButton(packCode)
            } else {
                CentrlSnackBar.showSnackBar(
                    snackbarCL!!,
                    resources.getString(R.string.add_channel_txt)
                )
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    /**
     * Menu options click handler
     */
    override fun onMenuItemClick(item: MenuItem?): Boolean {
        when (item!!.itemId) {
            R.id.action_profile -> {
                val intent = Intent(this, ProfileActivity::class.java)
                intent.putExtra(AppConstant.GROUP_KEY, groupKey)
                intent.putExtra(AppConstant.PARTICIPANTS, adapter.mChatTitle)
                startActivity(intent)
                return true
            }
            R.id.action_mute -> {
                if (isMute == 0) {
                    mGroupChannelsViewModel!!.updateMuteChat(1, groupKey!!)
                } else {
                    mGroupChannelsViewModel!!.updateMuteChat(0, groupKey!!)
                }
                return true
            }
            R.id.action_block -> {
                if (isBlocked) {
                    mBlockedContactsViewModel!!.deleteBlockedContact(groupKey!!)
                } else {
                    if (mBlockedContactsViewModel!!.getAllBlockedContactsList().size == 0) {
                        mBlockedContactsViewModel!!.insertBlockedContacts(
                            BlockedContactsEntity(
                                chatDetails.groupID,
                                chatDetails.chatKey,
                                chatDetails.title
                            )
                        )
                    } else {
                        val getChatBlocked =
                            mBlockedContactsViewModel!!.getBlockedContacts(groupKey!!)
                        if (getChatBlocked.groupID.equals(chatDetails.groupID, ignoreCase = true)) {
                            CentrlSnackBar.showSnackBar(
                                snackbarCL!!,
                                resources.getString(R.string.already_blocked)
                            )
                        } else {
                            mBlockedContactsViewModel!!.insertBlockedContacts(
                                BlockedContactsEntity(
                                    chatDetails.groupID,
                                    chatDetails.chatKey,
                                    chatDetails.title
                                )
                            )
                        }
                    }
                }
                return true
            }
            R.id.action_add_shortcut -> {
                try {
                    val chat = mGroupChannelsViewModel!!.getGroupDetails(groupKey!!)
                    createHomeShortcut(chat)
                } catch (e: java.lang.Exception) {
                    e.printStackTrace()
                }
                return true
            }
            else -> return false
        }
    }

    /**
     * This is use to create shotcut to home screen for particular chat.
     */
    private fun createHomeShortcut(chat: GroupChannelsEntity) {
        try {
            if (ShortcutManagerCompat.isRequestPinShortcutSupported(applicationContext)) {
                val intent = Intent(applicationContext, ChatsRoomActivity::class.java)
                intent.action = Intent.ACTION_MAIN
                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
                intent.putExtra(AppConstant.GROUP_KEY, groupKey)
                intent.putExtra(AppConstant.PACKAGE_CODE_KEY, packageCode)
                intent.putExtra(AppConstant.CHAT_KEY, chatKey)
                intent.putExtra(AppConstant.IS_FROM_NOTIFICATION, true)
                intent.putExtra(AppConstant.SHARED_TEXT_KEY, "")
                if (chat.iconPath.isEmpty()) {
                    val shortcutInfo = ShortcutInfoCompat.Builder(applicationContext, chat.groupID)
                        .setIntent(intent) // !!! intent's action must be set on oreo
                        .setShortLabel(chat.groupTitle)
                        .setIcon(
                            IconCompat.createWithResource(
                                applicationContext,
                                R.drawable.default_user_image
                            )
                        )
                        .build()
                    ShortcutManagerCompat.requestPinShortcut(applicationContext, shortcutInfo, null)
                } else {
                    val picBitmap = ImageUtility.decodeUri(
                        this,
                        Uri.fromFile(File(chat.groupIconPath)),
                        192
                    )
                    val shortcutInfo = ShortcutInfoCompat.Builder(applicationContext, chat.groupID)
                        .setIntent(intent) // !!! intent's action must be set on oreo
                        .setShortLabel(chat.groupTitle)
                        .setIcon(IconCompat.createWithBitmap(picBitmap))
                        .build()
                    ShortcutManagerCompat.requestPinShortcut(applicationContext, shortcutInfo, null)
                }
            } else {
                CentrlSnackBar.showSnackBar(snackbarCL!!, resources.getString(R.string.not_support))
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    /**
     * This is use to filter channel messages
     */
    override fun onMessageFilter(packCode: Int) {
        try {
            if (packCode == AppConstant.GROUP_CODE) {
                isFilterMessage = false
                if (packCode != 0) {
                    groupChatChoice!!.setImageResource(CentrlUtil.getChannelsLogo()[packCode]!!)
                }
                val lastChat = mChatsViewModel!!.getLastChat(groupKey!!, AppConstant.CHAT_SCREEN)
                packageCode = lastChat.packageCode
                refresh()
            } else {
                val getChatKey = mGroupChannelsViewModel!!.getReplyChatKey(groupKey!!, packCode)
                if (getChatKey.isEmpty()) {
                    CentrlSnackBar.showSnackBar(snackbarCL!!, resources.getString(R.string.add_channel_txt))
                    return
                }
                isFilterMessage = true
                filterPackageCode = packCode
                packageCode = packCode
                if (packCode != 0) {
                    sendBtn!!.setImageResource(CentrlUtil.getChannelsLogo()[packageCode]!!)
                    groupChatChoice!!.setImageResource(CentrlUtil.getChannelsLogo()[packageCode]!!)
                }
            }

            chatList.clear()
            if (isFilterMessage) {
                for (chat in mChatsViewModel!!.getSingleChatsList(groupKey!!, packageCode)) {
                    chatList.add(chat)
                }
            } else {
                for (chat in mChatsViewModel!!.getGroupChatsList(groupKey!!)) {
                    chatList.add(chat)
                }
            }
            if (isNativeAdsShowing) {
                insertAds()
            } else {
                adapter.setDataChange(
                    CentrlUtil.groupDataIntoHashMap(chatList),
                    chatDetails.isGroup
                )
                rvRecyclerView!!.scrollToPosition(CentrlUtil.groupDataIntoHashMap(chatList).size - 1)
            }
            refreshUI()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent?): Boolean {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if (isShowedMultiSelect) {
                refresh()
            } else {
                closeActivity()
            }
            return true
        }
        return super.onKeyDown(keyCode, event)
    }

    private fun closeActivity() {
        try {
            if (isFromBundle) {
                isFromBundle = false
                val nextScreen = Intent(this, InteractionsActivity::class.java)
                nextScreen.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                nextScreen.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
                nextScreen.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
                startActivity(nextScreen)
            }
            finish()
            PrefManager.getInstance(this)!!.saveStringValue(AppConstant.CHAT_GROUP_ID, "")
            mSharedText = ""
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 1) {
            if (resultCode == Activity.RESULT_OK) {
                fetchAllContact()
                CentrlSnackBar.showSnackBar(
                    snackbarCL!!,
                    resources.getString(R.string.contact_added)
                )
            }
            if (resultCode == Activity.RESULT_CANCELED) {
                fetchAllContact()
                CentrlSnackBar.showSnackBar(
                    snackbarCL!!,
                    resources.getString(R.string.contact_cancelled)
                )
            }
        }
    }

    /**
     * Fetch added contact details from device and update to database
     */
    private fun fetchAllContact() {
        try {
            val contact = CentrlUtil.getContactDetailsByNumber(mPhoneNumber, this)
            val uniqueKey = NotificationData.md5("${contact.name}@$packageCode")
            val groupSize = mGroupChannelsViewModel!!.getGroupList(groupKey!!).size
            if (groupSize == 1) {
                for (lastChats in mChatsViewModel!!.getAllChatsList(AppConstant.CHAT_SCREEN)) {
                    if (uniqueKey.equals(lastChats.chatKey, ignoreCase = true)) {
                        groupKey = lastChats.groupID
                    }
                }
                mChatsViewModel!!.updateTitleChat(
                    groupKey!!,
                    contact.name!!,
                    uniqueKey,
                    mPhoneNumber
                )
                mGroupChannelsViewModel!!.updateTitleGroup(
                    contact.name!!,
                    groupKey!!,
                    uniqueKey,
                    mPhoneNumber
                )
                chatKey = uniqueKey
            } else {
                mChatsViewModel!!.updateTitleChat(
                    groupKey!!,
                    contact.name!!,
                    uniqueKey,
                    mPhoneNumber
                )
                mGroupChannelsViewModel!!.updateTitleGroup(
                    contact.name!!,
                    groupKey!!,
                    uniqueKey,
                    mPhoneNumber
                )
                chatKey = uniqueKey
            }
            mGroupChannelsViewModel!!.groupDetails(groupKey!!)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun insertAdsInMenuItems() {
        try {
            if (mNativeAds.size <= 0) {
                return
            }
            var index = 0
            val chatList = CentrlUtil.groupDataIntoHashMap(chatList)
            if (chatList.size > 5) {
                for (i in 0 until chatList.size) {
                    if (i % 6 == 0) {
                        if (index == 4) {
                            index = 0
                        }
                        val ads = AdsObject()
                        ads.ads = mNativeAds[index]
                        chatList.add(i, ads)
                        index++
                        //Show the nth element
                    }
                    //Display each table result normally
                }
                val ads = AdsObject()
                ads.ads = mNativeAds[0]
                chatList.add(chatList.size - 4, ads)
            }

            // Refresh adapter
            adapter.setDataChange(chatList, chatDetails.isGroup)
            rvRecyclerView!!.scrollToPosition(chatList.size - 1)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private var indexs = 0
    private fun insertAds() {
        try {
            if (mNativeAds.size <= 0) {
                adapter.setDataChange(
                    CentrlUtil.groupDataIntoHashMap(chatList),
                    chatDetails.isGroup
                )
                rvRecyclerView!!.scrollToPosition(CentrlUtil.groupDataIntoHashMap(chatList).size - 1)
                return
            }
            var index = 0
            val chatList = CentrlUtil.groupDataIntoHashMap(chatList)
            if (chatList.size > 5) {
                for (i in 0 until chatList.size) {
                    if (i % 6 == 0) {
                        if (index == 4) {
                            index = 0
                        }
                        val ads = AdsObject()
                        ads.ads = mNativeAds[index]
                        chatList.add(i, ads)
                        index++
                        //Show the nth element
                    }
                    //Display each table result normally
                }

                if (indexs == 4) {
                    indexs = 0
                }
                val ads = AdsObject()
                ads.ads = mNativeAds[indexs]
                chatList.add(chatList.size - 4, ads)
                // Refresh adapter
                adapter.setDataChange(chatList, chatDetails.isGroup)
                rvRecyclerView!!.scrollToPosition(chatList.size - 1)
                indexs++
            } else {
                adapter.setDataChange(
                    CentrlUtil.groupDataIntoHashMap(Companion.chatList),
                    chatDetails.isGroup
                )
                rvRecyclerView!!.scrollToPosition(CentrlUtil.groupDataIntoHashMap(Companion.chatList).size - 1)
            }

        } catch (e: Exception) {
            e.printStackTrace()
        }
    }


    private var currentNativeAd: UnifiedNativeAd? = null
    private fun loadNativeAds() {
        try {
            currentNativeAd?.destroy()
            mNativeAds.clear()
            // The AdLoader used to load ads.
            var adLoader: AdLoader? = null
            val builder = AdLoader.Builder(this, resources.getString(R.string.admob_native_unit_id))

            val videoOptions = VideoOptions.Builder()
                .setStartMuted(true)
                .build()

            val adOptions = NativeAdOptions.Builder()
                .setVideoOptions(videoOptions)
                .build()

            builder.withNativeAdOptions(adOptions)

            adLoader =
                builder.forUnifiedNativeAd { unifiedNativeAd -> // A native ad loaded successfully, check if the ad loader has finished loading
                    // and if so, insert the ads into the list.
                    currentNativeAd = unifiedNativeAd
                    mNativeAds.add(unifiedNativeAd)
                    if (!adLoader!!.isLoading) {
                        insertAdsInMenuItems()
                    }
                }.withAdListener(
                    object : AdListener() {
                        override fun onAdFailedToLoad(errorCode: Int) {
                            // A native ad failed to load, check if the ad loader has finished loading
                            // and if so, insert the ads into the list.
                            if (!adLoader!!.isLoading) {
                                insertAdsInMenuItems()
                            }
                        }
                    }).build()

            // Load the Native ads.
            adLoader.loadAds(AdRequest.Builder().build(), mNumberOfAds)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun onClick(position: Int, packageCode: Int, chatKey: String) {
        try {
            if (adapter.lastSelectedPosition.size == 0) {
                isMultiSelect = false
            }
            if (isMultiSelect) {
                adapter.multiSelectList(position)
                onMultiSelect(adapter.mChatGroupId)
            } else {
                adapter.selectList(-1)
                val packageName = installedAppsViewModel!!.getInstalledPackageName(packageCode)
                if (packageName.isNotEmpty()) {
                    pendingIntentManager.goOtherApps(
                        chatKey,
                        installedAppsViewModel!!.getInstalledPackageName(packageCode)
                    )
                } else {
                    CentrlSnackBar.showSnackBar(
                        snackbarCL!!,
                        resources.getString(R.string.package_not_installed)
                    )
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun onLongClick(position: Int, packageCode: Int, chatKey: String) {
        try {
            isMultiSelect = true
            adapter.multiSelectList(position)
            onMultiSelect(adapter.mChatGroupId)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }
}