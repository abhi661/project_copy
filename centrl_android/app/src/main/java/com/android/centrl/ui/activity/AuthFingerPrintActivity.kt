package com.android.centrl.ui.activity

import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.view.KeyEvent
import androidx.annotation.RequiresApi
import androidx.databinding.DataBindingUtil
import com.android.centrl.R
import com.android.centrl.databinding.ActivityAuthFingerPrintBinding
import com.android.centrl.utils.AppConstant
import me.aflak.libraries.callback.FingerprintSecureCallback
import me.aflak.libraries.utils.FingerprintToken
import me.aflak.libraries.view.Fingerprint

/**
 * AuthFingerprint activity for unlock Centrl
 */
@RequiresApi(api = Build.VERSION_CODES.M)
class AuthFingerPrintActivity : BaseActivity(), FingerprintSecureCallback {

    private lateinit var binding: ActivityAuthFingerPrintBinding
    private lateinit var mClassName: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_auth_finger_print)

        binding.authFingerprintStatusTxt.text = ""
        binding.authLockedTxt.text = resources.getString(R.string.fingerprint_auth_locked)
        binding.authLockedImg.setImageResource(R.drawable.ic_locked)

    }

    override fun onStart() {
        super.onStart()
        val bundle = intent.extras
        if (bundle != null) {
            mClassName = bundle.getString(AppConstant.CLASS_NAME).toString()
        }
        binding.authLockedTxt.text = resources.getString(R.string.fingerprint_auth_locked)
        binding.authLockedImg.setImageResource(R.drawable.ic_locked)
        if (Fingerprint.isAvailable(this)) {
            try {
                binding.authFingerprint.callback(this, "KeyName2")
                    .authenticate()
            } catch (e: Exception) {
                e.printStackTrace()
            }
        } else {
            if(!preferences!!.getString(AppConstant.PIN_LOCK, null).isNullOrEmpty()){
                val intent = Intent(this@AuthFingerPrintActivity, AuthPinLockActivity::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP)
                intent.putExtra(AppConstant.CLASS_NAME, mClassName)
                startActivity(intent)
                overridePendingTransition(R.anim.push_down_in, 0)
                finish()
            }
        }
    }

    override fun onResume() {
        super.onResume()
        binding.authFingerprint.animateOnFingerprint()
    }

    override fun onPause() {
        super.onPause()
        binding.authFingerprint.animateOffFingerprint()
        binding.authFingerprintStatusTxt.text = ""
    }

    override fun onStop() {
        super.onStop()
        binding.authFingerprint.animateOffFingerprint()
        binding.authFingerprintStatusTxt.text = ""
    }
    /**
     * Fingerprint authentication success handling
     */
    override fun onAuthenticationSucceeded() {
        binding.authLockedTxt.text = resources.getString(R.string.fingerprint_auth_unlocked)
        binding.authLockedImg.setImageResource(R.drawable.ic_unlocked)
        binding.authFingerprintStatusTxt.text = ""
        if (mClassName.equals(AppConstant.CLASS_NAME_SPLASH, ignoreCase = true)) {
            val intent = Intent(this@AuthFingerPrintActivity, InteractionsActivity::class.java)
            startActivity(intent)
        }
        finish()
        exitActivityAnimation()
    }
    /**
     * Fingerprint authentication failed handling
     */
    override fun onAuthenticationFailed() {
        binding.authFingerprintStatusTxt.text = resources.getString(R.string.fingerprint_failure)
    }
    /**
     * Fingerprint authentication new enrolled handling
     */
    override fun onNewFingerprintEnrolled(token: FingerprintToken) {}
    /**
     * Fingerprint authentication error handling
     */
    override fun onAuthenticationError(errorCode: Int, error: String) {
       // LogUtil.mLOGE("onAuthenticationError",""+errorCode)
        if(errorCode == 7){
            binding.authFingerprintStatusTxt.text = resources.getString(R.string.fingerprint_failure_attempt)
        } else {
            binding.authFingerprintStatusTxt.text = error
        }
    }

    override fun onAuthenticationHelp(errorCode: Int, error: String?) {
       // LogUtil.mLOGE("onAuthenticationHelp",""+errorCode)
        binding.authFingerprintStatusTxt.text = error
    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            val intent = Intent(this@AuthFingerPrintActivity, FinishActivity::class.java)
            intent.action = Intent.ACTION_MAIN
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(intent)
            finish()
            return true
        }
        return super.onKeyDown(keyCode, event)
    }
}