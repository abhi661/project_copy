package com.android.centrl.services

import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import androidx.core.app.JobIntentService
import com.android.centrl.notificationservice.PendingIntentManager
import com.android.centrl.persistence.entity.InstalledAppsEntity
import com.android.centrl.persistence.entity.KeyboardAppsTrayEntity
import com.android.centrl.persistence.repositry.PendingIntentManagerRepo
import com.android.centrl.utils.CentrlUtil

/**
 * Get all installed apps
 */
class ChannelsPackageRefreshService: JobIntentService() {

    companion object {
        /**
         * Unique job ID for this service.
         */
        private const val JOB_ID = 3
        val TAG: String = com.android.centrl.utils.LogUtil.makeLogTag(ChannelsPackageRefreshService::class.java)
        fun enqueueWork(context: Context, intent: Intent) {
            enqueueWork(context, ChannelsPackageRefreshService::class.java, JOB_ID, intent)
        }
    }

    val context: Context get() = applicationContext
    private var pendingIntentManagerRepo: PendingIntentManagerRepo? = null

    override fun onCreate() {
        super.onCreate()
        this.pendingIntentManagerRepo = PendingIntentManagerRepo(context)
    }
    override fun onHandleWork(intent: Intent) {
        try{
            val mAllPackages = pendingIntentManagerRepo!!.getAllKeyboardAppTrayPackName()
            val mRemovedAllPackages = pendingIntentManagerRepo!!.getRemovedAllKeyboardAppPackName()
            val pm = context.packageManager
            //get a list of installed apps.
            val packages = pm.getInstalledApplications(PackageManager.GET_META_DATA)
            for (packageInfo in packages) {
                val installedPackages = packageInfo.packageName
                if (CentrlUtil.getAppPackageList(context).contains(installedPackages)) {
                    if (mRemovedAllPackages.size == 0) {
                        if (!mAllPackages.contains(installedPackages)) {
                            pendingIntentManagerRepo!!.insertKeyboardAppTray(KeyboardAppsTrayEntity(CentrlUtil.getPackageCodeList(context)[installedPackages]!!, installedPackages))
                        }
                    } else {
                        if (!mRemovedAllPackages.contains(CentrlUtil.getPackageCodeList(context)[installedPackages]!!)) {
                            if (!mAllPackages.contains(installedPackages)) {
                                pendingIntentManagerRepo!!.insertKeyboardAppTray(KeyboardAppsTrayEntity(CentrlUtil.getPackageCodeList(context)[installedPackages]!!, installedPackages))
                            }
                        }
                    }
                }
            }
            Thread.sleep(100)
            PendingIntentManager.getInstance(context).getPackageList()
            val mAllInstalledPackages = pendingIntentManagerRepo!!.getAllInstalledPackageName()
            for (packageInfo in packages) {
                val installedPackages = packageInfo.packageName
                if (CentrlUtil.getAppPackageList(context).contains(installedPackages)) {
                    if (!mAllInstalledPackages.contains(installedPackages)) {
                        pendingIntentManagerRepo!!.insertInstalledApps(InstalledAppsEntity(CentrlUtil.getPackageCodeList(context)[installedPackages]!!, installedPackages))
                    }
                }
            }
        }catch (e: Exception){
            e.printStackTrace()
        }
    }
}