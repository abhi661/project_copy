package com.android.centrl.persistence.datasource

import com.android.centrl.persistence.entity.BlockedContactsEntity
import io.reactivex.Completable
import io.reactivex.Flowable

/**
 * Access point for managing blocked contacts data.
 */
interface BlockedContactsDataSource {
    /**
     * Inserts the blocked contacts into the data source, or, if this is an existing blocked contacts, updates it.
     *
     * @param blockedContactsEntity the blocked contacts to be inserted or updated.
     */
    fun insertBlockedContacts(blockedContactsEntity: BlockedContactsEntity): Completable

    /**
     * Gets the blocked contacts from the data source.
     *
     * @return the blocked contacts from the data source.
     */
    fun getAllBlockedList(): Flowable<List<BlockedContactsEntity>>

    /**
     * Gets the blocked contacts from the data source.
     *
     * @return the blocked contacts from the data source.
     */
    fun getBlockedDetails(groupID: String): Flowable<BlockedContactsEntity>


    /**
     * Deletes all blocked contacts from the data source.
     */
    fun deleteBlockedContact(groupID: String)

}