package com.android.centrl.ui.activity

import android.os.Bundle

/**
 * Finish the activity
 */
class FinishActivity : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        finish()
    }
}