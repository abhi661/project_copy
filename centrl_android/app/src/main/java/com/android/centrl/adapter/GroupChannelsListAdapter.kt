package com.android.centrl.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.android.centrl.R
import com.android.centrl.adapter.GroupChannelsListAdapter.GroupChannelViewHolder
import com.android.centrl.persistence.entity.GroupChannelsEntity
import com.android.centrl.utils.CentrlUtil
import java.util.*

/**
 * Group Channels list adapter
 */
class GroupChannelsListAdapter(context: Context, chatArrayList: ArrayList<GroupChannelsEntity>?, removeIdentityInterface: RemoveChannelInterface) : RecyclerView.Adapter<GroupChannelViewHolder>() {

    private var identityList: ArrayList<GroupChannelsEntity>? = null
    var context: Context
    private var removeIdentityInterface: RemoveChannelInterface

    init {
        identityList = chatArrayList
        this.context = context
        this.removeIdentityInterface = removeIdentityInterface
    }
    // Inflates the appropriate layout according to the ViewType.
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GroupChannelViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val view = layoutInflater.inflate(R.layout.group_identity_list, parent, false)
        return GroupChannelViewHolder(view)
    }
    // Passes the message object to a ViewHolder so that the contents can be bound to UI.
    override fun onBindViewHolder(holder: GroupChannelViewHolder, position: Int) {
        val chat = identityList!![position]
        // If the message is a received message.
        if (chat.title.isNotEmpty()) {
            holder.identityTitleTxt!!.text = chat.title
        }
        if(chat.packageCode != 0){
            holder.identityImageView!!.setBackgroundResource(CentrlUtil.getChannelsLogo()[chat.packageCode]!!)
        }
        holder.identityNameTxt!!.text = CentrlUtil.getChannelsNameHashMap()[chat.packageCode]
        if (position == 0) {
            if (identityList!!.size == 1) {
                holder.identityRemoveImageView!!.visibility = View.GONE
            } else {
                holder.identityRemoveImageView!!.visibility = View.VISIBLE
            }
        } else {
            holder.identityRemoveImageView!!.visibility = View.VISIBLE
        }
        holder.identityRemoveImageView!!.setOnClickListener {
            removeIdentityInterface.removeIdentity(chat)
        }
    }

    override fun getItemCount(): Int {
        return identityList!!.size
    }
    /**
     * Adapter view holder
     */
    class GroupChannelViewHolder(itemView: View?) : RecyclerView.ViewHolder(itemView!!) {
        var identityTitleTxt: TextView? = null
        private var identityNumberTxt: TextView? = null
        var identityNameTxt: TextView? = null
        var identityImageView: ImageView? = null
        var identityRemoveImageView: ImageView? = null

        init {
            if (itemView != null) {
                identityTitleTxt = itemView.findViewById<View>(R.id.identity_title_text_view) as TextView
                identityNumberTxt = itemView.findViewById<View>(R.id.identity_number_text_view) as TextView
                identityNameTxt = itemView.findViewById<View>(R.id.identity_type_txt) as TextView
                identityImageView = itemView.findViewById<View>(R.id.identity_image) as ImageView
                identityRemoveImageView = itemView.findViewById<View>(R.id.remove_identity) as ImageView
            }
        }
    }
    /**
     * This will update the list
     */
    fun updateList(chatArrayList: ArrayList<GroupChannelsEntity>?) {
        identityList = chatArrayList
        //now, tell the adapter about the update
        notifyDataSetChanged()
    }
    /**
     * Remove channel interface
     */
    interface RemoveChannelInterface {
        fun removeIdentity(chat: GroupChannelsEntity)
    }
}