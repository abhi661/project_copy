package com.android.centrl.notificationservice;

import android.app.Notification;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Icon;
import android.os.Build;
import android.service.notification.StatusBarNotification;
import android.text.TextUtils;

import androidx.annotation.RequiresApi;
import androidx.core.content.ContextCompat;

import com.android.centrl.R;
import com.android.centrl.utils.ImageUtils;
import com.android.centrl.persistence.entity.ChatsEntity;
import com.android.centrl.persistence.entity.GroupChannelsEntity;
import com.android.centrl.persistence.repositry.ChatsRepository;
import com.android.centrl.persistence.repositry.GroupChannelsRepository;
import com.android.centrl.utils.AppConstant;
import com.android.centrl.utils.DateUtil;
import com.android.centrl.utils.ImageUtility;
import com.android.centrl.utils.LogUtil;

import java.io.File;
import java.io.FileOutputStream;

import static android.content.pm.PackageManager.GET_META_DATA;
import static androidx.core.app.NotificationCompat.EXTRA_BIG_TEXT;
import static androidx.core.app.NotificationCompat.EXTRA_CONVERSATION_TITLE;
import static androidx.core.app.NotificationCompat.EXTRA_LARGE_ICON;
import static androidx.core.app.NotificationCompat.EXTRA_SMALL_ICON;
import static androidx.core.app.NotificationCompat.EXTRA_SUB_TEXT;
import static androidx.core.app.NotificationCompat.EXTRA_TEMPLATE;
import static androidx.core.app.NotificationCompat.EXTRA_TEXT;
import static androidx.core.app.NotificationCompat.EXTRA_TITLE;
import static com.android.centrl.utils.AppConstant.GMAIL_CODE;
import static com.android.centrl.utils.AppConstant.KAKAO_CODE;
import static com.android.centrl.utils.AppConstant.LINE_CODE;
import static com.android.centrl.utils.AppConstant.MESSENGER_CODE;
import static com.android.centrl.utils.AppConstant.OUTLOOK_CODE;
import static com.android.centrl.utils.AppConstant.SLACK_CODE;
import static com.android.centrl.utils.AppConstant.TELEGRAM_CODE;
import static com.android.centrl.utils.AppConstant.WHATSAPP_CODE;

/**
 * Notifications catch service model
 */
public class NotificationCatchServiceModel {

    private Context context;
    private String contentTemp;
    private String notiAtTemp;
    private GroupChannelsRepository groupChannelsRepository;
    private ChatsRepository chatsRepository;

    public NotificationCatchServiceModel(Context context) {
        this.context = context;
        groupChannelsRepository = new GroupChannelsRepository(context);
        chatsRepository = new ChatsRepository(context);
    }

    /**
     * Get notification sub title
     */
    private String getSubTitle(Notification notification) {
        CharSequence charSequence = notification.extras.getCharSequence(EXTRA_CONVERSATION_TITLE);
        return (charSequence != null ? charSequence.toString().trim() : "").trim();
    }

    /**
     * Get notification sub title
     */
    private String getSubText(Notification notification) {
        CharSequence charSequence = notification.extras.getCharSequence(EXTRA_SUB_TEXT);
        return (charSequence != null ? charSequence.toString().trim() : "").trim();
    }

    /**
     * Get notification title
     */
    private String getTitle(Notification notification) {
        CharSequence charSequence = notification.extras.getCharSequence(EXTRA_TITLE);
        return (charSequence != null ? charSequence.toString().trim() : "").trim();
    }

    /**
     * Parse message from notification
     */
    @RequiresApi(api = Build.VERSION_CODES.KITKAT_WATCH)
    public NotificationData getParserNotification(StatusBarNotification statusBarNotification, int packageCode) {
        String str = "FILTER";
        LogUtil.LOGI(str, "************************************* START ***********************************");
        Notification notification = statusBarNotification.getNotification();
        String string = notification.extras.getString(EXTRA_TEMPLATE);
        StringBuilder sb = new StringBuilder();
        sb.append("style : ");
        sb.append(string);
        NotificationData sVar = new NotificationData();
        sVar.appId = statusBarNotification.getId();
        sVar.packageName = statusBarNotification.getPackageName();
        sVar.packCode = packageCode;
        sVar.phoneNumber = "";
        if (packageCode == OUTLOOK_CODE) {
            int id = statusBarNotification.getId();
            if (id == 2) {
                return null;
            }
        }
        if (packageCode == GMAIL_CODE) {
            int id = statusBarNotification.getId();
            if (id == 0) {
                return null;
            }
        }
        if (packageCode == SLACK_CODE) {
            int id = statusBarNotification.getId();
            String mId = String.valueOf(id);
            if (mId.startsWith("-")) {
                return null;
            }
        }
        if (packageCode == WHATSAPP_CODE) {
            String tag = statusBarNotification.getTag();
            if (tag == null) {
                return null;
            }
            sVar.sortKey = notification.getSortKey();
        } else {
            sVar.sortKey = null;
        }
        if (packageCode == LINE_CODE || packageCode == KAKAO_CODE) {
            String tag = statusBarNotification.getTag();
            if (tag == null) {
                return null;
            }
        }

        if (packageCode <= AppConstant.TOTAL_CHATS_APP) {
            sVar.notiType = AppConstant.CHAT_SCREEN;
        } else {
            sVar.notiType = AppConstant.MAIL_SCREEN;
        }
        long j = notification.when;
        if (j == 0) {
            j = statusBarNotification.getPostTime();
        }
        long j2 = j;
        sVar.notiAt = DateUtil.INSTANCE.getDbNotiAt(j2);
        sVar.title = getTitle(notification);
        sVar.content = getContent(statusBarNotification.getPackageName(), notification, string);
        if (packageCode == AppConstant.LINE_CODE || packageCode == KAKAO_CODE) {
            sVar.subTitle = getSubText(notification);
        } else {
            sVar.subTitle = getSubTitle(notification);
        }
        if (packageCode == MESSENGER_CODE) {
            String tag = statusBarNotification.getTag();
            if (tag != null) {
                if (tag.contains("GROUP")) {
                    sVar.subTitle = "1";
                } else {
                    sVar.subTitle = "";
                }
            }
        }
        if (sVar.subTitle.isEmpty()) {
            sVar.isGroupChat = 0;
            if (packageCode == AppConstant.SKYPE_CODE) {
                if (getTitle(notification).contains("new conversation")) {
                    sVar.title = getTitle(notification);
                } else {
                    if (getTitle(notification).contains("unread messages")) {
                        String tempStr = getTitle(notification);
                        String[] splitStr = tempStr.split(":");
                        sVar.title = splitStr[1].trim();
                    } else {
                        String tempTitle = getTitle(notification);
                        if (tempTitle.contains("in")) {
                            sVar.isGroupChat = 1;
                            String[] tempArray = tempTitle.split("in");

                            String groupTitle = tempArray[1].replace("\"", "");
                            String fromTitle = tempArray[0].trim();
                            StringBuilder sbContent = new StringBuilder();
                            sbContent.append(fromTitle);
                            String str2 = "@@";
                            sbContent.append(str2);
                            sbContent.append(" ");
                            sbContent.append(sVar.content);
                            sVar.content = sbContent.toString();
                            sVar.title = groupTitle;
                        } else {
                            sVar.isGroupChat = 0;
                            sVar.title = getTitle(notification);
                        }
                    }
                }

            }

            if (packageCode == AppConstant.SLACK_CODE) {
                if (getTitle((notification)).startsWith("(")) {
                    String title = getTitle(notification);
                    String[] titleAry = title.split(" ");
                    sVar.title = titleAry[1];
                } else {
                    sVar.title = getTitle(notification);
                }

                if (sVar.title.contains("#")) {
                    sVar.isGroupChat = 1;
                    sVar.content = sVar.content.replaceFirst(":", "@@");
                } else {
                    sVar.isGroupChat = 0;
                }

            }

            if (packageCode == TELEGRAM_CODE) {
                String temTitle = getTitle(notification);
                if (temTitle.contains("(")) {
                    String[] temp = temTitle.split("()");
                    sVar.title = temp[0];
                } else {
                    sVar.title = getTitle(notification);
                }
            }

            if (packageCode == AppConstant.WECHAT_CODE) {
                String tempContent = getContent(statusBarNotification.getPackageName(), notification, string);
                if (tempContent.startsWith("[")) {
                    String[] titleAry = tempContent.split(":");
                    sVar.content = titleAry[1];
                } else {
                    sVar.content = tempContent;
                }
            }

            if (packageCode == AppConstant.TELEGRAM_CODE) {
                String tempContent = getContent(statusBarNotification.getPackageName(), notification, string);
                if (tempContent.contains(":")) {
                    String[] titleAry = tempContent.split(":");
                    sVar.content = titleAry[1];
                } else {
                    sVar.content = tempContent;
                }
            }

            if (packageCode == AppConstant.INSTAGRAM_CODE) {
                String tempContent = getContent(statusBarNotification.getPackageName(), notification, string);
                if (tempContent.contains(":")) {

                    String[] titleAry = tempContent.split(":");
                    if (titleAry.length == 2) {
                        sVar.content = titleAry[1];
                        if (titleAry[0].contains(" to ")) {
                            sVar.isGroupChat = 1;
                            String[] titleArys = titleAry[0].split("to");
                            sVar.title = titleArys[1].replaceFirst(" ", "");
                            String msgFrom = titleArys[0];
                            StringBuilder sbContent = new StringBuilder();
                            sbContent.append(msgFrom.trim());
                            String str2 = "@@";
                            sbContent.append(str2);
                            sbContent.append(" ");
                            sbContent.append(sVar.content);
                            sVar.content = sbContent.toString();
                        }
                    } else {
                        sVar.content = titleAry[2];
                    }
                } else {
                    sVar.content = tempContent;
                }
            }

            if (packageCode == AppConstant.REDDIT_CODE && sVar.title.contains(" to ")) {
                sVar.isGroupChat = 1;
                String[] titleAry = getTitle(notification).split("to");
                sVar.title = titleAry[1].replaceFirst(" ", "");
                String msgFrom = titleAry[0];
                StringBuilder sbContent = new StringBuilder();
                sbContent.append(msgFrom.trim());
                String str2 = "@@";
                sbContent.append(str2);
                sbContent.append(" ");
                sbContent.append(sVar.content);
                sVar.content = sbContent.toString();
            }

            if (packageCode == AppConstant.KIK_CODE && sVar.content.contains(":")) {
                sVar.isGroupChat = 1;
                sVar.content = sVar.content.replaceFirst(":", "@@");
            }
            sVar.groupTitle = sVar.title;
        } else {
            if (packageCode == AppConstant.SKYPE_CODE) {
                if (getTitle(notification).contains("new conversation")) {
                    sVar.title = getTitle(notification);
                } else {
                    if (getTitle(notification).contains("unread messages")) {
                        String tempStr = getTitle(notification);
                        String[] splitStr = tempStr.split(":");
                        String tempTitle = splitStr[0].replace("(", "@ ").split("@")[0].trim();
                        String tempTitle1 = splitStr[1].trim();
                        if (tempTitle.equalsIgnoreCase(tempTitle1)) {
                            sVar.isGroupChat = 0;
                            sVar.title = tempTitle;
                        } else {
                            sVar.isGroupChat = 1;
                            sVar.title = tempTitle;
                            StringBuilder sbContent = new StringBuilder();
                            sbContent.append(tempTitle1);
                            String str2 = "@@";
                            sbContent.append(str2);
                            sbContent.append(" ");
                            sbContent.append(sVar.content);
                            sVar.content = sbContent.toString();
                        }

                    } else {
                        sVar.title = getTitle(notification);
                    }
                }
                sVar.groupTitle = sVar.title;

            } else {
                sVar.isGroupChat = 1;
                if (packageCode == MESSENGER_CODE) {
                    sVar.title = getTitle(notification);
                    sVar.content = getContent(statusBarNotification.getPackageName(), notification, string).replace(":", "@@");
                } else if (packageCode == KAKAO_CODE) {
                    sVar.title = getSubText(notification);
                    StringBuilder sbContent = new StringBuilder();
                    sbContent.append(getTitle(notification));
                    String str2 = "@@";
                    sbContent.append(str2);
                    sbContent.append(" ");
                    sbContent.append(sVar.content);

                    sVar.content = sbContent.toString();
                } else {
                    if (getTitle(notification).contains("(")) {
                        String[] titleAry = getTitle(notification).replace("(", "@").split("@");
                        sVar.title = titleAry[0];
                    } else {
                        if (packageCode == AppConstant.LINE_CODE) {
                            sVar.title = getSubText(notification);
                        } else {
                            sVar.title = getSubTitle(notification);
                        }
                    }
                    String msgFrom;
                    String tempTitle = getTitle(notification);
                    if (tempTitle.contains(":")) {
                        String[] titleAry = tempTitle.split(":");
                        msgFrom = titleAry[1];
                    } else {
                        msgFrom = getTitle(notification);
                    }
                    StringBuilder sbContent = new StringBuilder();
                    sbContent.append(msgFrom.trim());
                    String str2 = "@@";
                    sbContent.append(str2);
                    sbContent.append(" ");
                    sbContent.append(sVar.content);

                    sVar.content = sbContent.toString();
                }
                sVar.groupTitle = sVar.title;
            }
        }

        if (!checkDuplicate(sVar)) {
            if (packageCode == WHATSAPP_CODE) {
                if (isValidMobile(getTitle(notification))) {
                    sVar.phoneNumber = getTitle(notification).replace(" ", "");
                    sVar.title = getTitle(notification).replace(" ", "");
                }
            }
            if (sVar.isGroupChat == 0) {
                sVar.uniqueKey = NotificationData.md5(sVar.title.toLowerCase() + "@" + sVar.isGroupChat + packageCode);
            } else {
                if(packageCode == AppConstant.DISCORD_CODE){
                    sVar.uniqueKey = NotificationData.md5(sVar.title.toLowerCase() + "@" + sVar.isGroupChat + packageCode);
                } else {
                    sVar.uniqueKey = NotificationData.md5(statusBarNotification.getKey() + "@" + sVar.isGroupChat + packageCode);
                }
            }

            if (packageCode == GMAIL_CODE || packageCode == OUTLOOK_CODE) {
                sVar.mailKey = NotificationData.md5(sVar.title.toLowerCase() + "@" + packageCode + "@" + sVar.content);
            } else {
                sVar.mailKey = "";
            }
            GroupChannelsEntity chat = groupChannelsRepository.getGroupKey(sVar.uniqueKey, packageCode);
            String chatKey = chat.getGroupID();
            if (chatKey.isEmpty()) {
                if (sVar.isGroupChat == 0) { // single chats already exist and auto merge
                    if (packageCode == GMAIL_CODE || packageCode == OUTLOOK_CODE) {
                        for (ChatsEntity lastChats : chatsRepository.getAllChatsList(AppConstant.MAIL_SCREEN)) {
                            if (lastChats.getTitle().equalsIgnoreCase(sVar.title)) {
                                sVar.groupKey = lastChats.getGroupID();
                                sVar.groupIconPath = lastChats.getGroupIconPath();
                            }
                        }
                    } else {
                        for (ChatsEntity lastChats : chatsRepository.getAllChatsList(AppConstant.CHAT_SCREEN)) {
                            if (lastChats.getTitle().equalsIgnoreCase(sVar.title)) {
                                sVar.groupKey = lastChats.getGroupID();
                                sVar.groupIconPath = lastChats.getGroupIconPath();
                            }
                        }
                    }
                }
                // Group chat and single chat new conversation
                if (sVar.iconPath == null) {
                    if (packageCode == TELEGRAM_CODE || packageCode == OUTLOOK_CODE) {
                        Bitmap icon = BitmapFactory.decodeResource(context.getResources(), R.drawable.default_user_image);
                        Bitmap circularBitmap = ImageUtils.getCircularBitmap(icon);
                        sVar.iconPath = saveTelegram(circularBitmap, sVar.uniqueKey);
                    } else {
                        sVar.iconPath = getNotificationImage(getBitmapImage(notification, sVar), sVar.uniqueKey);
                    }
                }
                if (sVar.groupKey == null) {
                    sVar.groupKey = sVar.uniqueKey;
                }
                if (sVar.groupIconPath == null) {
                    sVar.groupIconPath = sVar.iconPath;
                }

            } else { // Chats already exist
                sVar.groupKey = chat.getGroupID();
                int groupSize = groupChannelsRepository.getGroupList(chat.getGroupID()).size();
                if (chat.getEdit() == 1) {
                    sVar.iconPath = chat.getIconPath();
                    sVar.groupIconPath = chat.getGroupIconPath();
                    sVar.groupTitle = chat.getGroupTitle();
                } else {
                    sVar.groupTitle = sVar.title;
                    if (sVar.iconPath == null) {
                        if (packageCode == TELEGRAM_CODE || packageCode == OUTLOOK_CODE) {
                            Bitmap icon = BitmapFactory.decodeResource(context.getResources(), R.drawable.default_user_image);
                            Bitmap circularBitmap = ImageUtils.getCircularBitmap(icon);
                            sVar.iconPath = saveTelegram(circularBitmap, sVar.uniqueKey);
                        } else {
                            sVar.iconPath = getNotificationImage(getBitmapImage(notification, sVar), sVar.uniqueKey);
                        }
                    }

                    if (groupSize == 1) {
                        sVar.groupIconPath = sVar.iconPath;
                    } else {
                        sVar.groupIconPath = chat.getGroupIconPath();
                    }
                }
            }

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                sVar.backGroundColor = getColor(notification);
            }
            sVar.isRead = false;
            this.contentTemp = sVar.content;
            this.notiAtTemp = sVar.notiAt;
            StringBuilder sb4 = new StringBuilder();
            sb4.append(sVar.notiAt);
            String str2 = ", ";
            sb4.append(str2);
            sb4.append(sVar.packCode);
            sb4.append(str2);
            sb4.append(sVar.uniqueKey);
            sb4.append(str2);
            sb4.append(sVar.title);
            sb4.append(str2);
            sb4.append(sVar.content);
            sb4.append(str2);
            sb4.append(sVar.groupKey);
            sb4.append(str2);
            sb4.append(sVar.iconPath);
            sb4.append(str2);
            sb4.append(sVar.backGroundColor);
            LogUtil.LOGI("NOTIINFO", sb4.toString());
            return sVar;
        }
        return null;
    }

    /**
     * Validate phone number
     */
    private boolean isValidMobile(String phone) {
        return android.util.Patterns.PHONE.matcher(phone).matches();
    }

    /**
     * Get Notifications color
     */
    @RequiresApi(api = Build.VERSION_CODES.M)
    private int getColor(Notification notification) {
        int i = notification.color;
        return i != 0 ? i : ContextCompat.getColor(this.context, R.color.icon_default_color);
    }

    /**
     * Check notification duplicate
     */
    private boolean checkDuplicate(NotificationData sVar) {
        return (TextUtils.isEmpty(sVar.title) && TextUtils.isEmpty(sVar.content)) || checkContent(sVar.content, sVar.title) || isDuplicatedNoti(sVar.content, sVar.notiAt) || checkWhatsAppDuplicate(sVar.sortKey);
    }

    /**
     * Check notification duplicate
     */
    private boolean checkContent(String str, String title) {
        boolean isTrue = false;
        if (title.contains("Me") || str.contains("Last message") || str.contains("new messages") || str.contains("You have a new message") || str.isEmpty()
                || str.contains("You have missed messages from") || title.equalsIgnoreCase("LINE") || title.equalsIgnoreCase("Viber") || title.equalsIgnoreCase("WhatsApp")
                || title.contains("You") || title.contains("Outlook") || title.contains("gmail") || title.contains("new message") || title.contains("Google Ads")
                || title.contains("Missed call") || title.contains("App reinstallation") || title.contains("Almost there") || title.contains("Sign in")
                || title.contains("Retrieving...") || title.contains("App reinstallations...") || str.contains("sent you a message") || str.contains("Add as friends")
                || title.contains("카카오") || title.contains("Missed calls") || title.contains("App re-installation required") || str.contains("Please sign in to")
                || title.contains("unread messages") || title.contains("Telegram") || title.equalsIgnoreCase("Instagram") || title.equalsIgnoreCase("ICQ New")
                || title.equalsIgnoreCase("Hike") || title.contains("Trending") || title.contains("Suggested") || str.contains("Invited you to") || str.contains("joined the chat")
                || str.contains("Share these stickers with your friends") || title.contains("One message not sent") || title.contains("Welcome to")
                || title.contains("Chats")) {
            //LogUtil.LOGI("checkContent if..", str);
            isTrue = true;
        }
        return isTrue;
    }

    /**
     * Check whatsApp dnotification duplicate
     */
    private boolean checkWhatsAppDuplicate(String sortKey) {
        boolean isTrue;
        if (sortKey == null) {
            isTrue = false;
        } else {
            if (sortKey.contains("1")) {
                isTrue = false;
            } else {
                isTrue = true;
            }
        }
        return isTrue;
    }

    /**
     * Check notification duplicate
     */
    private boolean isDuplicatedNoti(String str, String str2) {
        String str3 = this.contentTemp;
        boolean z = str3 != null && this.notiAtTemp != null && str3.equals(str) && this.notiAtTemp.equals(str2);
        StringBuilder sb = new StringBuilder();
        sb.append("check : ");
        sb.append(z);
        LogUtil.INSTANCE.mLOGE("isDuplicatedNoti",""+sb);
        return z;
    }

    /**
     * Get content from notification
     */
    private String getContent(String str, Notification notification, String str2) {
        CharSequence charSequence = notification.extras.getCharSequence(("com.Slack".equals(str) || !(!TextUtils.isEmpty(str2) && str2.contains("BigTextStyle"))) ? EXTRA_TEXT : EXTRA_BIG_TEXT);
        return charSequence != null ? charSequence.toString() : "";
    }

    /**
     * Get bitmap image from notification
     */
    private Bitmap getBitmapImage(Notification notification, NotificationData sVar) {
        if (Build.VERSION.SDK_INT >= 23) {
            Icon largeIcon = notification.getLargeIcon();
            if (largeIcon != null) {
                Bitmap drawableToBitmap = ImageUtility.Companion.drawableToBitmap(largeIcon.loadDrawable(this.context));
                sVar.isLargeIcon = true;
                return drawableToBitmap;
            }
            Icon smallIcon = notification.getSmallIcon();
            if (smallIcon != null) {
                return ImageUtility.Companion.drawableToBitmap(smallIcon.loadDrawable(this.context));
            }
            return null;
        }
        Bitmap bitmap = (Bitmap) notification.extras.getParcelable(EXTRA_LARGE_ICON);
        if (bitmap == null) {
            int i = notification.extras.getInt(EXTRA_SMALL_ICON);
            PackageManager packageManager = this.context.getPackageManager();
            try {
                return BitmapFactory.decodeResource(packageManager.getResourcesForApplication(packageManager.getApplicationInfo(sVar.packageName, GET_META_DATA)), i);
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }
        }
        return bitmap;
    }

    /**
     * Get notification images
     */
    private String getNotificationImage(Bitmap imagePath, String id) {
        String cImagePath = null;
        try {
            Bitmap circularBitmap = ImageUtils.getCircularBitmap(imagePath);
            cImagePath = saveImage(circularBitmap, id);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return cImagePath;
    }

    /**
     * Save notification icon image to local storage
     */
    public String saveImage(Bitmap finalBitmap, String fileName) {
        boolean z = true;
        String appPath = context.getFilesDir().getAbsolutePath();
        File myDir = new File(appPath + "/saved_images");
        myDir.mkdirs();
        String fName = fileName + ".jpg";
        File file = new File(myDir, fName);
        String profilePath = file.getAbsolutePath();
        if (file.exists()) file.delete();
        try {
            FileOutputStream out = new FileOutputStream(file);
            finalBitmap.setHasAlpha(true);
            finalBitmap.compress(z ? Bitmap.CompressFormat.PNG : Bitmap.CompressFormat.JPEG, 100, out);
            out.flush();
            out.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return profilePath;
    }

    /**
     * Save telegram notification icon image to local storage
     */
    public String saveTelegram(Bitmap finalBitmap, String fileName) {
        boolean z = true;
        String appPath = context.getFilesDir().getAbsolutePath();
        File myDir = new File(appPath + "/saved_images");
        myDir.mkdirs();

        String fName = fileName + ".jpg";
        File file = new File(myDir, fName);
        String profilePath = file.getAbsolutePath();
        if (file.exists()) file.delete();
        try {
            FileOutputStream out = new FileOutputStream(file);
            finalBitmap.compress(z ? Bitmap.CompressFormat.PNG : Bitmap.CompressFormat.JPEG, 100, out);
            out.flush();
            out.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return profilePath;
    }

    /**
     * Validate notification insert or not
     */
    public boolean shouldInsertValidate(String str) {
        return !AppConstant.NOTISAVE.equals(str) && !"com.hawk.notifybox".equals(str) && !"android".equals(str) && !"com.android.providers.contacts".equals(str);
    }
}
