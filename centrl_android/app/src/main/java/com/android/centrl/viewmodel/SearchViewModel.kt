package com.android.centrl.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

/**
 * View Model for the [com.android.centrl.ui.activity.InteractionsActivity]
 */
class SearchViewModel : ViewModel() {

    private var contactMutableLiveData: MutableLiveData<String>? = null
    private var chatMutableLiveData: MutableLiveData<String>? = null
    private var mailMutableLiveData: MutableLiveData<String>? = null
    private var refreshMutableLiveData: MutableLiveData<Int>? = null
    private var mergeMutableLiveData: MutableLiveData<Int>? = null

    fun init() {
        contactMutableLiveData = MutableLiveData()
        chatMutableLiveData = MutableLiveData()
        mailMutableLiveData = MutableLiveData()
        refreshMutableLiveData = MutableLiveData()
        mergeMutableLiveData = MutableLiveData()
    }

    fun sendContactData(msg: String) {
        contactMutableLiveData!!.value = msg
    }

    val contact: LiveData<String>?
        get() = contactMutableLiveData

    fun sendChatData(msg: String) {
        chatMutableLiveData!!.value = msg
    }

    val chat: LiveData<String>?
        get() = chatMutableLiveData

    fun sendMailData(msg: String) {
        mailMutableLiveData!!.value = msg
    }

    val mail: LiveData<String>?
        get() = mailMutableLiveData

    fun refreshPage(refresh: Int) {
        refreshMutableLiveData!!.value = refresh
    }

    val refresh: LiveData<Int>?
        get() = refreshMutableLiveData

    fun setMerge(merge: Int) {
        mergeMutableLiveData!!.value = merge
    }

    val merge: LiveData<Int>?
        get() = mergeMutableLiveData
}