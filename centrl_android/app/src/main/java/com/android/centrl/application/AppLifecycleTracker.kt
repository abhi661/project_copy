package com.android.centrl.application

import android.app.Activity
import android.app.Application
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.net.Uri
import android.os.Bundle
import com.android.centrl.R
import com.android.centrl.notificationservice.PrefManager
import com.android.centrl.ui.activity.*
import com.android.centrl.utils.AppConstant
import com.android.centrl.utils.LogUtil
import java.util.concurrent.Executors
import java.util.concurrent.ScheduledExecutorService
import java.util.concurrent.TimeUnit

/**
 * App lifecycle tracking for security lock and unlock
 */
class AppLifecycleTracker : Application.ActivityLifecycleCallbacks {
    private val worker: ScheduledExecutorService = Executors.newSingleThreadScheduledExecutor()
    private var numStarted = 0
    var preferences: SharedPreferences? = null
    override fun onActivityStarted(activity: Activity) {
        preferences = activity.getSharedPreferences(AppConstant.PREF_NAME, Context.MODE_PRIVATE)
        val intent = activity.intent
        val data: Uri? = intent.data
        if(data != null){
            if(data.toString() == "https://www.centrl.com/forgot/pin" || data.toString() == "https://www.centrl.com/forgot/pattern"){
                numStarted++
                return
            }
        } else {
            if (numStarted == 0) {
                // app went to foreground
                if(!PrefManager.getInstance(activity)!!.isEnabled(AppConstant.SET_IMAGE, false)){
                    if (preferences!!.getInt(AppConstant.LOCK_TYPE, 0) != 0) {
                        if(activity.localClassName.equals(AppConstant.CLASS_NAME_SPLASH, ignoreCase = true)){
                            val runnable = Runnable {
                                when {
                                    preferences!!.getInt(AppConstant.LOCK_TYPE, 0) == AppConstant.PIN_LOCK_CODE -> {
                                        val intentPin = Intent(activity, AuthPinLockActivity::class.java)
                                        intentPin.putExtra(AppConstant.CLASS_NAME, activity.localClassName)
                                        activity.startActivity(intentPin)
                                        activity.overridePendingTransition(R.anim.push_down_in, 0)
                                    }
                                    preferences!!.getInt(AppConstant.LOCK_TYPE, 0) == AppConstant.PATTERN_LOCK_CODE -> {
                                        val intentPattern= Intent(activity, AuthPatternLockActivity::class.java)
                                        intentPattern.putExtra(AppConstant.CLASS_NAME, activity.localClassName)
                                        activity.startActivity(intentPattern)
                                        activity.overridePendingTransition(R.anim.push_down_in, 0)
                                    }
                                    preferences!!.getInt(AppConstant.LOCK_TYPE, 0) == AppConstant.FINGER_LOCK_CODE -> {
                                        val intentFinger = Intent(activity, AuthFingerPrintActivity::class.java)
                                        intentFinger.putExtra(AppConstant.CLASS_NAME, activity.localClassName)
                                        activity.startActivity(intentFinger)
                                        activity.overridePendingTransition(R.anim.push_down_in, 0)
                                    }
                                }
                            }
                            worker.schedule(runnable, 2, TimeUnit.SECONDS)
                        } else {
                            when {
                                preferences!!.getInt(AppConstant.LOCK_TYPE, 0) == AppConstant.PIN_LOCK_CODE -> {
                                    val intentPin = Intent(activity, AuthPinLockActivity::class.java)
                                    intentPin.putExtra(AppConstant.CLASS_NAME, activity.localClassName)
                                    activity.startActivity(intentPin)
                                    activity.overridePendingTransition(R.anim.push_down_in, 0)
                                }
                                preferences!!.getInt(AppConstant.LOCK_TYPE, 0) == AppConstant.PATTERN_LOCK_CODE -> {
                                    val intentPattern= Intent(activity, AuthPatternLockActivity::class.java)
                                    intentPattern.putExtra(AppConstant.CLASS_NAME, activity.localClassName)
                                    activity.startActivity(intentPattern)
                                    activity.overridePendingTransition(R.anim.push_down_in, 0)
                                }
                                preferences!!.getInt(AppConstant.LOCK_TYPE, 0) == AppConstant.FINGER_LOCK_CODE -> {
                                    val intentFinger = Intent(activity, AuthFingerPrintActivity::class.java)
                                    intentFinger.putExtra(AppConstant.CLASS_NAME, activity.localClassName)
                                    activity.startActivity(intentFinger)
                                    activity.overridePendingTransition(R.anim.push_down_in, 0)
                                }
                            }
                        }

                    }
                }
            }
            numStarted++
        }
    }

    override fun onActivityStopped(activity: Activity) {
        numStarted--
        if (numStarted == 0) {
            LogUtil.LOGI("onActivityStopped","onActivityStopped")
            //activity!!.window.addFlags(WindowManager.LayoutParams.FLAG_SECURE)
        }
    }

    override fun onActivityPaused(activity: Activity) {
        //activity.window.setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE)
    }
    override fun onActivityDestroyed(activity: Activity) {}
    override fun onActivitySaveInstanceState(activity: Activity, outState: Bundle) {}
    override fun onActivityCreated(activity: Activity, savedInstanceState: Bundle?) {}
    override fun onActivityResumed(activity: Activity) {
        //activity.window.clearFlags(WindowManager.LayoutParams.FLAG_SECURE)
    }
}