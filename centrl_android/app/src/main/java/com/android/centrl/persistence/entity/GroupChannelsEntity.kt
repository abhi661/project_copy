package com.android.centrl.persistence.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

/**
 * Entity model class for a group channels table
 */
@Entity(tableName = "groupChannel")
data class GroupChannelsEntity(
    @PrimaryKey(autoGenerate = true)
    var id: Int = 0,
    var groupID: String,
    var chatKey: String,
    var title: String,
    var packageCode: Int,
    var iconPath: String,
    var groupIconPath: String,
    var groupTitle: String,
    var number: String,
    var muteChat: Int,
    var notiType: String,
    var edit: Int,
    var isGroup: Int
)