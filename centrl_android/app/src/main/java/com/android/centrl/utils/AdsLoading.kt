package com.android.centrl.utils

import android.app.Activity
import android.util.DisplayMetrics
import android.view.View
import android.widget.Button
import android.widget.FrameLayout
import android.widget.TextView
import android.widget.Toast
import com.android.centrl.R
import com.android.centrl.notificationservice.PrefManager
import com.google.android.gms.ads.*
import com.google.android.gms.ads.formats.MediaView
import com.google.android.gms.ads.formats.NativeAdOptions
import com.google.android.gms.ads.formats.UnifiedNativeAd
import com.google.android.gms.ads.formats.UnifiedNativeAdView

/**
 * Ads setup and loading class
 */
object AdsLoading {
    /**
     * Loading ads banner
     */
    fun loadBanner(context: Activity, frameLayout: FrameLayout): AdView? {

        if(!PrefManager.getInstance(context)!!.isEnabled(AppConstant.PREMIUM, false)){
            // Create an ad request.
            val mAdView = AdView(context)
            mAdView.adUnitId = context.resources.getString(R.string.admob_unit_id)
            frameLayout.removeAllViews()
            frameLayout.addView(mAdView)
            val adSize = getAdSize(context, frameLayout)
            mAdView.adSize = adSize
            val adRequest = AdRequest.Builder().build()
            // Start loading the ad in the background.

            mAdView.loadAd(adRequest)
            return mAdView
        }
        return null
    }

    /**
     * Get ads size
     */
    private fun getAdSize(context: Activity, frameLayout: FrameLayout): AdSize {
        // Determine the screen width (less decorations) to use for the ad width.
        val display = context.windowManager.defaultDisplay
        val outMetrics = DisplayMetrics()
        display.getMetrics(outMetrics)
        val density = outMetrics.density
        var adWidthPixels = frameLayout.width.toFloat()
        // If the ad hasn't been laid out, default to the full screen width.
        if (adWidthPixels == 0f) {
            adWidthPixels = outMetrics.widthPixels.toFloat()
        }
        val adWidth = (adWidthPixels / density).toInt()
        //return AdSize.getCurrentOrientationBannerAdSizeWithWidth(context, adWidth)
        return AdSize.getCurrentOrientationAnchoredAdaptiveBannerAdSize(context, adWidth)
    }
    
    fun resumeAds(context: Activity, adView: AdView?, frameLayout: FrameLayout){
        if(PrefManager.getInstance(context)!!.isEnabled(AppConstant.PREMIUM, false)){
            adView?.destroy()
            frameLayout.removeAllViews()
        } else {
            adView?.resume()
        }
    }
}