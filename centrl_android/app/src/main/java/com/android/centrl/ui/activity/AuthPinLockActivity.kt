package com.android.centrl.ui.activity

import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.view.KeyEvent
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import com.android.centrl.R
import com.android.centrl.databinding.ActivityAuthPinLockBinding
import com.android.centrl.ui.dialog.DialogConfirmEmail
import com.android.centrl.ui.views.pinlockview.IndicatorDots
import com.android.centrl.ui.views.pinlockview.PinLockListener
import com.android.centrl.utils.AppConstant
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount

/**
 * AuthPIN activity for unlock Centrl
 */
@RequiresApi(Build.VERSION_CODES.M)
class AuthPinLockActivity : BaseActivity(), DialogConfirmEmail.DialogConfirmEmailListener {

    private var account: GoogleSignInAccount? = null
    private lateinit var binding: ActivityAuthPinLockBinding
    private var mClassName: String? = null


    private val mPinLockListener: PinLockListener = object : PinLockListener {

        override fun onComplete(pin: String) {
            val getPin = preferences!!.getString(AppConstant.PIN_LOCK, null)
            if (getPin != null && getPin.isNotEmpty()) {
                if (getPin == pin) {
                    if (mClassName.equals(AppConstant.CLASS_NAME_SPLASH, ignoreCase = true)) {
                        val intent = Intent(this@AuthPinLockActivity, InteractionsActivity::class.java)
                        startActivity(intent)
                    }
                    editor!!.putString(AppConstant.PIN_LOCK, pin)
                    editor!!.putInt(AppConstant.LOCK_TYPE, AppConstant.PIN_LOCK_CODE)
                    editor!!.commit()
                    finish()
                    exitActivityAnimation()
                } else {
                    binding.pinLockViewAuth.resetPinLockView()
                    binding.authPinStatusTxt.text = resources.getString(R.string.wrong_pin)
                    binding.authPinStatusTxt.setTextColor(resources.getColor(R.color.security_error, null))
                }
            }
        }
        override fun onEmpty() {
            binding.authPinStatusTxt.text =""
        }
        override fun onPinChange(pinLength: Int, intermediatePin: String) {}
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_auth_pin_lock)
        account = GoogleSignIn.getLastSignedInAccount(applicationContext)
        val bundle = intent.extras
        if (bundle != null) {
            mClassName = bundle.getString(AppConstant.CLASS_NAME)
        }
        binding.authPinStatusTxt.text = ""
        binding.pinLockViewAuth.attachIndicatorDots(binding.indicatorDotsAuth)
        binding.pinLockViewAuth.setPinLockListener(mPinLockListener)
        binding.pinLockViewAuth.pinLength = 4
        binding.pinLockViewAuth.textColor = ContextCompat.getColor(this, R.color.security_text)
        binding.pinLockViewAuth.deleteButtonDrawable = resources.getDrawable(R.drawable.ic_delete, null)
        binding.indicatorDotsAuth.indicatorType = IndicatorDots.IndicatorType.FILL_WITH_ANIMATION

        binding.authForgotPin.setOnClickListener {
            val intent = Intent(this@AuthPinLockActivity, PinLockActivity::class.java)
            intent.data = Uri.parse("https://www.centrl.com/forgot/pin")
            startActivity(intent)
            finish()
            /*if(account == null){
                confirmEmail("")
            } else {
                confirmEmail(account!!.email)
            }*/
        }
    }

    /**
     * Confirm email to send link dialog.
     */
    private fun confirmEmail(emailId: String?) {
        val dialogAddNewChannels = DialogConfirmEmail()
        val bundle = Bundle()
        bundle.putString(AppConstant.EMAIL, emailId)
        dialogAddNewChannels.arguments = bundle
        dialogAddNewChannels.show(supportFragmentManager, DialogConfirmEmail.TAG)
    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
           close()
            return true
        }
        return super.onKeyDown(keyCode, event)
    }

   private fun close(){
       val intent = Intent(this@AuthPinLockActivity, FinishActivity::class.java)
       intent.action = Intent.ACTION_MAIN
       intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
       intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
       startActivity(intent)
       finish()
   }

    override fun onSentLink() {
        Toast.makeText(this, resources.getString(R.string.sent_link), Toast.LENGTH_LONG).show()
    }

}