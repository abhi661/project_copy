package com.android.centrl.viewmodel

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.android.centrl.persistence.datasource.ChatBackupDataSource

/**
 * Factory for ViewModels
 */
class ChatBackupViewModelFactory(private val application: Application, private val mDataSource: ChatBackupDataSource) :
    ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(ChatBackupViewModel::class.java)) {
            return ChatBackupViewModel(application, mDataSource) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }

}