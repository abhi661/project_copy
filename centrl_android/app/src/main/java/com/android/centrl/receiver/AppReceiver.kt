package com.android.centrl.receiver

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import com.android.centrl.notificationservice.PendingIntentManager
import com.android.centrl.persistence.entity.InstalledAppsEntity
import com.android.centrl.persistence.entity.KeyboardAppsTrayEntity
import com.android.centrl.persistence.repositry.InstalledAppsRepository
import com.android.centrl.persistence.repositry.KeyBoardAppsRepository
import com.android.centrl.utils.AppConstant
import com.android.centrl.utils.CentrlUtil
import com.android.centrl.utils.LogUtil

/**
 * Centrl app receiver for getting installed or removed apps.
 */
class AppReceiver : BroadcastReceiver() {
    private lateinit var installedAppsRepository: InstalledAppsRepository
    private lateinit var keyBoardAppsRepository: KeyBoardAppsRepository

    override fun onReceive(context: Context, intent: Intent) {
        installedAppsRepository = InstalledAppsRepository(context)
        keyBoardAppsRepository = KeyBoardAppsRepository(context)
        val i: Int
        val i2: Int
        val str = "android.intent.extra.REPLACING"
        val str2 = "AppReceiver"
        try {
            val data = intent.data
            if (data != null) {
                LogUtil.mLOGW(str2, "onReceive start ")
                val schemeSpecificPart = data.schemeSpecificPart
                val action = intent.action
                if (action != null) {
                    val str3 = " / "
                    if (action == "android.intent.action.PACKAGE_ADDED") {
                        val sb = StringBuilder()
                        sb.append("Package ADD: ")
                        sb.append(schemeSpecificPart)
                        sb.append(str3)
                        sb.append(0)
                        sb.append(str3)
                        sb.append(0)
                        LogUtil.mLOGW(str2, sb.toString())
                        i2 = 0
                        i = 1
                    } else {
                        i2 = if (action == "android.intent.action.PACKAGE_REMOVED") {
                            val sb2 = StringBuilder()
                            sb2.append("Package Removed: ")
                            sb2.append(schemeSpecificPart)
                            sb2.append(str3)
                            sb2.append(1)
                            sb2.append(str3)
                            sb2.append(0)
                            LogUtil.mLOGW(str2, sb2.toString())
                            1
                        } else {
                            if (action == "android.intent.action.PACKAGE_REPLACED") {
                                val sb3 = StringBuilder()
                                sb3.append("Package REPLACED: ")
                                sb3.append(schemeSpecificPart)
                                sb3.append(str3)
                                sb3.append(0)
                                sb3.append(str3)
                                sb3.append(0)
                                LogUtil.mLOGW(str2, sb3.toString())
                            }
                            0
                        }
                        i = 0
                    }
                    if (intent.extras == null || !intent.extras!!.containsKey(str) || !intent.extras!!.getBoolean(str, false)) {
                        val i3 = i2 + 1
                        val i4 = i + 1
                        val sb4 = StringBuilder()
                        sb4.append("Package REPLACE--->>: ")
                        sb4.append(schemeSpecificPart)
                        sb4.append(str3)
                        sb4.append(i3)
                        sb4.append(str3)
                        sb4.append(i4)
                        LogUtil.mLOGW(str2, sb4.toString())
                        if (i3 == 2) {
                            val sb5 = StringBuilder()
                            sb5.append("Completely Package Removed: ")
                            sb5.append(schemeSpecificPart)
                            sb5.append(str3)
                            sb5.append(i3)
                            sb5.append(str3)
                            sb5.append(i4)
                            LogUtil.mLOGW(str2, sb5.toString())
                            removeChatApp(context, schemeSpecificPart)
                        } else if (i4 == 2) {
                            val sb6 = StringBuilder()
                            sb6.append("Completely Package Add: ")
                            sb6.append(schemeSpecificPart)
                            sb6.append(str3)
                            sb6.append(i3)
                            sb6.append(str3)
                            sb6.append(i4)
                            LogUtil.mLOGW(str2, sb6.toString())
                            insertNewChatApps(context, schemeSpecificPart)
                        }
                    }
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun insertNewChatApps(context: Context?, mPackage: String?) {
        try {
            val mAllPackages = keyBoardAppsRepository.getAllKeyboardAppTrayPackName()
            if (CentrlUtil.getAppPackageList(context).contains(mPackage)) {
                if (mAllPackages.size == 0) {
                    keyBoardAppsRepository.insertKeyboardAppTray(KeyboardAppsTrayEntity(CentrlUtil.getPackageCodeList(context)[mPackage]!!, mPackage!!))
                    installedAppsRepository.insertInstalledApps(InstalledAppsEntity(CentrlUtil.getPackageCodeList(context)[mPackage]!!, mPackage))
                    Thread.sleep(100)
                    PendingIntentManager.getInstance(context).getPackageList()
                } else {
                    if (!mAllPackages.contains(mPackage)) {
                        keyBoardAppsRepository.insertKeyboardAppTray(KeyboardAppsTrayEntity(CentrlUtil.getPackageCodeList(context)[mPackage]!!, mPackage!!))
                        installedAppsRepository.insertInstalledApps(InstalledAppsEntity(CentrlUtil.getPackageCodeList(context)[mPackage]!!, mPackage))
                        Thread.sleep(100)
                        PendingIntentManager.getInstance(context).getPackageList()
                    }
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun removeChatApp(context: Context?, mPackage: String?) {
        try {
            val mAllPackages = keyBoardAppsRepository.getAllKeyboardAppTrayPackName()
            val mAllPackagesCode = keyBoardAppsRepository.getAllRemovedChatAppPackages()
            if (mAllPackages.size != 0) {
                if (mAllPackages.contains(mPackage)) {
                    keyBoardAppsRepository.deleteKeyboardAppTray(CentrlUtil.getPackageCodeList(context)[mPackage]!!)
                    installedAppsRepository.deleteInstalledApps(CentrlUtil.getPackageCodeList(context)[mPackage]!!)
                    Thread.sleep(AppConstant.SLEEP)
                    PendingIntentManager.getInstance(context).getPackageList()
                } else {
                    if (mAllPackagesCode.contains(CentrlUtil.getPackageCodeList(context)[mPackage])) {
                        keyBoardAppsRepository.deleteKeyboardApps(CentrlUtil.getPackageCodeList(context)[mPackage]!!)
                        installedAppsRepository.deleteInstalledApps(CentrlUtil.getPackageCodeList(context)[mPackage]!!)
                        Thread.sleep(AppConstant.SLEEP)
                        PendingIntentManager.getInstance(context).getPackageList()
                    }
                }
            } else {
                if (mAllPackagesCode.size != 0 && mAllPackagesCode.contains(CentrlUtil.getPackageCodeList(context)[mPackage])) {
                    keyBoardAppsRepository.deleteKeyboardApps(CentrlUtil.getPackageCodeList(context)[mPackage]!!)
                    installedAppsRepository.deleteInstalledApps(CentrlUtil.getPackageCodeList(context)[mPackage]!!)
                    Thread.sleep(AppConstant.SLEEP)
                    PendingIntentManager.getInstance(context).getPackageList()
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    companion object {
        var TAG = "AppReceiver"
    }
}