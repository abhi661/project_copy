package com.android.centrl.adapter

import android.content.Context
import android.graphics.Typeface
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseExpandableListAdapter
import android.widget.ImageView
import android.widget.TextView
import com.android.centrl.R
import com.android.centrl.models.FAQChild
import java.util.*

class FAQExpandableListAdapter(private val mContext: Context, private val header: List<String>,
                               private val child: HashMap<String, List<FAQChild>>, private val childSelectInterface: ChildSelectInterface) : BaseExpandableListAdapter() {


    var icons = arrayOf(R.drawable.faq_download,R.drawable.faq_chats,R.drawable.faq_mails, R.drawable.faq_contacts, R.drawable.faq_security, R.drawable.faq_trouble,R.drawable.faq_keyboard, R.drawable.faq_ads,R.drawable.faq_invite, R.drawable.faq_notification, R.drawable.faq_help)

    override fun getGroupCount(): Int {
        // Get header size
        return header.size
    }

    override fun getChildrenCount(groupPosition: Int): Int {
        // return children count
        return child[header[groupPosition]]!!.size
    }

    override fun getGroup(groupPosition: Int): Any {
        // Get header position
        return header[groupPosition]
    }

    override fun getChild(groupPosition: Int, childPosition: Int): Any {
        // This will return the child
        return child[header[groupPosition]]!![childPosition]
    }

    override fun getGroupId(groupPosition: Int): Long {
        return groupPosition.toLong()
    }

    override fun getChildId(groupPosition: Int, childPosition: Int): Long {
        return childPosition.toLong()
    }

    override fun hasStableIds(): Boolean {
        return false
    }

    override fun getGroupView(groupPosition: Int, isExpanded: Boolean, convertView: View?, parent: ViewGroup): View {
        // Getting header title
        var view = convertView
        val headerTitle = getGroup(groupPosition) as String
        // Inflating header layout and setting text
        if (view == null) {
            val infalInflater = mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            view = infalInflater.inflate(R.layout.faq_header, parent, false)
        }
        val headerText = view!!.findViewById<View>(R.id.faq_header) as TextView
        val iconImage = view.findViewById<View>(R.id.faq_image) as ImageView
        headerText.text = headerTitle
        iconImage.setImageResource(icons[groupPosition])
        // If group is expanded then change the text into bold and change the
        // icon
        if (isExpanded) {
            headerText.setTypeface(null, Typeface.BOLD)
            // header_text.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_up_arrow_grey, 0);
        } else {
            // If group is not expanded then change the text back into normal
            // and change the icon
            headerText.setTypeface(null, Typeface.NORMAL)
            // header_text.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_down_arrow_grey, 0);
        }
        return view
    }

    override fun getChildView(groupPosition: Int, childPosition: Int, isLastChild: Boolean, convertView: View?, parent: ViewGroup): View {
        // Getting child text
        var view = convertView
        val childText = getChild(groupPosition, childPosition) as FAQChild
        // Inflating child layout and setting textview
        if (view == null) {
            val infalInflater = mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            view = infalInflater.inflate(R.layout.faq_child, parent, false)
        }
        val tetQuestion = view!!.findViewById<View>(R.id.question_name) as TextView
        tetQuestion.text = childText.mQuestion
        view.setOnClickListener {
            childSelectInterface.childItem(childText.mQuestion, childText.mContent)
        }
        return view
    }

    override fun isChildSelectable(groupPosition: Int, childPosition: Int): Boolean {
        return true
    }

    /**
     * Search filter interface
     */
    interface ChildSelectInterface {
        fun childItem(ques: String, ans: String)
    }
}