package com.android.centrl.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Typeface
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.ImageView
import android.widget.TextView
import com.android.centrl.R
import com.android.centrl.utils.CentrlUtil
import java.util.*

/**
 * Creating an custom spinner Adapter Class
 */
class CustomSpinnerAdapter(mContext: Context, channelsList: ArrayList<String>) : ArrayAdapter<String>(mContext, 0, channelsList) {

    private val mInflater: LayoutInflater = LayoutInflater.from(mContext)
    private val items: ArrayList<String> = channelsList

    override fun getDropDownView(position: Int, convertView: View?, parent: ViewGroup): View {
        return createItemView(position, parent)
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        return createItemView(position, parent)
    }

    @SuppressLint("NewApi")
    private fun createItemView(position: Int, parent: ViewGroup): View {
        val view = mInflater.inflate(R.layout.custom_channel_spinner, parent, false)
        val title = view.findViewById<TextView>(R.id.list_item_channel_name)
        val image = view.findViewById<ImageView>(R.id.list_item_icon)
        val channelTitle = items[position]
        title.text = channelTitle
        if (channelTitle.equals(context.resources.getString(R.string.channel_select), ignoreCase = true)) {
            image.visibility = View.GONE
            title.setTextColor(context.resources.getColor(R.color.chat_msg, null))
            title.setTypeface(null, Typeface.NORMAL)
        } else {
            image.visibility = View.VISIBLE
            val packageCode = CentrlUtil.getChannelsPackageCodeHashMap()[items[position]]!!

            if(packageCode != 0){
                image.setBackgroundResource(CentrlUtil.getChannelsLogo()[packageCode]!!)
            }
            title.setTextColor(context.resources.getColor(R.color.chat_msg, null))
            title.setTypeface(null, Typeface.BOLD)
        }
        return view
    }
}