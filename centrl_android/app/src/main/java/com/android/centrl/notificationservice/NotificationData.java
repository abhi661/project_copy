package com.android.centrl.notificationservice;

import android.text.TextUtils;

import com.android.centrl.utils.LogUtil;

import java.io.Serializable;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import kotlin.UByte;

/**
 * Notifications data parsing class
 */
public class NotificationData implements Serializable, Cloneable {

    public int appId;
    public int backGroundColor;
    public int packCode;
    public String content;
    public String groupTitle = "";
    public int height;
    public String iconPath;
    public String groupIconPath;

    public boolean isLargeIcon;
    public boolean isRead;
    public String notiAt;
    public String packageName;
    public String subTitle;
    public String title;
    public String sortKey;
    public String uniqueKey;
    public String mailKey;
    public String groupKey;
    public String notiType;
    public String phoneNumber = "";
    public int isGroupChat = 0;

    public String getChatKey() {
        boolean z = !TextUtils.isEmpty(this.subTitle);
        String str = z ? this.subTitle : this.title;
        StringBuilder sb = new StringBuilder();
        sb.append("subTitle ");
        sb.append(this.subTitle);
        String str2 = "getChatKey";
        LogUtil.LOGI(str2, sb.toString());
        StringBuilder sb2 = new StringBuilder();
        sb2.append("title : ");
        sb2.append(this.title);
        LogUtil.LOGI(str2, sb2.toString());
        StringBuilder sb3 = new StringBuilder();
        sb3.append("isGroup : ");
        sb3.append(z);
        LogUtil.LOGI(str2, sb3.toString());
        if (TextUtils.isEmpty(this.packageName) || TextUtils.isEmpty(str)) {
            return null;
        }
        StringBuilder sb4 = new StringBuilder();
        sb4.append(this.packageName);
        sb4.append(z);
        sb4.append(str);
        String md5 = md5(sb4.toString());
        // String md5 = sb4.toString();
        StringBuilder sb5 = new StringBuilder();
        sb5.append("key : ");
        sb5.append(md5);
        LogUtil.LOGI(str2, sb5.toString());
        return md5;
    }

    public String getGroupTitle() {
        String str = this.groupTitle;
        return str == null ? "" : str;
    }

    public boolean isGroupChat() {
        boolean isGroup = !TextUtils.isEmpty(this.subTitle);
        return isGroup;
    }

    public String getKey() {
        if (TextUtils.isEmpty(this.packageName) || TextUtils.isEmpty(this.notiAt) || TextUtils.isEmpty(this.content)) {
            return null;
        }
        StringBuilder sb = new StringBuilder();
        sb.append(this.packageName);
        sb.append(this.notiAt);
        sb.append(this.content);
        return md5(sb.toString());
        //return sb.toString();
    }

    public String getUniqueKey() {
        return this.uniqueKey;
    }

    public String getMailKey() {
        return this.mailKey;
    }

    public boolean hasSubTitle() {
        return !TextUtils.isEmpty(this.subTitle);
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(", title='");
        sb.append(this.title);
        sb.append('\'');
        sb.append(", subTitle='");
        sb.append(this.subTitle);
        sb.append('\'');
        sb.append(", content='");
        sb.append(this.content);
        sb.append('\'');
        sb.append(", iconPath='");
        sb.append(this.iconPath);
        sb.append('\'');
        sb.append(", backGroundColor=");
        sb.append(this.backGroundColor);
        sb.append('\'');
        sb.append(", notiAt='");
        sb.append(this.notiAt);
        sb.append('\'');
        sb.append(", groupKey=");
        sb.append(this.groupKey);
        sb.append('\'');
        sb.append(", packCode=");
        sb.append(this.packCode);
        sb.append('\'');
        sb.append(", packageName='");
        sb.append(this.packageName);
        sb.append('\'');
        sb.append(", uniqueKey='");
        sb.append(this.uniqueKey);
        sb.append('\'');
        sb.append('}');
        return sb.toString();
    }

    public NotificationData clone() throws CloneNotSupportedException {
        return (NotificationData) super.clone();
    }

    /**
     * Generate md5 key
     */
    public static String md5(String str) {
        try {
            MessageDigest instance = MessageDigest.getInstance("MD5");
            instance.update(str.getBytes());
            byte[] digest = instance.digest();
            StringBuilder sb = new StringBuilder();
            for (byte b : digest) {
                String hexString = Integer.toHexString(b & UByte.MAX_VALUE);
                while (hexString.length() < 2) {
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append("0");
                    sb2.append(hexString);
                    hexString = sb2.toString();
                }
                sb.append(hexString);
            }
            return sb.toString();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            return "";
        }
    }
}
