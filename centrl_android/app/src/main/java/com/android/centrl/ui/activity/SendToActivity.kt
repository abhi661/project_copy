package com.android.centrl.ui.activity

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.KeyEvent
import android.view.View
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.TextView
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.centrl.R
import com.android.centrl.adapter.SelectContactListAdapter
import com.android.centrl.databinding.ActivitySendToBinding
import com.android.centrl.interfaces.RecyclerViewClickListener
import com.android.centrl.persistence.entity.ChatsEntity
import com.android.centrl.ui.views.RecyclerViewTouchListener
import com.android.centrl.utils.AdsLoading
import com.android.centrl.utils.AppConstant
import com.android.centrl.utils.DrawableClickListener
import com.android.centrl.utils.LogUtil
import com.google.android.gms.ads.AdView
import java.util.*

/**
 * Select contacts to share text.
 */
class SendToActivity : BaseActivity(), SelectContactListAdapter.FilterSearchInterface {
    private lateinit var binding: ActivitySendToBinding
    private var selectContactListAdapter: SelectContactListAdapter? = null
    private var mChatContactList = ArrayList<ChatsEntity>()
    private var animation: Animation? = null
    private var mAdView: AdView? = null
    private var isSearchOpen = false
    private var mSearchedTxt = ""
    private var mSharedTxt = ""
    @SuppressLint("ClickableViewAccessibility")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_send_to)

        binding.selectContactTitleRlSt.visibility = View.VISIBLE
        binding.searchLlSelectContactSt.visibility = View.GONE

        animation = AnimationUtils.loadAnimation(this, R.anim.search_right_to_left)
        val linearLayoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        binding.selectContactRecycleViewSt.layoutManager = linearLayoutManager
        selectContactListAdapter = SelectContactListAdapter(this, mChatContactList, this)
        binding.selectContactRecycleViewSt.adapter = selectContactListAdapter

        binding.selectContactBackSt.setOnClickListener { finish() }

        binding.selectContactRecycleViewSt.addOnItemTouchListener(
            RecyclerViewTouchListener(this, binding.selectContactRecycleViewSt,
            object : RecyclerViewClickListener {
                override fun onClick(view: View, position: Int) {
                    val chat = selectContactListAdapter!!.chatArrayList!![position]

                    val intent = Intent(this@SendToActivity, ChatsRoomActivity::class.java)
                    intent.putExtra(AppConstant.GROUP_KEY, chat.groupID)
                    intent.putExtra(AppConstant.PACKAGE_CODE_KEY, chat.packageCode)
                    intent.putExtra(AppConstant.CHAT_KEY, chat.chatKey)
                    intent.putExtra(AppConstant.SHARED_TEXT_KEY, mSharedTxt)
                    intent.putExtra(AppConstant.IS_FROM_NOTIFICATION, true)
                    startActivity(intent)
                    finish()
                    val imm = (getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager)
                    imm.hideSoftInputFromWindow(binding.searchBackSc.windowToken, 0)
                }
                override fun onLongClick(view: View, position: Int) {
                    //
                }
            })
        )

        binding.selectContactSearchSt.setOnClickListener {
            isSearchOpen = true
            binding.searchLlSelectContactSt.startAnimation(animation)
            binding.searchLlSelectContactSt.visibility = View.VISIBLE
            binding.selectContactTitleRlSt.visibility = View.GONE
            binding.searchEditTxtScSt.requestFocus()
            binding.searchEditTxtScSt.isCursorVisible = true
            (Objects.requireNonNull(getSystemService(Context.INPUT_METHOD_SERVICE)) as InputMethodManager).showSoftInput(binding.searchEditTxtScSt, InputMethodManager.SHOW_FORCED)
        }
        binding.searchBackSc.setOnClickListener {
            isSearchOpen = false
            binding.selectContactTitleRlSt.startAnimation(animation)
            binding.selectContactTitleRlSt.visibility = View.VISIBLE
            binding.searchLlSelectContactSt.visibility = View.GONE
            binding.searchEditTxtScSt.setText("")
            binding.searchEditTxtScSt.hint = resources.getString(R.string.search)
            (Objects.requireNonNull(getSystemService(Context.INPUT_METHOD_SERVICE)) as InputMethodManager).hideSoftInputFromWindow(binding.searchBackSc.windowToken, 0)
        }

        binding.searchEditTxtScSt.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {
                //
            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                mSearchedTxt = s.toString().trim { it <= ' ' }
                selectContactListAdapter!!.filter.filter(mSearchedTxt)

            }

            override fun afterTextChanged(s: Editable) {
                //
            }
        })
        binding.searchEditTxtScSt.setOnTouchListener(object : DrawableClickListener.RightDrawableClickListener(binding.searchEditTxtScSt) {
            override fun onDrawableClick(): Boolean {
                binding.searchEditTxtScSt.setText("")
                selectContactListAdapter!!.filter.filter("")
                binding.searchEditTxtScSt.hint = resources.getString(R.string.search)
                return false
            }
        })
        binding.searchEditTxtScSt.setOnEditorActionListener { _: TextView?, actionId: Int, _: KeyEvent? ->
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                selectContactListAdapter!!.filter.filter(mSearchedTxt)
                val imm = (getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager)
                imm.hideSoftInputFromWindow(binding.searchBackSc.windowToken, 0)
                return@setOnEditorActionListener true
            }
            false
        }
        binding.adViewContainerScSt.adViewContainer.post {
            mAdView = AdsLoading.loadBanner(this, binding.adViewContainerScSt.adViewContainer)
        }


        mChatsViewModel!!.mChatContactsList.observe(this, androidx.lifecycle.Observer { result ->
            try{
                mChatContactList.clear()
                mChatContactList = result
                selectContactListAdapter!!.updateList(mChatContactList)
                if (mChatContactList.size == 0) {
                    binding.emptySelectedRlSt.visibility = View.VISIBLE
                    binding.selectContactRecycleViewSt.visibility = View.GONE
                } else {
                    binding.emptySelectedRlSt.visibility = View.GONE
                    binding.selectContactRecycleViewSt.visibility = View.VISIBLE
                }
            } catch (e: Exception){
                e.printStackTrace()
            }

        })

        val intent = intent
        val action = intent.action
        val type = intent.type
        if ("android.intent.action.SEND" == action && type != null && "text/plain" == type) {
            mSharedTxt = intent.getStringExtra("android.intent.extra.TEXT").toString()
            LogUtil.mLOGE("shareablTextExtra", ""+intent.getStringExtra("android.intent.extra.TEXT"))
        }
    }
    override fun onStart() {
        super.onStart()
        mChatsViewModel!!.getChatContactsList(AppConstant.CHAT_SCREEN)
    }
    override fun onResume() {
        super.onResume()
        if (mAdView != null) {
            AdsLoading.resumeAds(this, mAdView, binding.adViewContainerScSt.adViewContainer)
        }
    }
    override fun onPause() {
        if (mAdView != null) {
            mAdView!!.pause()
        }
        super.onPause()
    }

    override fun onDestroy() {
        if (mAdView != null) {
            mAdView!!.destroy()
        }
        super.onDestroy()
    }
    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            finish()
            return true
        }
        return super.onKeyDown(keyCode, event)
    }
    /**
     * Search filter result
     */
    override fun filterResult(size: Int) {
        if (size == 0) {
            binding.noResultFoundTxtScSt.visibility = View.VISIBLE
            binding.selectContactRecycleViewSt.visibility = View.GONE
        } else {
            binding.noResultFoundTxtScSt.visibility = View.GONE
            binding.selectContactRecycleViewSt.visibility = View.VISIBLE
        }
    }
}