package com.android.centrl.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.android.centrl.R
import java.util.ArrayList
/**
 * Channels group participants adapter
 */
class GroupParticipantsListAdapter(context: Context, chatArrayList: ArrayList<String>?) : RecyclerView.Adapter<GroupParticipantsListAdapter.GroupParticipantsViewHolder>() {

    private var identityList: ArrayList<String>? = null
    var context: Context

    init {
        identityList = chatArrayList
        this.context = context
    }
    // Inflates the appropriate layout according to the ViewType.
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GroupParticipantsViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val view = layoutInflater.inflate(R.layout.group_participants_list, parent, false)
        return GroupParticipantsViewHolder(view)
    }
    // Passes the message object to a ViewHolder so that the contents can be bound to UI.
    override fun onBindViewHolder(holder: GroupParticipantsViewHolder, position: Int) {
        val chat = identityList!![position]
        // If the message is a received message.
        if (chat.isNotEmpty()) {
            holder.identityTitleTxt!!.text = chat
        }
    }

    override fun getItemCount(): Int {
        return identityList!!.size
    }
    /**
     * Adapter view holder
     */
    class GroupParticipantsViewHolder(itemView: View?) : RecyclerView.ViewHolder(itemView!!) {
        var identityTitleTxt: TextView? = null
        init {
            if (itemView != null) {
                identityTitleTxt = itemView.findViewById<View>(R.id.participants_title_text_view) as TextView
            }
        }
    }
    /**
     * This will update the list
     */
    fun updateList(chatArrayList: ArrayList<String>?) {
        identityList = chatArrayList
        //now, tell the adapter about the update
        notifyDataSetChanged()
    }
}