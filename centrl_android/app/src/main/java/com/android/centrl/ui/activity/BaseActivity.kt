package com.android.centrl.ui.activity

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Build
import android.os.Bundle
import android.provider.Settings.ACTION_NOTIFICATION_LISTENER_SETTINGS
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import com.android.centrl.R
import com.android.centrl.persistence.injections.BlockedContactsInjection
import com.android.centrl.persistence.repositry.KeyBoardAppsRepository
import com.android.centrl.utils.AppConstant
import com.android.centrl.viewmodel.*

/**
 * Centrl Base activity
 */
open class BaseActivity : AppCompatActivity() {

    companion object {
        private const val REQUEST_CODE_NOTIFICATION_SETTINGS = 201
    }

    var preferences: SharedPreferences? = null
    var editor: SharedPreferences.Editor? = null

    var chatKey: String? = null
    var groupKey: String? = null
    var packageCode: Int = 1
    var installedAppsViewModel: InstalledAppsViewModel? = null
    private var keyboardAppsRepository: KeyBoardAppsRepository? = null
    var mBlockedContactsViewModel: BlockedContactsViewModel? = null
    private var mBlockedContactsViewModelFactory: BlockedContactsViewModelFactory? = null
    var mMutiChannelViewModel: MultiChannelCountViewModel? = null
    var mGroupChannelsViewModel: GroupChannelsViewModel? = null
    var mChatsViewModel: ChatsViewModel? = null
    @SuppressLint("CommitPrefEdits")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        preferences = getSharedPreferences(AppConstant.PREF_NAME, Context.MODE_PRIVATE)
        editor = preferences!!.edit()
        keyboardAppsRepository = KeyBoardAppsRepository(this)
        installedAppsViewModel = ViewModelProvider(this).get(InstalledAppsViewModel::class.java)
        mBlockedContactsViewModelFactory = BlockedContactsInjection.provideViewModelFactory(application, this)
        mBlockedContactsViewModel = ViewModelProvider(this, mBlockedContactsViewModelFactory!!).get(BlockedContactsViewModel::class.java)
        mMutiChannelViewModel = ViewModelProvider(this).get(MultiChannelCountViewModel::class.java)
        mGroupChannelsViewModel = ViewModelProvider(this).get(GroupChannelsViewModel::class.java)
        mChatsViewModel = ViewModelProvider(this).get(ChatsViewModel::class.java)
    }

    /**
     * Open notification settings for enabling notification access
     */
    private fun goNotificationSettings() {
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
                startActivityForResult(
                    Intent(ACTION_NOTIFICATION_LISTENER_SETTINGS),
                    REQUEST_CODE_NOTIFICATION_SETTINGS
                )
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    /**
     * Build Notification Listener Alert Dialog.
     * Builds the alert dialog that pops up if the user has not turned
     * the Notification Listener Service on yet.
     * @return An alert dialog which leads to the notification enabling screen
     */
    fun buildNotificationServiceAlertDialog(context: Context): AlertDialog? {
        val alertDialogBuilder =
            AlertDialog.Builder(context)
        alertDialogBuilder.setTitle(R.string.notification_listener_service)
        alertDialogBuilder.setMessage(R.string.notification_listener_service_explanation)
        alertDialogBuilder.setPositiveButton(R.string.next) { _, _ ->
            goNotificationSettings()
        }

        return alertDialogBuilder.create()
    }

    /**
     * Exit activity with custom animation
     */
    open fun exitActivityAnimation() {
        overridePendingTransition(0, R.anim.push_down_out)
    }

}