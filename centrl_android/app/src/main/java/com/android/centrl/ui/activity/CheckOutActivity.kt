package com.android.centrl.ui.activity

import android.annotation.SuppressLint
import android.app.ProgressDialog
import android.content.Context
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.KeyEvent
import android.view.View
import android.view.animation.AnimationUtils
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import android.widget.Toast.makeText
import androidx.databinding.DataBindingUtil
import com.android.centrl.R
import com.android.centrl.databinding.ActivityCheckOutBinding
import com.android.centrl.notificationservice.PrefManager
import com.android.centrl.utils.AppConstant
import com.android.centrl.utils.CentrlSnackBar
import com.android.centrl.utils.LogUtil
import net.authorize.acceptsdk.AcceptSDKApiClient
import net.authorize.acceptsdk.datamodel.merchant.ClientKeyBasedMerchantAuthentication
import net.authorize.acceptsdk.datamodel.transaction.CardData
import net.authorize.acceptsdk.datamodel.transaction.EncryptTransactionObject
import net.authorize.acceptsdk.datamodel.transaction.TransactionObject
import net.authorize.acceptsdk.datamodel.transaction.TransactionType
import net.authorize.acceptsdk.datamodel.transaction.callbacks.EncryptTransactionCallback
import net.authorize.acceptsdk.datamodel.transaction.response.EncryptTransactionResponse
import net.authorize.acceptsdk.datamodel.transaction.response.ErrorTransactionResponse

class CheckOutActivity : BaseActivity(), EncryptTransactionCallback {

    companion object {
        const val TAG = "WebCheckoutFragment"

        private const val CARD_NUMBER = "4111111111111111"
        private const val EXPIRATION_MONTH = "11"
        private const val EXPIRATION_YEAR = "2017"
        private const val CVV = "256"
        private const val POSTAL_CODE = "98001"
        private const val CLIENT_KEY =
            "6gSuV295YD86Mq4d86zEsx8C839uMVVjfXm9N4wr6DRuhTHpDU97NFyKtfZncUq8"

        // replace with your CLIENT KEY
        private const val API_LOGIN_ID = "6AB64hcB" // replace with your API LOGIN_ID
        private const val MIN_CARD_NUMBER_LENGTH = 13
        private const val MIN_YEAR_LENGTH = 2
        private const val MIN_CVV_LENGTH = 3
        private const val YEAR_PREFIX = "20"
    }
    private var progressDialog: ProgressDialog? = null
    private var cardNumber: String? = null
    private var month: String? = null
    private var year: String? = null
    private var cvv: String? = null
    private var apiClient: AcceptSDKApiClient? = null

    private lateinit var binding: ActivityCheckOutBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_check_out)
        binding.handler = ClickHandler()
        try {
            apiClient = AcceptSDKApiClient.Builder(this, AcceptSDKApiClient.Environment.SANDBOX)
                .connectionTimeout(
                    4000) // optional connection time out in milliseconds
                .build()
        } catch (e: NullPointerException) {
            e.printStackTrace()
        }
        setUpCreditCardEditText()
        binding.checkoutBack.setOnClickListener {
            finish()
        }
    }
    inner class ClickHandler{

        fun onClick(v: View) {
            if (!areFormDetailsValid()) return
            progressDialog = ProgressDialog.show(this@CheckOutActivity, getString(R.string.progress_title),
                getString(R.string.progress_message), true)

            try {
                val transactionObject = prepareTransactionObject()

                /*
            Make a call to get Token API
            parameters:
              1) EncryptTransactionObject - The transactionObject for the current transaction
              2) callback - callback of transaction
           */
                apiClient!!.getTokenWithRequest(transactionObject, this@CheckOutActivity)
            } catch (e: NullPointerException) {
                // Handle exception transactionObject or callback is null.
                if (progressDialog!!.isShowing) progressDialog!!.dismiss()
                e.printStackTrace()
            }
        }
    }

    /**
     * prepares a transaction object with dummy data to be used with the Gateway transactions
     */
    private fun prepareTransactionObject(): EncryptTransactionObject {
        val merchantAuthentication =
            ClientKeyBasedMerchantAuthentication.createMerchantAuthentication(
                API_LOGIN_ID,
                CLIENT_KEY
            )

        // create a transaction object by calling the predefined api for creation
        return TransactionObject.createTransactionObject(TransactionType.SDK_TRANSACTION_ENCRYPTION) // type of transaction object
            .cardData(prepareCardDataFromFields()) // card data to get Token
            .merchantAuthentication(merchantAuthentication).build()
    }

    private fun prepareTestTransactionObject(): EncryptTransactionObject {
        val merchantAuthentication =
            ClientKeyBasedMerchantAuthentication.createMerchantAuthentication(
                API_LOGIN_ID,
                CLIENT_KEY
            )

        // create a transaction object by calling the predefined api for creation
        return EncryptTransactionObject.createTransactionObject(TransactionType.SDK_TRANSACTION_ENCRYPTION) // type of transaction object
            .cardData(prepareTestCardData()) // card data to prepare token
            .merchantAuthentication(merchantAuthentication).build()
    }

    private fun prepareTestCardData(): CardData {
        return CardData.Builder(
            CARD_NUMBER,
            EXPIRATION_MONTH,
            EXPIRATION_YEAR
        ).cvvCode("")
            .zipCode("")
            .cardHolderName("")
            .build()
    }

    /* ---------------------- Callback Methods - Start -----------------------*/
    @SuppressLint("SetTextI18n")
    override fun onEncryptionFinished(response: EncryptTransactionResponse) {
        hideSoftKeyboard()
        if (progressDialog!!.isShowing) progressDialog!!.dismiss()
        LogUtil.mLOGE("onEncryptionFinished","Data Desc::${response.dataDescriptor}\nDataValue::${response.dataValue}")
        PrefManager.getInstance(this)!!.saveBoolean(AppConstant.PREMIUM, true)
        finish()
    }

    override fun onErrorReceived(errorResponse: ErrorTransactionResponse) {
        hideSoftKeyboard()
        if (progressDialog!!.isShowing) progressDialog!!.dismiss()
        val error = errorResponse.firstErrorMessage
        CentrlSnackBar.showSnackBar(binding.checkoutSnackar.snackbarCl, error.messageCode)
        LogUtil.mLOGE("onErrorReceived","Code::${error.messageCode}\nmessage::${error.messageText}")
    }

    /* ---------------------- Callback Methods - End -----------------------*/
    private fun hideSoftKeyboard() {
        val keyboard = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        if (this != null && this.currentFocus != null) {
            keyboard.hideSoftInputFromWindow(binding.cardNumberView.windowToken, 0)
        }
    }

    private fun areFormDetailsValid(): Boolean {
        cardNumber = binding.cardNumberView.text.toString().replace(" ", "")
        month = binding.dateMonthView.text.toString()
        cvv = binding.securityCodeView.text.toString()
        year = binding.dateYearView.text.toString()
        if (isEmptyField) {
            binding.buttonCheckoutOrder.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake_error))
            CentrlSnackBar.showSnackBar(binding.checkoutSnackar.snackbarCl, resources.getString(R.string.empty_fields))
            return false
        }
        year = YEAR_PREFIX + binding.dateYearView.text.toString()
        return validateFields()
    }

    private val isEmptyField: Boolean
        get() = cardNumber != null && cardNumber!!.isEmpty() || month != null && month!!.isEmpty() || (year != null
                && year!!.isEmpty()) || cvv != null && cvv!!.isEmpty()

    private fun validateFields(): Boolean {
        if (cardNumber!!.length < MIN_CARD_NUMBER_LENGTH) {
            binding.cardNumberView.requestFocus()
            binding.cardNumberView.error = getString(R.string.invalid_card_number)
            binding.buttonCheckoutOrder.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake_error))
            return false
        }
        val monthNum = month!!.toInt()
        if (monthNum < 1 || monthNum > 12) {
            binding.dateMonthView.requestFocus()
            binding.dateMonthView.error = getString(R.string.invalid_month)
            binding.buttonCheckoutOrder.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake_error))
            return false
        }
        if (month!!.length < MIN_YEAR_LENGTH) {
            binding.dateMonthView.requestFocus()
            binding.dateMonthView.error = getString(R.string.two_digit_month)
            binding.buttonCheckoutOrder.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake_error))
            return false
        }
        if (year!!.length < MIN_YEAR_LENGTH) {
            binding.dateYearView.requestFocus()
            binding.dateYearView.error = getString(R.string.invalid_year)
            binding.buttonCheckoutOrder.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake_error))
            return false
        }
        if (cvv!!.length < MIN_CVV_LENGTH) {
            binding.securityCodeView.requestFocus()
            binding.securityCodeView.error = getString(R.string.invalid_cvv)
            binding.buttonCheckoutOrder.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake_error))
            return false
        }
        return true
    }

    private fun setUpCreditCardEditText() {
        binding.cardNumberView.addTextChangedListener(object : TextWatcher {
            private var spaceDeleted = false
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
            }

            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {
                // check if a space was deleted
                val charDeleted = s.subSequence(start, start + count)
                spaceDeleted = " " == charDeleted.toString()
            }

            override fun afterTextChanged(editable: Editable) {
                // disable text watcher
                binding.cardNumberView.removeTextChangedListener(this)

                // record cursor position as setting the text in the textview
                // places the cursor at the end
                val cursorPosition = binding.cardNumberView.selectionStart
                val withSpaces = formatText(editable)
                binding.cardNumberView.setText(withSpaces)
                // set the cursor at the last position + the spaces added since the
                // space are always added before the cursor
                binding.cardNumberView.setSelection(cursorPosition + (withSpaces.length - editable.length))

                // if a space was deleted also deleted just move the cursor
                // before the space
                if (spaceDeleted) {
                    binding.cardNumberView.setSelection(binding.cardNumberView.selectionStart - 1)
                    spaceDeleted = false
                }

                // enable text watcher
                binding.cardNumberView.addTextChangedListener(this)
            }

            private fun formatText(text: CharSequence): String {
                val formatted = StringBuilder()
                var count = 0
                for (i in text.indices) {
                    if (Character.isDigit(text[i])) {
                        if (count % 4 == 0 && count > 0) formatted.append(" ")
                        formatted.append(text[i])
                        ++count
                    }
                }
                return formatted.toString()
            }
        })
    }

    private fun prepareCardDataFromFields(): CardData {
        return CardData.Builder(cardNumber, month, year).cvvCode(cvv) //CVV Code is optional
            .build()
    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            finish()
            return true
        }
        return super.onKeyDown(keyCode, event)
    }
}