package com.android.centrl.ui.activity

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Canvas
import android.graphics.Color
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.Settings
import android.view.KeyEvent
import android.view.View
import android.widget.ImageView
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import com.android.centrl.R
import com.android.centrl.databinding.ActivityEditProfileBinding
import com.android.centrl.utils.ImageUtils
import com.android.centrl.notificationservice.PrefManager
import com.android.centrl.utils.AdsLoading
import com.android.centrl.utils.AppConstant
import com.android.centrl.utils.CentrlSnackBar
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.github.dhaval2404.imagepicker.ImagePicker
import com.github.dhaval2404.imagepicker.ImagePicker.Companion.getFilePath
import com.github.dhaval2404.imagepicker.ImagePicker.Companion.with
import com.google.android.gms.ads.AdView
import jp.wasabeef.glide.transformations.CropCircleTransformation
import java.io.File
import java.io.FileOutputStream

/**
 * Edit profile name and change profile picture.
 */
class ProfileEditActivity : BaseActivity() {

    private lateinit var binding: ActivityEditProfileBinding
    private var mAdView: AdView? = null
    private var uploadFileName: String? = null
    private var chatID: String? = null
    private var groupIconPath: String? = null
    private var iconPath: String? = null
    private var groupSize: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_edit_profile)
        binding.handler = ClickHandler()
        try{
            val bundle = intent.extras
            if (bundle != null) {
                groupKey = bundle.getString(AppConstant.GROUP_KEY)
            }
            groupSize = mGroupChannelsViewModel!!.getGroupList(groupKey!!).size

            val chat = mGroupChannelsViewModel!!.getGroupDetails(groupKey!!)
            chatID = chat.chatKey
            groupIconPath = chat.groupIconPath
            iconPath = chat.iconPath
            binding.edtProfileUserName.setText(chat.groupTitle)
            if(chat.groupIconPath.isEmpty()){
                binding.editProfileImage.setImageResource(R.drawable.default_user_image)
            } else {
                val profileImage: Bitmap = BitmapFactory.decodeFile(chat.groupIconPath)
                binding.editProfileImage.setImageBitmap(profileImage)
            }

        }catch (e: Exception){
            e.printStackTrace()
        }

        binding.adsContainerEdp.adViewContainer.post {
            // Loading ads
            mAdView = AdsLoading.loadBanner(this@ProfileEditActivity, binding.adsContainerEdp.adViewContainer)
        }

    }
    /**
     * Handling view click event class.
     */
    inner class ClickHandler{
        /**
         * View click event.
         */
        fun onClick(view: View) {
            when (view.id) {
                R.id.edit_profile_back -> { finish() }
                R.id.btn_edit_profile_done -> {
                    try {
                        if (binding.edtProfileUserName.text.toString().isEmpty()) {
                            CentrlSnackBar.showSnackBar(binding.edProfileSnackar.snackbarCl, resources.getString(R.string.enter_user_name))
                        } else {
                            uploadFileName = saveImage(binding.editProfileImage, groupKey!!)
                            if (uploadFileName == null) {
                                if (groupSize == 1){
                                    mGroupChannelsViewModel!!.updateEditSingleProfile(groupKey!!, binding.edtProfileUserName.text.toString(), groupIconPath!!, iconPath!!,0)
                                } else {
                                    mGroupChannelsViewModel!!.updateEditProfileGroup(groupKey!!, binding.edtProfileUserName.text.toString(), groupIconPath!!)
                                }
                                finish()
                            } else {
                                if (groupSize == 1){
                                    mGroupChannelsViewModel!!.updateEditSingleProfile(groupKey!!, binding.edtProfileUserName.text.toString(), uploadFileName!!, uploadFileName!!,1)
                                } else {
                                    mGroupChannelsViewModel!!.updateEditProfileGroup(groupKey!!, binding.edtProfileUserName.text.toString(), uploadFileName!!)
                                }
                                finish()
                            }
                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
                R.id.edit_profile_image_btn -> {
                    try {
                        if (checkPermission()) {
                            with(this@ProfileEditActivity)
                                .crop()
                                .start()
                            PrefManager.getInstance(this@ProfileEditActivity)!!.saveBoolean(AppConstant.SET_IMAGE, true)
                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            }
        }
    }

    override fun onResume() {
        super.onResume()
        PrefManager.getInstance(this)!!.saveBoolean(AppConstant.SET_IMAGE, false)
        if (mAdView != null) {
            AdsLoading.resumeAds(this, mAdView, binding.adsContainerEdp.adViewContainer)
        }
    }

    override fun onPause() {
        if (mAdView != null) {
            mAdView!!.pause()
        }
        super.onPause()
    }

    override fun onDestroy() {
        if (mAdView != null) {
            mAdView!!.destroy()
        }
        PrefManager.getInstance(this)!!.saveBoolean(AppConstant.SET_IMAGE, false)
        super.onDestroy()
    }
    /**
     * Requesting camera and storage permission.
     */
    private fun checkPermission(): Boolean {
        return when {
            ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED -> {
                // Should we show an explanation?
                if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CAMERA)) {
                    // sees the explanation, try again to request the permission.
                    val builder = AlertDialog.Builder(this)
                    builder.setTitle(resources.getString(R.string.permission))
                    builder.setMessage(resources.getString(R.string.camera_txt))
                    builder.setCancelable(false)
                    builder.setPositiveButton(resources.getString(R.string.continue_txt)) { _, _ ->
                        MY_PERMISSIONS_REQUEST_LOCATION.openPermissionSettings(this@ProfileEditActivity)
                    }
                    val dialog = builder.create()
                    dialog.show()
                    dialog.getButton(AlertDialog.BUTTON_POSITIVE).isAllCaps = false
                    dialog.getButton(AlertDialog.BUTTON_NEGATIVE).isAllCaps = false
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        dialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(resources.getColor(R.color.continue_color, null))
                    } else {
                        dialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(Color.parseColor("#4FC3B8"))
                    }
                } else {
                    // No explanation needed, we can request the permission.
                    ActivityCompat.requestPermissions(
                        this, arrayOf(
                            Manifest.permission.CAMERA,
                            Manifest.permission.READ_EXTERNAL_STORAGE,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE
                        ),
                        MY_PERMISSIONS_REQUEST_LOCATION
                    )
                }
                false
            }
            ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED -> {
                // Should we show an explanation?
                if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                    // sees the explanation, try again to request the permission.
                    val builder = AlertDialog.Builder(this)
                    builder.setTitle(resources.getString(R.string.permission))
                    builder.setMessage(resources.getString(R.string.storage_txt))
                    builder.setCancelable(false)
                    builder.setPositiveButton(resources.getString(R.string.continue_txt)) { _, _ ->
                        MY_PERMISSIONS_REQUEST_LOCATION.openPermissionSettings(this@ProfileEditActivity)
                    }
                    val dialog = builder.create()
                    dialog.show()
                    dialog.getButton(AlertDialog.BUTTON_POSITIVE).isAllCaps = false
                    dialog.getButton(AlertDialog.BUTTON_NEGATIVE).isAllCaps = false
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        dialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(resources.getColor(R.color.continue_color, null))
                    } else {
                        dialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(Color.parseColor("#4FC3B8"))
                    }
                } else {
                    // No explanation needed, we can request the permission.
                    ActivityCompat.requestPermissions(
                        this, arrayOf(
                            Manifest.permission.CAMERA,
                            Manifest.permission.READ_EXTERNAL_STORAGE,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE
                        ),
                        MY_PERMISSIONS_REQUEST_LOCATION
                    )
                }
                false
            }
            else -> {
                true
            }
        }
    }
    /**
     * Open permissions screen forcefully for camera and storage access.
     */
    private fun Int.openPermissionSettings(activity: Activity) {
        if (this == MY_PERMISSIONS_REQUEST_LOCATION) {
            val intent =
                Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
            val uri = Uri.parse("package:" + activity.packageName)
            intent.data = uri
            if (intent.resolveActivity(activity.packageManager) != null) activity.startActivity(
                intent
            )
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            MY_PERMISSIONS_REQUEST_LOCATION -> {
                return
            }
        }
    }

    public override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        try {
            com.android.centrl.utils.LogUtil.mLOGE("onActivityResult",""+data!!.extras!!.getString(ImagePicker.EXTRA_ERROR))
            setPicture(getFilePath(data)!!)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }
    private fun setPicture(filePath: String){
        Glide.with(this)
            .load(filePath)
            .error(R.drawable.default_user_image)
            .bitmapTransform(CropCircleTransformation(this))
            .diskCacheStrategy(DiskCacheStrategy.NONE)
            .skipMemoryCache(true)
            .into(binding.editProfileImage)
        PrefManager.getInstance(this)!!.saveBoolean(AppConstant.SET_IMAGE, false)
    }

    private fun viewToBitmap(view: View, widh: Int, hight: Int): Bitmap? {
        val bitmap = Bitmap.createBitmap(widh, hight, Bitmap.Config.ARGB_8888)
        val canvas = Canvas(bitmap)
        view.draw(canvas)
        return bitmap
    }

    /**
     * Save notification icon image to local storage
     */
    private fun saveImage(img: ImageView, fileName: String): String? {
        val appPath: String = filesDir.absolutePath
        val myDir = File("$appPath/saved_images")
        myDir.mkdirs()
        val fName = "$fileName.jpg"
        val file = File(myDir, fName)
        val profilePath = file.absolutePath
        if (file.exists()) file.delete()
        try {
            val out = FileOutputStream(file)
            val finalBitmap = viewToBitmap(img, img.width, img.height)
            val circularBitmap = ImageUtils.getCircularBitmap(finalBitmap)
            circularBitmap!!.setHasAlpha(true)
            circularBitmap.compress(Bitmap.CompressFormat.PNG, 100, out)
            out.flush()
            out.close()
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        }
        return profilePath
    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            finish()
            return true
        }
        return super.onKeyDown(keyCode, event)
    }

    companion object {
        var TAG = "EditProfileActivity"
        const val MY_PERMISSIONS_REQUEST_LOCATION = 99
    }
}