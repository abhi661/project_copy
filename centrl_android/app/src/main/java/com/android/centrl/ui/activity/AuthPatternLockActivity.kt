package com.android.centrl.ui.activity

import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.view.KeyEvent
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.databinding.DataBindingUtil
import com.android.centrl.R
import com.android.centrl.databinding.ActivityAuthPatternLockBinding
import com.android.centrl.ui.dialog.DialogConfirmEmail
import com.android.centrl.ui.views.patternlockview.PatternLockView
import com.android.centrl.ui.views.patternlockview.listener.PatternLockViewListener
import com.android.centrl.ui.views.patternlockview.utils.PatternLockUtils
import com.android.centrl.ui.views.patternlockview.utils.ResourceUtils
import com.android.centrl.utils.AppConstant
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount

/**
 * AuthPattern activity for unlock Centrl
 */
@RequiresApi(Build.VERSION_CODES.M)
class AuthPatternLockActivity : BaseActivity(), DialogConfirmEmail.DialogConfirmEmailListener {
    private var account: GoogleSignInAccount? = null
    private lateinit var binding: ActivityAuthPatternLockBinding
    private var mClassName: String? = null
    /**
     * Pattern lock listener
     */
    private val mPatternLockViewListener: PatternLockViewListener =
        object : PatternLockViewListener {
            override fun onStarted() {}
            override fun onProgress(progressPattern: List<PatternLockView.Dot>) {}

            override fun onComplete(pattern: List<PatternLockView.Dot>) {
                val getPatternLock = preferences!!.getString(AppConstant.PATTERN_LOCK, null)
                if (getPatternLock != null && getPatternLock.isNotEmpty()) {
                    if (getPatternLock == PatternLockUtils.patternToString(binding.patterLockViewAuth, pattern)) {
                        if (mClassName.equals(AppConstant.CLASS_NAME_SPLASH, ignoreCase = true)) {
                            val intent = Intent(this@AuthPatternLockActivity, InteractionsActivity::class.java)
                            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                            startActivity(intent)
                            finish()
                        } else {
                            finish()
                            exitActivityAnimation()
                        }

                    } else {
                        binding.authPatternStatusTxt.text = resources.getString(R.string.wrong_pattern)
                        postClearPatternRunnable()
                    }
                }
            }
            override fun onCleared() {}
        }
    private fun postClearPatternRunnable() {
        removeClearPatternRunnable()
        binding.patterLockViewAuth.postDelayed(
            clearPatternRunnable,
            2000
        )
    }
    private fun removeClearPatternRunnable() {
        binding.patterLockViewAuth.removeCallbacks(clearPatternRunnable)
    }
    private val clearPatternRunnable =
        Runnable { // clearPattern() resets display mode to DisplayMode.Correct.
            binding.patterLockViewAuth.clearPattern()
        }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_auth_pattern_lock)
        account = GoogleSignIn.getLastSignedInAccount(applicationContext)

        val bundle = intent.extras
        if (bundle != null) {
            mClassName = bundle.getString(AppConstant.CLASS_NAME)
        }

        binding.authPatternStatusTxt.text = ""
        binding.patterLockViewAuth.dotCount = 3
        binding.patterLockViewAuth.dotNormalSize = ResourceUtils.getDimensionInPx(this, R.dimen.pattern_lock_dot_size).toInt()
        binding.patterLockViewAuth.dotSelectedSize = ResourceUtils.getDimensionInPx(this, R.dimen.pattern_lock_dot_selected_size).toInt()
        binding.patterLockViewAuth.pathWidth = ResourceUtils.getDimensionInPx(this, R.dimen.pattern_lock_path_width).toInt()
        binding.patterLockViewAuth.isAspectRatioEnabled = true
        binding.patterLockViewAuth.aspectRatio = PatternLockView.AspectRatio.ASPECT_RATIO_HEIGHT_BIAS
        binding.patterLockViewAuth.setViewMode(PatternLockView.PatternViewMode.CORRECT)
        binding.patterLockViewAuth.dotAnimationDuration = 150
        binding.patterLockViewAuth.pathEndAnimationDuration = 50
        binding.patterLockViewAuth.correctStateColor = ResourceUtils.getColor(this, R.color.pattern_drawing)
        binding.patterLockViewAuth.isInStealthMode = false
        binding.patterLockViewAuth.isTactileFeedbackEnabled = true
        binding.patterLockViewAuth.isInputEnabled = true
        binding.patterLockViewAuth.addPatternLockListener(mPatternLockViewListener)

        binding.authForgotPattern.setOnClickListener {
            val intent = Intent(this@AuthPatternLockActivity, PatternLockActivity::class.java)
            intent.data = Uri.parse("https://www.centrl.com/forgot/pattern")
            startActivity(intent)
            finish()
            /*if(account == null){
                confirmEmail("")
            } else {
                confirmEmail(account!!.email)
            }*/
        }
    }

    /**
     * Confirm email to send link dialog.
     */
    private fun confirmEmail(emailId: String?) {
        val dialogAddNewChannels = DialogConfirmEmail()
        val bundle = Bundle()
        bundle.putString(AppConstant.EMAIL, emailId)
        dialogAddNewChannels.arguments = bundle
        dialogAddNewChannels.show(supportFragmentManager, DialogConfirmEmail.TAG)
    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            close()
            return true
        }
        return super.onKeyDown(keyCode, event)
    }

    private fun close(){
        val intent = Intent(this@AuthPatternLockActivity, FinishActivity::class.java)
        intent.action = Intent.ACTION_MAIN
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        startActivity(intent)
        finish()
    }

    override fun onSentLink() {
        Toast.makeText(this, resources.getString(R.string.sent_link), Toast.LENGTH_LONG).show()
    }
}