package com.android.centrl.ui.activity

import android.content.Intent
import android.os.Bundle
import android.view.KeyEvent
import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.android.centrl.R
import com.android.centrl.databinding.ActivitySettingsBinding
import com.android.centrl.notificationservice.PrefManager
import com.android.centrl.ui.dialog.DialogGoPremium
import com.android.centrl.viewmodel.SwitchEDViewModel
import com.android.centrl.utils.AdsLoading
import com.android.centrl.utils.AppConstant
import com.android.centrl.utils.CentrlSnackBar
import com.google.android.gms.ads.AdView

/**
 * Settings activity.
 */
class SettingsActivity : BaseActivity(), DialogGoPremium.DialogGoPremiumListener {

    private var mAdView: AdView? = null
    private lateinit var binding: ActivitySettingsBinding
    private lateinit var switchEDViewModel: SwitchEDViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        switchEDViewModel = ViewModelProvider(this).get(SwitchEDViewModel::class.java)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_settings)
        binding.handler = ClickHandlers()
        binding.switchViewModel = switchEDViewModel
        binding.lifecycleOwner = this

        binding.adsContainerSetting.adViewContainer.post {
            // Loading ads
            mAdView = AdsLoading.loadBanner(this@SettingsActivity, binding.adsContainerSetting.adViewContainer)
        }
    }
    /**
     * View click switch handler class.
     */
   inner class ClickHandlers {
       /**
        * Click handler
        */
        fun onClickListener(view: View) {
            when (view.id) {
                R.id.settings_back -> { finish() }
                R.id.general_ll -> { startActivity(Intent(this@SettingsActivity, GeneralActivity::class.java)) }
                R.id.chat_ll -> { startActivity(Intent(this@SettingsActivity, ChatsBackUpActivity::class.java)) }
                R.id.security_ll -> { startActivity(Intent(this@SettingsActivity, SecurityActivity::class.java)) }
                R.id.help_ll -> { startActivity(Intent(this@SettingsActivity, HelpActivity::class.java)) }
                R.id.invite_ll -> { inviteFriends() }
                R.id.gopremium_ll -> {
                    if(PrefManager.getInstance(this@SettingsActivity)!!.isEnabled(AppConstant.PREMIUM, false)){
                        CentrlSnackBar.showSnackBar(binding.settingsSnackar.snackbarCl, resources.getString(R.string.premium_user))
                    } else {
                        val dialogAddFoodLog = DialogGoPremium()
                        val bundle = Bundle()
                        dialogAddFoodLog.arguments = bundle
                        dialogAddFoodLog.show(supportFragmentManager, DialogGoPremium.TAG)
                    }

                }
                R.id.keyboard_ll -> { startActivity(Intent(this@SettingsActivity, KeyBoardSettingsActivity::class.java)) }
            }
        }
        /**
         * switch handler.
         */
       fun saveGoPremium(isChecked: Boolean) {
           if(isChecked && !PrefManager.getInstance(this@SettingsActivity)!!.isEnabled(AppConstant.PREMIUM, false)){
               val dialogAddFoodLog = DialogGoPremium()
               val bundle = Bundle()
               dialogAddFoodLog.arguments = bundle
               dialogAddFoodLog.show(supportFragmentManager, DialogGoPremium.TAG)
           }
       }
   }

    override fun onResume() {
        super.onResume()
      /*  if(PrefManager.getInstance(this)!!.isEnabled(AppConstant.PREMIUM, false)){
            binding.gopremiumLl.visibility = View.GONE
            binding.switchViewModel!!.setGoPremium(true)
        } else {
            binding.gopremiumLl.visibility = View.VISIBLE
            binding.switchViewModel!!.setGoPremium(false)
        }*/
        if (mAdView != null) {
            AdsLoading.resumeAds(this, mAdView, binding.adsContainerSetting.adViewContainer)
        }
    }

    override fun onPause() {
        if (mAdView != null) {
            mAdView!!.pause()
        }
        super.onPause()
    }

    override fun onDestroy() {
        if (mAdView != null) {
            mAdView!!.destroy()
        }
        super.onDestroy()
    }
    /**
     * Invite friends to use Centrl app.
     */
    private fun inviteFriends() {
        try {
            val shareIntent = Intent(Intent.ACTION_SEND)
            shareIntent.type = "text/plain"
            shareIntent.putExtra(Intent.EXTRA_TEXT, "https://play.google.com/store/apps/details?id=com.android.centrl&hl=en")
            startActivity(Intent.createChooser(shareIntent, "Invite a friend via..."))
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            finish()
            return true
        }
        return super.onKeyDown(keyCode, event)
    }

    override fun onCancel() {
        binding.gopremiumLl.visibility = View.VISIBLE
        binding.switchViewModel!!.setGoPremium(false)
    }
}