package com.android.centrl.adapter

import android.content.Context
import android.util.Patterns
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.android.centrl.R
import com.android.centrl.persistence.entity.BlockedContactsEntity
import java.util.*

/**
 * Blocked contacts list adapter
 */
class BlockedContactListAdapter(context: Context, chatArrayList: ArrayList<BlockedContactsEntity>?, removeIdentityInterface: RemoveContactsInterface) : RecyclerView.Adapter<BlockedContactListAdapter.BlockedContactViewHolder>() {
    private var identityList: ArrayList<BlockedContactsEntity>? = null
    private var context: Context
    private var removeIdentityInterface: RemoveContactsInterface

    /**
     * Init list and listener
     */
    init {
        identityList = chatArrayList
        this.context = context
        this.removeIdentityInterface = removeIdentityInterface
    }
    // Inflates the appropriate layout according to the ViewType.
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BlockedContactViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val view = layoutInflater.inflate(R.layout.item_ignored_contact, parent, false)
        return BlockedContactViewHolder(view)
    }

    // Passes the message object to a ViewHolder so that the contents can be bound to UI.
    override fun onBindViewHolder(holder: BlockedContactViewHolder, position: Int) {
        val chat = identityList!![position]
        // If the message is a received message.
        if (chat.number.isNotEmpty()) {
            if (isValidMobile(chat.number)) {
                holder.identityTitleTxt!!.text = context.resources.getString(R.string.unknown)
                holder.identityTitleTxt!!.visibility = View.VISIBLE
                holder.identityNumberTxt!!.visibility = View.VISIBLE
                holder.identityNumberTxt!!.text = chat.number
            } else {
                holder.identityTitleTxt!!.visibility = View.VISIBLE
                holder.identityNumberTxt!!.visibility = View.GONE
                holder.identityTitleTxt!!.text = chat.number
            }
        }
        holder.identityRemoveTxt!!.setOnClickListener { removeIdentityInterface.removeContacts(chat) }
    }


    override fun getItemCount(): Int {
        return identityList!!.size
    }

    private fun isValidMobile(phone: String): Boolean {
        return Patterns.PHONE.matcher(phone).matches()
    }
    /**
     * Adapter view holder
     */
    class BlockedContactViewHolder(itemView: View?) : RecyclerView.ViewHolder(itemView!!) {
        var identityTitleTxt: TextView? = null
        var identityNumberTxt: TextView? = null
        var identityRemoveTxt: TextView? = null

        init {
            if (itemView != null) {
                identityTitleTxt = itemView.findViewById<View>(R.id.ignored_title_text_view) as TextView
                identityNumberTxt = itemView.findViewById<View>(R.id.ignored_number_text_view) as TextView
                identityRemoveTxt = itemView.findViewById<View>(R.id.ignored_remove) as TextView
            }
        }
    }
    /**
     * Update list and refresh list
     */
    fun updateList(chatArrayList: ArrayList<BlockedContactsEntity>?) {
        identityList = chatArrayList
        //now, tell the adapter about the update
        notifyDataSetChanged()
    }
    /**
     * Unblocked interface class
     */
    interface RemoveContactsInterface {
        fun removeContacts(chat: BlockedContactsEntity?)
    }

   
}