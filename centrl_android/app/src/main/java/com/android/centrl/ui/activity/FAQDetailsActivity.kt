package com.android.centrl.ui.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.android.centrl.R
import kotlinx.android.synthetic.main.activity_f_a_q_details.*

class FAQDetailsActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_f_a_q_details)

        val bundle = intent.extras
        if (bundle != null) {
            val mHeader = bundle.getString("header")
            val mContent = bundle.getString("content")
            faq_details_title.text = mHeader
            faq_details_content.text = mContent

        }
        faq_details_back.setOnClickListener {
            finish()
        }
        faq_home_btn.setOnClickListener {
            finish()
        }
    }
}