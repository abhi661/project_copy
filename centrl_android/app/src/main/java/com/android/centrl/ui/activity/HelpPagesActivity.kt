package com.android.centrl.ui.activity

import android.os.Bundle
import android.view.KeyEvent
import androidx.databinding.DataBindingUtil
import com.android.centrl.R
import com.android.centrl.databinding.ActivityHelpPagesBinding
import com.android.centrl.utils.AdsLoading
import com.google.android.gms.ads.AdView

/**
 * Help pages activity.
 */
class HelpPagesActivity : BaseActivity() {

    private lateinit var binding: ActivityHelpPagesBinding
    private var mAdView: AdView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_help_pages)

        val bundle = intent.extras
        if (bundle != null) {
            val mHeader = bundle.getString("help_title")
            binding.helpPagesTitle.text = mHeader
        }
        binding.faqBack.setOnClickListener { finish() }
        binding.helpPageAds.adViewContainer.post {
            // Loading ads
            mAdView = AdsLoading.loadBanner(this@HelpPagesActivity, binding.helpPageAds.adViewContainer)
        }
    }

    override fun onResume() {
        super.onResume()
        if (mAdView != null) {
            AdsLoading.resumeAds(this, mAdView, binding.helpPageAds.adViewContainer)
        }
    }

    override fun onPause() {
        if (mAdView != null) {
            mAdView!!.pause()
        }
        super.onPause()
    }

    override fun onDestroy() {
        if (mAdView != null) {
            mAdView!!.destroy()
        }
        super.onDestroy()
    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            finish()
            return true
        }
        return super.onKeyDown(keyCode, event)
    }
}