package com.android.centrl.persistence.repositry

import android.content.Context
import com.android.centrl.persistence.dao.KeyboardAppTrayDao
import com.android.centrl.persistence.dao.RemovedKeyboardAppsDao
import com.android.centrl.persistence.database.CentrlDatabase
import com.android.centrl.persistence.entity.KeyboardAppsTrayEntity
import io.reactivex.Completable
import io.reactivex.CompletableObserver
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import org.jetbrains.anko.doAsyncResult
import kotlin.collections.ArrayList

/**
 * Access point for managing keyBoardApps data.
 */
class KeyBoardAppsRepository(context: Context) {

    private var keyboardAppTrayDao: KeyboardAppTrayDao
    private var removedKeyboardAppsDao: RemovedKeyboardAppsDao

    init {
        val centrlDatabase = CentrlDatabase.getInstance(context)
        keyboardAppTrayDao = centrlDatabase.keyBoardAppTrayDao()
        removedKeyboardAppsDao = centrlDatabase.removedKeyBoardAppsDao()
    }

    /**
     * Insert a KeyboardAppTray apps details in the database. If the KeyboardAppTray apps details already exists, replace it.
     *
     * @param keyboardAppsEntity the KeyboardAppTray apps details to be inserted.
     */
    fun insertKeyboardAppTray(keyboardAppsEntity: KeyboardAppsTrayEntity) {
        try {
            Completable.fromAction {
                keyboardAppTrayDao.insertKeyboardAppsTray(keyboardAppsEntity)
            }.observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(object : CompletableObserver {
                    override fun onSubscribe(d: Disposable) {}
                    override fun onComplete() {}
                    override fun onError(e: Throwable) {}
                })
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    /**
     * @return the KeyboardAppTray app package name from the table
     */
    fun getAllKeyboardAppTrayPackCode(): ArrayList<Int> {
        return getAppTrayPackCodeAsyncTask()
    }

    /**
     * @return the KeyboardAppTray app package name from the table
     */
    fun getAllKeyboardAppTrayPackName(): ArrayList<String> {
        return getKeyboardAppsTrayAsyncTask()
    }

    /**
     * @return the KeyboardAppTray app package name from the table
     */
    fun deleteKeyboardAppTray(code: Int) {
        try {
            Completable.fromAction {
                keyboardAppTrayDao.deleteKeyboardAppsTray(code)
            }.observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(object : CompletableObserver {
                    override fun onSubscribe(d: Disposable) {}
                    override fun onComplete() {}
                    override fun onError(e: Throwable) {}
                })
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    /**
     * @return the KeyboardAppTray app package name from the table
     */
    fun getAllRemovedChatAppPackages(): ArrayList<Int> {
        return getRemovedAppPackagesAsyncTask()
    }

    /**
     * Delete apps from KeyboardAppTray.
     */
    fun deleteKeyboardApps(code: Int) {
        try {
            Completable.fromAction {
                removedKeyboardAppsDao.deleteKeyboardApps(code)
            }.observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(object : CompletableObserver {
                    override fun onSubscribe(d: Disposable) {}
                    override fun onComplete() {}
                    override fun onError(e: Throwable) {}
                })
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    /**
     * AsyncTask for get all KeyboardAppTray from database
     */
    private fun getKeyboardAppsTrayAsyncTask(): ArrayList<String> {
        val result = doAsyncResult(null,{
            keyboardAppTrayDao.getAllKeyboardAppsTrayPackName() as ArrayList<String>
        })
        return result.get()
    }

    /**
     * AsyncTask for get all KeyboardAppTray package code from database
     */
    private fun getAppTrayPackCodeAsyncTask(): ArrayList<Int> {
        val result = doAsyncResult(null,{
            keyboardAppTrayDao.getAllKeyboardAppsTrayPackCode() as ArrayList<Int>
        })
        return result.get()
    }

    /**
     * AsyncTask for get all removed apps package code from database
     */
    private fun getRemovedAppPackagesAsyncTask(): ArrayList<Int> {
        val result = doAsyncResult(null,{
            removedKeyboardAppsDao.getRemovedChatAppPackages() as ArrayList<Int>
        })
        return result.get()
    }
}