package com.android.centrl.persistence.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

/**
 * Entity model class for a installed apps table
 */
@Entity(tableName = "installedApps")
data class InstalledAppsEntity(
    @PrimaryKey
    var packageCode: Int,
    var packagesName: String
)