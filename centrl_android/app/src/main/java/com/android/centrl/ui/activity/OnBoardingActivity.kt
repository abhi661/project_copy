package com.android.centrl.ui.activity

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import com.android.centrl.R
import com.android.centrl.adapter.OnBoardingPagerAdapter
import com.android.centrl.adapter.OnBoardingPagerAdapter.SimplePagerAdapterListener
import com.android.centrl.databinding.ActivityOnboardingBinding
import com.android.centrl.utils.AppConstant
import kotlinx.android.synthetic.main.activity_onboarding.*

/**
 * OnBoarding activity.
 */
class OnBoardingActivity : BaseActivity(), SimplePagerAdapterListener {

    private lateinit var binding: ActivityOnboardingBinding
    private var mSimplePagerAdapter: OnBoardingPagerAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_onboarding)
        binding.handler = ClickHandler()

        btn_start_rl.visibility = View.INVISIBLE
        mSimplePagerAdapter = OnBoardingPagerAdapter()
        mSimplePagerAdapter!!.setListener(this)
        mSimplePagerAdapter!!.addPage(this)
        viewpager.adapter = mSimplePagerAdapter
        viewPagerIndicator.initWithViewPager(viewpager)
    }

    /**
     * Views click handler class
     */
    inner class ClickHandler{
        fun onClick(view: View){
            when(view.id){
                R.id.prev_btn -> {
                    val current = viewpager.currentItem
                    val back = current - 1
                    viewpager.setCurrentItem(back, true)
                }
                R.id.next_btn -> {
                    val current = viewpager.currentItem
                    val next = current + 1
                    viewpager.setCurrentItem(next, true)
                }
                R.id.skip_btn -> {
                    editor!!.putBoolean(AppConstant.INTRO_SCREEN_FINISHED, true)
                    editor!!.commit()
                    startActivity(Intent(this@OnBoardingActivity, PermissionActivity::class.java))
                    finish()
                }
                R.id.start_btn -> {
                    editor!!.putBoolean(AppConstant.INTRO_SCREEN_FINISHED, true)
                    editor!!.commit()
                    startActivity(Intent(this@OnBoardingActivity, PermissionActivity::class.java))
                    finish()
                }
            }
        }
    }

    /**
     * Change the views while scrolling pages
     */
    override fun onViewsInMemoryChanged(totalViewsInMemory: Int) {
        if (null == viewpager) {
            return
        }
        when (viewpager.currentItem) {
            0 -> {
                prev_btn.visibility = View.GONE
                prev_btn_disabled.visibility = View.VISIBLE
                next_btn.visibility = View.VISIBLE
                next_btn_disabled.visibility = View.GONE
                btn_start_rl.visibility = View.INVISIBLE
                skip_btn.visibility = View.VISIBLE
            }
            1 -> {
                prev_btn.visibility = View.VISIBLE
                prev_btn_disabled.visibility = View.GONE
                next_btn.visibility = View.VISIBLE
                next_btn_disabled.visibility = View.GONE
                btn_start_rl.visibility = View.INVISIBLE
                skip_btn.visibility = View.VISIBLE
            }
            2 -> {
                prev_btn.visibility = View.VISIBLE
                prev_btn_disabled.visibility = View.GONE
                next_btn!!.visibility = View.GONE
                next_btn_disabled!!.visibility = View.VISIBLE
                btn_start_rl.visibility = View.VISIBLE
                skip_btn.visibility = View.GONE
            }
        }
    }
}