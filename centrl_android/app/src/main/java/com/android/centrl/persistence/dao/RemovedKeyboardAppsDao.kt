package com.android.centrl.persistence.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.android.centrl.persistence.entity.RemovedKeyboardAppsEntity
import io.reactivex.Completable
import io.reactivex.Flowable

/**
 * Data Access Object for the removed keyboard apps table.
 */
@Dao
interface RemovedKeyboardAppsDao {
    /**
     * Insert a removed keyboard apps details in the database. If the removed keyboard apps details already exists, replace it.
     *
     * @param removedKeyboardAppsEntity the removed keyboard apps details to be inserted.
     */
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertRemovedKeyboardApps(removedKeyboardAppsEntity: RemovedKeyboardAppsEntity): Completable

    /**
     * Get the removed keyboard details from the table. Since for simplicity we only have one backup details in the database,
     *
     * @return the removed keyboard apps details from the table
     */
    @Query("SELECT packageCode FROM removedKeyboardApps")
    fun getAllRemovedChatAppPackages(): Flowable<List<Int>>

    /**
     * Delete all removed keyboard details.
     */

    @Query("DELETE FROM removedKeyboardApps WHERE packageCode = :code")
    fun deleteKeyboardApps(code: Int)

    /**
     * @return the removed keyboard apps details from the table
     */
    @Query("SELECT packageCode FROM removedKeyboardApps")
    fun getRemovedChatAppPackages(): List<Int>
}