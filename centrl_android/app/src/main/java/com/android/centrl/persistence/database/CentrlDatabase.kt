package com.android.centrl.persistence.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.migration.Migration
import androidx.sqlite.db.SupportSQLiteDatabase
import com.android.centrl.persistence.dao.*
import com.android.centrl.persistence.entity.*
import com.android.centrl.utils.AppConstant

/**
 * The Room database that contains the ChatBackup table
 */
@Database(
    entities = [BlockedContactsEntity::class, ChatsCountsEntity::class, ChatsEntity::class, GroupChannelsEntity::class, InstalledAppsEntity::class, KeyboardAppsTrayEntity::class, MultiChatChannelsEntity::class, RemovedKeyboardAppsEntity::class],
    version = 1,
    exportSchema = false
)
abstract class CentrlDatabase : RoomDatabase() {
    /**
     * Installed apps in device Dao
     */
    abstract fun installedAppsDao(): InstalledAppsDao

    /**
     * Keyboard apps tray Dao
     */
    abstract fun keyBoardAppTrayDao(): KeyboardAppTrayDao

    /**
     * Removed keyboard tray Dao
     */
    abstract fun removedKeyBoardAppsDao(): RemovedKeyboardAppsDao

    /**
     * Blocked chat contacts Dao
     */
    abstract fun bockedContactsDao(): BlockedContactsDao

    /**
     * Multi channels counts Dao
     */
    abstract fun multiChannelDao(): MultiChannelsDao

    /**
     * Chats count Dao
     */
    abstract fun chatsCountDao(): ChatsCountDao

    /**
     * Group channel Dao
     */
    abstract fun groupChannelsDao(): GroupChannelsDao

    /**
     * Chats details Dao
     */
    abstract fun chatsDao(): ChatsDao

    companion object {

        /**
         * Create centrl database.
         */
        fun getInstance(context: Context): CentrlDatabase = Room.databaseBuilder(
            context.applicationContext,
            CentrlDatabase::class.java,
            AppConstant.DATA_BASE
        ).setJournalMode(JournalMode.TRUNCATE).build()
    }
}