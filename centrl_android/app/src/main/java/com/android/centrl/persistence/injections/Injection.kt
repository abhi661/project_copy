package com.android.centrl.persistence.injections

import android.app.Application
import android.content.Context
import com.android.centrl.persistence.database.CentrlBackupDatabase
import com.android.centrl.persistence.repositry.ChatBackupRepository
import com.android.centrl.persistence.datasource.ChatBackupDataSource
import com.android.centrl.viewmodel.ChatBackupViewModelFactory

/**
 * Enables injection of data sources.
 */
object Injection {
    /**
     * Provide centrl data source
     */
    private fun provideCentrlDataSource(context: Context?): ChatBackupDataSource {
        val database = CentrlBackupDatabase.getInstance(context!!)
        return ChatBackupRepository(database.chatBackupDao())
    }
    /**
     * Provide view model factory
     */
    fun provideViewModelFactory(application: Application, context: Context?): ChatBackupViewModelFactory {
        val dataSource = provideCentrlDataSource(context)
        return ChatBackupViewModelFactory(application, dataSource)
    }
}