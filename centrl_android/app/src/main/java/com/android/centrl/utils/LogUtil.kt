package com.android.centrl.utils

import android.util.Log

/**
 * Centrl log util class
 */
object LogUtil {
    private const val LOG_LENGTH = 9
    fun mLOGE(str: String, str2: String) {
        Log.e(str, str2)
    }

    @JvmStatic
    fun mLOGI(str: String, str2: String) {
        Log.i(str, "...$str2")
    }

    fun mLOGW(str: String, str2: String) {
        Log.w(str, str2)
    }

    private fun makeLogTag(str: String): String {
        val str2 = "Centrl_"
        if (str.length > 23 - LOG_LENGTH) {
            val sb = StringBuilder()
            sb.append(str2)
            sb.append(str.substring(0, 23 - LOG_LENGTH - 1))
            return sb.toString()
        }
        val sb2 = StringBuilder()
        sb2.append(str2)
        sb2.append(str)
        return sb2.toString()
    }

    @JvmStatic
    fun makeLogTag(cls: Class<*>): String {
        return makeLogTag(cls.simpleName)
    }

    @JvmStatic
    fun LOGI(str: String, s: String) {
        Log.i(str, "...$s")
    }

    @JvmStatic
    fun LOGE(s: String, s1: String) {
        Log.e(s, "...$s1")
    }
}