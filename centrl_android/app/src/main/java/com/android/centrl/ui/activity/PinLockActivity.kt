package com.android.centrl.ui.activity

import android.net.Uri
import android.os.Bundle
import android.view.KeyEvent
import androidx.fragment.app.Fragment
import com.android.centrl.interfaces.PinLockInterface
import com.android.centrl.R
import com.android.centrl.ui.fragment.PinLockFirstFragment
import com.android.centrl.ui.fragment.PinLockSecondFragment

/**
 * PIN lock setup activity.
 */
class PinLockActivity : BaseActivity(), PinLockInterface {

    private var isResetPin: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pin_lock)
        isResetPin = false
        val intent = intent
        val data: Uri? = intent.data
        if(data != null){
            if(data.toString() == "https://www.centrl.com/forgot/pin"){
                isResetPin = true
            }
        }
        val fragmentFirstPin: Fragment = PinLockFirstFragment()
        val fragmentTransaction = supportFragmentManager.beginTransaction()
        fragmentTransaction.add(R.id.pin_container, fragmentFirstPin)
        fragmentTransaction.commit()
    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            finish()
            return true
        }
        return super.onKeyDown(keyCode, event)
    }

    override fun firstPinLock(pin: String) {
        PinLockSecondFragment.newInstance(pin, isResetPin)
        val fragmentSecondPin: Fragment = PinLockSecondFragment()
        val ft = supportFragmentManager.beginTransaction()
        ft.replace(R.id.pin_container, fragmentSecondPin)
        ft.addToBackStack(null)
        ft.commit()
    }
}