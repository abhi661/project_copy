package com.android.centrl.models

/**
 * Chat and mail object class for separate receiver and sender.
 */
abstract class ListObject {
    abstract fun getType(userId: Int): Int

    companion object {
        const val TYPE_DATE = 0
        const val TYPE_GENERAL_RIGHT = 1
        const val TYPE_GENERAL_LEFT = 2
        const val TYPE_ADS = 3
    }
}