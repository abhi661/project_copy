package com.android.centrl.ui.activity

import android.annotation.SuppressLint
import android.content.ClipData
import android.os.Bundle
import android.util.DisplayMetrics
import android.view.*
import android.widget.*
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.android.centrl.R
import com.android.centrl.databinding.ActivityKeyBoardSettingsBinding
import com.android.centrl.notificationservice.PendingIntentManager
import com.android.centrl.persistence.entity.KeyboardAppsTrayEntity
import com.android.centrl.persistence.entity.RemovedKeyboardAppsEntity
import com.android.centrl.persistence.injections.KeyBoardTrayInjection
import com.android.centrl.utils.AdsLoading
import com.android.centrl.utils.AppConstant
import com.android.centrl.utils.CentrlUtil
import com.android.centrl.viewmodel.KeyBoardAppsViewModel
import com.android.centrl.viewmodel.KeyBoardAppsViewModelFactory
import com.google.android.gms.ads.AdView

/**
 * App tray keyBoard settings activity.
 */
class KeyBoardSettingsActivity : BaseActivity() {

    private lateinit var binding: ActivityKeyBoardSettingsBinding
    private lateinit var mViewModel: KeyBoardAppsViewModel
    private lateinit var mViewModelFactory: KeyBoardAppsViewModelFactory
    private var mAdView: AdView? = null
    private var appId = 0
    private var mWidthHeight = 100
    private var isDropped: Boolean = false


    @SuppressLint("ClickableViewAccessibility")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_key_board_settings)

        mViewModelFactory = KeyBoardTrayInjection.provideViewModelFactory(application, this)
        mViewModel = ViewModelProvider(this, mViewModelFactory).get(KeyBoardAppsViewModel::class.java)

        binding.keyboardTray.appTray.setOnDragListener(MyDragListener())
        binding.keyboardTray.appList.setOnDragListener(MyDragListener())

        binding.keyboardSettingsBack.setOnClickListener { finish() }
        displayDensity
        binding.keyboardAds.adViewContainer.post {
            // loading ads
            mAdView = AdsLoading.loadBanner(this@KeyBoardSettingsActivity, binding.keyboardAds.adViewContainer)
        }
        // Show keyboard apps tray
        mViewModel.mAppsTrayPackages.observe(this, Observer { trayApps ->
            try {
                binding.keyboardTray.appTray.removeAllViews()
                for (i in trayApps.indices) {
                    val packCode = trayApps[i]
                    if (packCode != 0) {
                        val image = ImageView(this)
                        val vp = RelativeLayout.LayoutParams(mWidthHeight, mWidthHeight)
                        image.layoutParams = vp
                        image.setPadding(15, 15, 15, 15)
                        image.id = packCode
                        image.setImageResource(CentrlUtil.getChannelsLogo()[packCode]!!)
                        image.setOnTouchListener(MyTouchListener())
                        binding.keyboardTray.appTray.addView(image)
                    }
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        })
        // Show removed apps from keyboard tray
        mViewModel.mRemovedAppsPackage.observe(this, Observer { removedApps ->
            try {
                binding.keyboardTray.appList.removeAllViews()
                if (removedApps.size != 0) {
                    for (packCode in removedApps) {
                        val image = ImageView(this)
                        val vp = LinearLayout.LayoutParams(mWidthHeight, mWidthHeight)
                        image.layoutParams = vp
                        image.setPadding(15, 15, 15, 15)
                        image.id = packCode
                        image.setImageResource(CentrlUtil.getChannelsLogo()[packCode]!!)
                        image.setOnTouchListener(MyTouchListener())
                        binding.keyboardTray.appList.addView(image)
                    }
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        })


    }

    override fun onResume() {
        super.onResume()
        mViewModel.getAllKeyboardAppTrayPackCode()
        mViewModel.getAllRemovedChatAppPackages()
        if (mAdView != null) {
            AdsLoading.resumeAds(this, mAdView, binding.keyboardAds.adViewContainer)
        }
    }


    /**
     * Get screen size for config app try
     */
    private val displayDensity: Unit
        get() {
            try {
                when (resources.displayMetrics.densityDpi) {
                    DisplayMetrics.DENSITY_LOW, DisplayMetrics.DENSITY_MEDIUM -> {
                        mWidthHeight = 100
                        // LDPI
                        binding.keyboardTray.appTray.columnCount = 4
                        binding.keyboardTray.appList.columnCount = 4
                    }
                    DisplayMetrics.DENSITY_TV, DisplayMetrics.DENSITY_HIGH -> {
                        mWidthHeight = 100
                        // HDPI
                        binding.keyboardTray.appTray.columnCount = 5
                        binding.keyboardTray.appList.columnCount = 5
                    }
                    DisplayMetrics.DENSITY_XHIGH, DisplayMetrics.DENSITY_280 -> {
                        mWidthHeight = 100
                        // XHDPI
                        binding.keyboardTray.appTray.columnCount = 6
                        binding.keyboardTray.appList.columnCount = 6
                    }
                    DisplayMetrics.DENSITY_XXHIGH, DisplayMetrics.DENSITY_360, DisplayMetrics.DENSITY_400, DisplayMetrics.DENSITY_420, DisplayMetrics.DENSITY_440, DisplayMetrics.DENSITY_450 -> {
                        mWidthHeight = 125
                        // XXHDPI
                        binding.keyboardTray.appTray.columnCount = 7
                        binding.keyboardTray.appList.columnCount = 7
                    }
                    DisplayMetrics.DENSITY_XXXHIGH, DisplayMetrics.DENSITY_560 -> {
                        mWidthHeight = 125
                        // XXXHDPI
                        binding.keyboardTray.appTray.columnCount = 10
                        binding.keyboardTray.appList.columnCount = 10
                    }
                    DisplayMetrics.DENSITY_600 -> {
                        mWidthHeight = 125
                        // XXXHDPI
                        binding.keyboardTray.appTray.columnCount = 11
                        binding.keyboardTray.appList.columnCount = 11
                    }
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    /**
     * View touch handler
     */
    @Suppress("DEPRECATION")
    private inner class MyTouchListener : View.OnTouchListener {
        @SuppressLint("ClickableViewAccessibility")
        override fun onTouch(view: View, motionEvent: MotionEvent): Boolean {
            return if (motionEvent.action == MotionEvent.ACTION_DOWN) {
                appId = view.id
                val data = ClipData.newPlainText("", "")
                val shadowBuilder = View.DragShadowBuilder(view)
                view.startDrag(data, shadowBuilder, view, 0)
                view.visibility = View.INVISIBLE
                true
            } else {
                false
            }
        }
    }
    /**
     * Drag and drop channels to keyboard tray for receiving messages.
     */
    internal inner class MyDragListener : View.OnDragListener {
        override fun onDrag(v: View, event: DragEvent): Boolean {
            val removedKeyBoardAppTray = R.id.app_list
            val keyBoardAppTray = R.id.app_tray
            when (event.action) {
                DragEvent.ACTION_DRAG_STARTED -> { isDropped = false }
                DragEvent.ACTION_DRAG_ENTERED -> v.setBackgroundResource(R.drawable.shape_droptarget)
                DragEvent.ACTION_DRAG_EXITED -> v.setBackgroundResource(R.drawable.shape)
                DragEvent.ACTION_DROP -> try {
                    isDropped = true
                    // Dropped, reassign View to ViewGroup
                    val view = event.localState as View
                    val owner = view.parent as ViewGroup
                    owner.removeView(view)
                    val container = v as GridLayout
                    container.addView(view)
                    view.visibility = View.VISIBLE
                    if (v.getId() == keyBoardAppTray) { // Add channels to app tray
                        val mAllPackages = mViewModel.getKeyboardAppTrayPackCodeList()
                        var isAlready = false
                        if (mAllPackages.size != 0) {
                            var i = 0
                            while (i < mAllPackages.size) {
                                val packCode = mAllPackages[i]
                                if (packCode == appId) {
                                    isAlready = true
                                }
                                i++
                            }
                            if (!isAlready) {
                                mViewModel.insertKeyboardAppTray(KeyboardAppsTrayEntity(appId, installedAppsViewModel!!.getInstalledPackageName(appId)))
                                mViewModel.deleteKeyboardApps(appId)
                                Thread.sleep(AppConstant.SLEEP)
                                PendingIntentManager.getInstance(this@KeyBoardSettingsActivity).getPackageList()
                            }
                        } else {
                            mViewModel.insertKeyboardAppTray(KeyboardAppsTrayEntity(appId, installedAppsViewModel!!.getInstalledPackageName(appId)))
                            mViewModel.deleteKeyboardApps(appId)
                            Thread.sleep(AppConstant.SLEEP)
                            PendingIntentManager.getInstance(this@KeyBoardSettingsActivity).getPackageList()
                        }
                    } else if (v.getId() == removedKeyBoardAppTray) { // Remove channels from app tray
                        val packCodeList = mViewModel.getRemovedAppPackagesList()
                        var isAlready = false
                        if (packCodeList.size != 0) {
                            for (packCode in packCodeList) {
                                if (packCode == appId) {
                                    isAlready = true
                                }
                            }
                            if (!isAlready) {
                                mViewModel.insertRemovedKeyboardApps(RemovedKeyboardAppsEntity(appId))
                                mViewModel.deleteKeyboardAppTray(appId)
                                Thread.sleep(AppConstant.SLEEP)
                                PendingIntentManager.getInstance(this@KeyBoardSettingsActivity).getPackageList()
                            }
                        } else {
                            mViewModel.insertRemovedKeyboardApps(RemovedKeyboardAppsEntity(appId))
                            mViewModel.deleteKeyboardAppTray(appId)
                            Thread.sleep(AppConstant.SLEEP)
                            PendingIntentManager.getInstance(this@KeyBoardSettingsActivity).getPackageList()
                        }
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
                DragEvent.ACTION_DRAG_ENDED -> {
                    v.setBackgroundResource(R.drawable.shape)
                    if(!isDropped){
                        mViewModel.getAllKeyboardAppTrayPackCode()
                        mViewModel.getAllRemovedChatAppPackages()
                    }
                }
                else -> { isDropped = false }
            }
            return true
        }
    }

    override fun onPause() {
        if (mAdView != null) {
            mAdView!!.pause()
        }
        super.onPause()
    }

    override fun onDestroy() {
        if (mAdView != null) {
            mAdView!!.destroy()
        }
        super.onDestroy()
    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            finish()
            return true
        }
        return super.onKeyDown(keyCode, event)
    }
}