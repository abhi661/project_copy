package com.android.centrl.persistence.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.android.centrl.persistence.entity.MultiChatChannelsEntity

/**
 * Data Access Object for the multi channel table.
 */
@Dao
interface MultiChannelsDao {

    /**
     * Insert a multi channel details in the database. If the multi channel details already exists, replace it.
     *
     * @param multiChatChannelsEntity the multi channel details to be inserted.
     */
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertMultiChatIdentity(multiChatChannelsEntity: MultiChatChannelsEntity)

    /**
     *
     * @return the group multi channel details from the table
     */
    @Query("SELECT * FROM multiChats")
    fun getAllGroupMultiChat(): List<MultiChatChannelsEntity>

    /**
     * @return the package code from the table
     */
    @Query("SELECT packageCode FROM multiChats WHERE packageCode = :packCode and groupID = :groupId")
    fun getPackageCode(packCode: Int, groupId: String): Int

    /**
     * @return the all group package code from the table
     */
    @Query("SELECT * FROM multiChats WHERE groupID = :groupId")
    fun getGroupAllPackageCode(groupId: String): List<MultiChatChannelsEntity>

    /**
     * Delete multi channel details.
     */
    @Query("DELETE FROM multiChats WHERE groupID = :groupId")
    fun deleteGroupAllPackageCode(groupId: String)
}