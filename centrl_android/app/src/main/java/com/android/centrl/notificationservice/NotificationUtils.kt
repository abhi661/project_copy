package com.android.centrl.notificationservice

import android.app.ActivityManager
import android.app.KeyguardManager
import android.content.ComponentName
import android.content.Context
import android.content.pm.PackageManager
import android.os.Bundle
import android.os.Parcel
import android.text.TextUtils
import androidx.core.app.NotificationManagerCompat
import com.android.centrl.services.NotificationCatchService
import com.android.centrl.utils.DateUtil
import com.android.centrl.utils.LogUtil
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException
import java.util.*
import kotlin.experimental.and

/**
 * Centrl notification utils class
 */
object NotificationUtils {

    fun isAfterDays(j: Long, i: Int): Boolean {
        val instance = Calendar.getInstance()
        instance.timeInMillis = j
        return DateUtil.getDiffDay(instance).toLong() > i.toLong()
    }
    /**
     * Check screen is locked
     */
    fun isLocked(context: Context): Boolean {
        return try {
            (context.getSystemService(Context.KEYGUARD_SERVICE) as KeyguardManager).inKeyguardRestrictedInputMode()
        } catch (unused: Exception) {
            false
        }
    }
    /**
     * Check Centrl notification is crashed
     */
    fun isNLServiceCrashed(context: Context): Boolean {
        val runningServices: List<*>? = (context.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager).getRunningServices(Int.MAX_VALUE)
        if (runningServices != null) {
            val it = runningServices.iterator()
            while (true) {
                if (!it.hasNext()) {
                    break
                }
                val runningServiceInfo =
                    it.next() as ActivityManager.RunningServiceInfo
                if (NotificationCatchService::class.java.name == runningServiceInfo.service.className) {
                    val sb = StringBuilder()
                    sb.append("foreground")
                    sb.append(runningServiceInfo.foreground)
                    sb.append("")
                    LogUtil.mLOGI("SERVICECHECK", sb.toString())
                    if (runningServiceInfo.crashCount > 0) {
                        return true
                    }
                }
            }
        }
        return false
    }
    /**
     * Check is night mode is on
     */
    fun isNightMode(context: Context): Boolean {
        return context.resources.configuration.uiMode and 48 == 32
    }
    /**
     * Check notfication catch service is enabled
     */
    fun isNotiCathEnabled(context: Context): Boolean {
        return try {
            val sb = StringBuilder()
            sb.append("isNotiCathEnabled")
            sb.append(NotificationManagerCompat.getEnabledListenerPackages(context).contains(context.packageName))
            NotificationManagerCompat.getEnabledListenerPackages(context).contains(context.packageName)
        } catch (unused: Exception) {
            true
        }
    }

    fun isOver1Mb(bundle: Bundle?): Boolean {
        val obtain = Parcel.obtain()
        obtain.writeBundle(bundle)
        val dataSize = obtain.dataSize()
        obtain.recycle()
        val sb = StringBuilder()
        sb.append("size")
        sb.append(dataSize)
        LogUtil.mLOGI("isOver1Mb", sb.toString())
        return dataSize >= 1048576
    }
    /**
     * Check service is running in background
     */
    fun isServiceRunningInForeground(
        context: Context,
        cls: Class<*>
    ): Boolean {
        for (runningServiceInfo in (context.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager).getRunningServices(Int.MAX_VALUE)) {
            if (cls.name == runningServiceInfo.service.className && runningServiceInfo.foreground) {
                return true
            }
        }
        return false
    }
    /**
     * Generate md5 key
     */
    @ExperimentalUnsignedTypes
    fun md5(str: String): String {
        return try {
            val instance = MessageDigest.getInstance("MD5")
            instance.update(str.toByteArray())
            val digest = instance.digest()
            val sb = StringBuilder()
            for (b in digest) {
                var hexString = Integer.toHexString((b and UByte.MAX_VALUE.toByte()).toInt())
                while (hexString.length < 2) {
                    val sb2 = StringBuilder()
                    sb2.append("0")
                    sb2.append(hexString)
                    hexString = sb2.toString()
                }
                sb.append(hexString)
            }
            sb.toString()
        } catch (e: NoSuchAlgorithmException) {
            e.printStackTrace().toString()
        }
    }
    /**
     * Replace special characters
     */
    fun replaceInjectionKeyword(str: String): String {
        if (TextUtils.isEmpty(str)) {
            return str
        }
        val str2 = ""
        return str.replace("`", str2).replace("\"", str2).replace("‎", str2).replace("​", str2)
            .replace("⁩", str2).replace("⁨", str2).replace(" ", str2).trim { it <= ' ' }
    }
    /**
     * Start notification service
     */
    fun startNotiCatchService(context: Context) {
        try {
            val componentName = ComponentName(context, NotificationCatchService::class.java)
            val packageManager = context.packageManager
            packageManager.setComponentEnabledSetting(
                componentName,
                PackageManager.COMPONENT_ENABLED_STATE_DISABLED,
                PackageManager.DONT_KILL_APP
            )
            packageManager.setComponentEnabledSetting(
                componentName,
                PackageManager.COMPONENT_ENABLED_STATE_ENABLED,
                PackageManager.DONT_KILL_APP
            )
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }
}