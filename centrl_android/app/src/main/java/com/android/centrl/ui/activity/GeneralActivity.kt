package com.android.centrl.ui.activity

import android.content.Intent
import android.os.Bundle
import android.view.KeyEvent
import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.android.centrl.R
import com.android.centrl.databinding.ActivityGeneralBinding
import com.android.centrl.viewmodel.SwitchEDViewModel
import com.android.centrl.utils.AdsLoading
import com.android.centrl.utils.AppConstant
import com.google.android.gms.ads.AdView

/**
 * General settings activity
 */
class GeneralActivity : BaseActivity() {

    private var mAdView: AdView? = null
    private lateinit var binding: ActivityGeneralBinding
    private lateinit var switchEDViewModel: SwitchEDViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        switchEDViewModel = ViewModelProvider(this).get(SwitchEDViewModel::class.java)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_general)
        binding.handler = ClickHandlers()
        binding.switchViewModel = switchEDViewModel
        binding.lifecycleOwner = this

        binding.adsContainerGeneral.adViewContainer.post {
            // Loading ads
            mAdView = AdsLoading.loadBanner(this@GeneralActivity,  binding.adsContainerGeneral.adViewContainer)
        }
    }

    override fun onResume() {
        super.onResume()
        binding.switchViewModel!!.setNotiVibration(preferences!!.getBoolean(AppConstant.VIBRATE, false))
        binding.switchViewModel!!.setNotiLightUp(preferences!!.getBoolean(AppConstant.LIGHT_UP, false))
        binding.switchViewModel!!.setNotiSound(preferences!!.getBoolean(AppConstant.NOTI_SOUND, false))
        binding.switchViewModel!!.setNotiVisibility(preferences!!.getBoolean(AppConstant.SHOW_MSG, false))
        if (mAdView != null) {
            AdsLoading.resumeAds(this, mAdView, binding.adsContainerGeneral.adViewContainer)
        }
    }

    override fun onPause() {
        if (mAdView != null) {
            mAdView!!.pause()
        }
        super.onPause()
    }

    override fun onDestroy() {
        if (mAdView != null) {
            mAdView!!.destroy()
        }
        super.onDestroy()
    }
    /**
     * Click switch handler class
     */
    inner class ClickHandlers {
        /**
         * View click handler
         */
        fun onClickListener(view: View) {
            when (view.id) {
                R.id.general_back -> { finish() }
                R.id.keyboard_setting_rl -> { startActivity(Intent(this@GeneralActivity, KeyBoardSettingsActivity::class.java)) }
                R.id.ignored_rl -> { startActivity(Intent(this@GeneralActivity, BlockedContactListActivity::class.java)) }
            }
        }
        /**
         * View switch handler
         */
        fun saveGeneral(view: View, isChecked: Boolean) {
            when (view.id) {
                R.id.vibrate_switch -> {
                    editor!!.putBoolean(AppConstant.VIBRATE, isChecked)
                    editor!!.commit()
                }
                R.id.lightup_switch -> {
                    editor!!.putBoolean(AppConstant.LIGHT_UP, isChecked)
                    editor!!.commit()
                }
                R.id.noti_sound_switch -> {
                    editor!!.putBoolean(AppConstant.NOTI_SOUND, isChecked)
                    editor!!.commit()
                }
                R.id.show_msg_switch -> {
                    editor!!.putBoolean(AppConstant.SHOW_MSG, isChecked)
                    editor!!.commit()
                }
            }
        }
    }
    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            finish()
            return true
        }
        return super.onKeyDown(keyCode, event)
    }
}