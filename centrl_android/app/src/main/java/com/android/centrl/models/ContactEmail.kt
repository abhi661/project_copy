package com.android.centrl.models

/**
 * Contact get mail id model class.
 */
class ContactEmail(var address: String, var type: String)