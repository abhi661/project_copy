package com.android.centrl.persistence.repositry

import android.content.Context
import com.android.centrl.persistence.dao.ChatsDao
import com.android.centrl.persistence.dao.GroupChannelsDao
import com.android.centrl.persistence.database.CentrlDatabase
import com.android.centrl.persistence.entity.ChatsEntity
import com.android.centrl.persistence.entity.GroupChannelsEntity
import io.reactivex.Completable
import io.reactivex.CompletableObserver
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import org.jetbrains.anko.doAsyncResult

/**
 * Access point for managing chat details.
 */
class ChatsRepository(context: Context) {

    private val chatsDao: ChatsDao
    private val groupChannelsDao: GroupChannelsDao

    init {
        val centrlDatabase = CentrlDatabase.getInstance(context)
        chatsDao = centrlDatabase.chatsDao()
        groupChannelsDao = centrlDatabase.groupChannelsDao()
    }

    /**
     * @param chatsEntity the chat details to be inserted.
     */
    fun insertChat(chatsEntity: ChatsEntity) {
        try {
            Completable.fromAction {
                chatsDao.insertChat(chatsEntity)
            }.observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(object : CompletableObserver {
                    override fun onSubscribe(d: Disposable) {}
                    override fun onComplete() {}
                    override fun onError(e: Throwable) {}
                })
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    /**
     * update the chat details.
     */
    fun updateChat(chatKey: String, groupId: String) {
        try {
            Completable.fromAction {
                chatsDao.updateChat(chatKey, groupId)
            }.observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(object : CompletableObserver {
                    override fun onSubscribe(d: Disposable) {}
                    override fun onComplete() {}
                    override fun onError(e: Throwable) {}
                })
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    /**
     * update the chat details.
     */
    fun updateTitleChat(groupId: String, title: String, chatKey: String, phoneNumber: String) {
        try {
            Completable.fromAction {
                chatsDao.updateTitleChat(groupId, title, chatKey, phoneNumber)
            }.observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(object : CompletableObserver {
                    override fun onSubscribe(d: Disposable) {}
                    override fun onComplete() {}
                    override fun onError(e: Throwable) {}
                })
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    /**
     * @return the group chat list from the table
     */
    fun getGroupChatsList(groupId: String): ArrayList<ChatsEntity> {
        return getGroupChatsListAsyncTask(groupId)
    }

    /**
     * @return the last chats from the table
     */
    fun getLastGroupChats(groupId: String): ChatsEntity {
        return getLastGroupChatsAsyncTask(groupId)
    }

    /**
     * @return the single chats list from the table
     */
    fun getSingleChatsList(groupId: String, packageCode: Int): ArrayList<ChatsEntity> {
        return getSingleChatsListAsyncTask(packageCode, groupId)
    }

    /**
     * @return the last single chats from the table
     */
    fun getLastSingleChats(groupId: String, packageCode: Int): ChatsEntity {
        return getLastSingleChatsAsyncTask(packageCode, groupId)
    }

    /**
     * @return the return the all chats list from the table
     */
    fun getAllChatsList(notiType: String): ArrayList<ChatsEntity> {
        val chatList: ArrayList<ChatsEntity> = ArrayList()
        val getAllChats = getAllChatsListAsyncTask(notiType)
        for (chats in getAllChats) {
            val chatsEntity = getLastChat(chats, notiType)
             if (chatsEntity.groupID.isNotEmpty()) {
                 chatList.add(chatsEntity)
             }
        }
        return chatList
    }

    /**
     * @return the last chats from the table
     */
    fun getLastChat(groupId: String, notiType: String): ChatsEntity {
        val chatsEntity = getLastChatAsyncTask(groupId, notiType)
        val getGroupDetails = getGroupDetailsAsyncTask(groupId)
        if (getGroupDetails.groupTitle.isNotEmpty()) {
            chatsEntity.groupTitle = getGroupDetails.groupTitle
            chatsEntity.groupIconPath = getGroupDetails.groupIconPath
            chatsEntity.muteChat = getGroupDetails.muteChat
            chatsEntity.groupChat = getGroupDetails.isGroup
        }
        return chatsEntity
    }

    /**
     * @return the return the all chats list from the table
     */
    fun getAllChatsLists(): ArrayList<ChatsEntity> {
        val chatList: ArrayList<ChatsEntity> = ArrayList()
        val getAllChats = getAllChatsListsAsyncTask()
        for (chats in getAllChats) {
            val chatsEntity = getLastChats(chats)
            if (chatsEntity.groupID.isNotEmpty()) {
                chatList.add(chatsEntity)
            }
        }
        return chatList
    }

    /**
     * @return the last chats from the table
     */
    private fun getLastChats(groupId: String): ChatsEntity {
        val chatsEntity = getLastChatsAsyncTask(groupId)
        val getGroupDetails = getGroupDetailsAsyncTask(groupId)
        if (getGroupDetails.groupTitle.isNotEmpty()) {
            chatsEntity.groupTitle = getGroupDetails.groupTitle
            chatsEntity.groupIconPath = getGroupDetails.groupIconPath
            chatsEntity.muteChat = getGroupDetails.muteChat
            chatsEntity.groupChat = getGroupDetails.isGroup
        }
        return chatsEntity
    }

    /**
     * @return the single chats list from the table
     */
    fun getGroupChannelsList(groupId: String): ArrayList<Int> {
        return getGroupChannelsListsAsyncTask(groupId)
    }
    /**
     * Delete chats from table.
     */
    fun deleteChat(groupId: String) {
        try {
            Completable.fromAction {
                chatsDao.deleteChat(groupId)
            }.observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(object : CompletableObserver {
                    override fun onSubscribe(d: Disposable) {}
                    override fun onComplete() {}
                    override fun onError(e: Throwable) {}
                })
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    /**
     * Delete message details from table.
     */
    fun deleteChatMessages(chatId: Int) {
        try {
            Completable.fromAction {
                chatsDao.deleteChatMessages(chatId)
            }.observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(object : CompletableObserver {
                    override fun onSubscribe(d: Disposable) {}
                    override fun onComplete() {}
                    override fun onError(e: Throwable) {}
                })
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }


    /**
     * AsyncTask for get Chat list from database
     */
    private fun getGroupChatsListAsyncTask(groupId: String): ArrayList<ChatsEntity> {
        val result = doAsyncResult(null,{
            chatsDao.getGroupChatsList(groupId) as ArrayList<ChatsEntity>
        })
        return result.get()
    }

    /**
     * AsyncTask for get Last grou chats from database
     */
    private fun getLastGroupChatsAsyncTask(groupId: String): ChatsEntity {
        val chatsEntity = doAsyncResult(null,{
            chatsDao.getLastGroupChats(groupId)
        })
        if(chatsEntity.get() == null){
            return ChatsEntity(0, "", "", "", "", 0, "", "", "", "", "", "")
        }
        return chatsEntity.get()
    }

    /**
     * AsyncTask for get single chat list from database
     */
    private fun getSingleChatsListAsyncTask(packCode: Int, groupId: String): ArrayList<ChatsEntity> {
        val result = doAsyncResult(null,{
            chatsDao.getSingleChatsList(groupId, packCode) as ArrayList<ChatsEntity>
        })
        return result.get()
    }

    /**
     * AsyncTask for get last single chat from database
     */
    private fun getLastSingleChatsAsyncTask(packCode: Int, groupId: String): ChatsEntity {
        val chatsEntity = doAsyncResult(null,{
            chatsDao.getLastSingleChats(groupId, packCode)
        })
        if(chatsEntity.get() == null){
            return ChatsEntity(0, "", "", "", "", 0, "", "", "", "", "", "")
        }
        return chatsEntity.get()
    }

    /**
     * AsyncTask for get all Chat list from database
     */
    private fun getAllChatsListAsyncTask(notiType: String): ArrayList<String> {
        val result = doAsyncResult(null,{
            groupChannelsDao.getAllGroupList(notiType) as ArrayList<String>
        })
        return result.get()
    }

    /**
     * AsyncTask for get all Chat list from database
     */
    private fun getAllChatsListsAsyncTask(): ArrayList<String> {
        val result = doAsyncResult(null,{
            groupChannelsDao.getAllContactsLists() as ArrayList<String>
        })
        return result.get()
    }

    /**
     * AsyncTask for get last Chat from database
     */
    private fun getLastChatAsyncTask(groupId: String, notiType: String): ChatsEntity {
        val chatsEntity = doAsyncResult(null,{
            chatsDao.getLastChat(groupId, notiType)
        })
        if(chatsEntity.get() == null){
            return ChatsEntity(0, "", "", "", "", 0, "", "", "", "", "", "")
        }
        return chatsEntity.get()
    }

    /**
     * AsyncTask for get last Chat from database
     */
    private fun getLastChatsAsyncTask(groupId: String): ChatsEntity {
        val chatsEntity = doAsyncResult(null,{
            chatsDao.getLastChats(groupId)
        })
        if(chatsEntity.get() == null){
            return ChatsEntity(0, "", "", "", "", 0, "", "", "", "", "", "")
        }
        return chatsEntity.get()
    }

    /**
     * AsyncTask for get group details from database
     */
    private fun getGroupDetailsAsyncTask(groupId: String): GroupChannelsEntity {
        val chatsEntity = doAsyncResult(null,{
            groupChannelsDao.getGroupDetails(groupId)
        })
        if(chatsEntity.get() == null){
            return GroupChannelsEntity(0, "", "", "", 0, "", "", "", "", 0, "", 0, 0)
        }
        return chatsEntity.get()
    }

    /**
     * AsyncTask for get all Chat list from database
     */
    private fun getGroupChannelsListsAsyncTask(groupId: String): ArrayList<Int> {
        val result = doAsyncResult(null,{
            groupChannelsDao.getGroupChannelList(groupId) as ArrayList<Int>
        })
        return result.get()
    }
}