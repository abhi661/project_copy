package com.android.centrl.ui.activity

import android.content.Intent
import android.os.Bundle
import com.android.centrl.R
import com.android.centrl.utils.AppConstant
import com.google.android.gms.ads.MobileAds
import java.util.concurrent.Executors
import java.util.concurrent.ScheduledExecutorService
import java.util.concurrent.TimeUnit

/**
 * Splash Activity.
 */
class SplashActivity : BaseActivity() {
    private val worker: ScheduledExecutorService = Executors.newSingleThreadScheduledExecutor()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        MobileAds.initialize(this) {}
        val runnable = Runnable {
            if (preferences!!.getBoolean(AppConstant.PERMISSION_SCREEN, false)) {
                if (preferences!!.getInt(AppConstant.LOCK_TYPE, 0) == 0) {
                    val intent = Intent(this@SplashActivity, InteractionsActivity::class.java)
                    startActivity(intent)
                }
                finish()
            } else {
                if (preferences!!.getBoolean(AppConstant.INTRO_SCREEN_FINISHED, false)) {
                    val intent = Intent(this@SplashActivity, PermissionActivity::class.java)
                    startActivity(intent)
                    finish()
                } else {
                    val intent = Intent(this@SplashActivity, OnBoardingActivity::class.java)
                    startActivity(intent)
                    finish()
                }
            }
        }
        worker.schedule(runnable, 2, TimeUnit.SECONDS)

    }
}