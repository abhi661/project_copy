package com.android.centrl.persistence.datasource

import com.android.centrl.persistence.entity.KeyboardAppsTrayEntity
import com.android.centrl.persistence.entity.RemovedKeyboardAppsEntity
import io.reactivex.Completable
import io.reactivex.Flowable
/**
 * Access point for managing keyboard tray apps.
 */
interface KeyBoardAppsTrayDataSource {
    /**
     * Insert a keyboard apps details in the database. If the keyboard apps details already exists, replace it.
     *
     * @param keyboardAppsTrayEntity the keyboard apps details to be inserted.
     */
    fun insertKeyboardAppTray(keyboardAppsTrayEntity: KeyboardAppsTrayEntity): Completable
    /**
     * Get the keyboard apps details from the table. Since for simplicity we only have one backup details in the database,
     *
     * @return the keyboard apps details from the table
     */
    fun getAllKeyboardAppTrayPackName(): Flowable<List<String>>

    /**
     * Get the keyboard apps details from the table. Since for simplicity we only have one backup details in the database,
     *
     * @return the keyboard apps details from the table
     */
    fun getAllKeyboardAppTrayPackCode(): Flowable<List<Int>>

    /**
     * Delete all keyboard apps details.
     */
    fun deleteKeyboardAppsTray(code: Int)

    /**
     * Insert a removed keyboard apps details in the database. If the removed keyboard apps details already exists, replace it.
     *
     * @param removedKeyboardAppsEntity the removed keyboard apps details to be inserted.
     */
    fun insertRemovedKeyboardApps(removedKeyboardAppsEntity: RemovedKeyboardAppsEntity): Completable


    /**
     * Get the removed keyboard details from the table. Since for simplicity we only have one backup details in the database,
     *
     * @return the removed keyboard apps details from the table
     */
    fun getAllRemovedChatAppPackages(): Flowable<List<Int>>

    /**
     * Delete all removed keyboard apps details.
     */

    fun deleteKeyboardApps(code: Int)
}