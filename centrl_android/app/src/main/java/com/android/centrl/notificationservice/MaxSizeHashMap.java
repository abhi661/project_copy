package com.android.centrl.notificationservice;

import java.util.LinkedHashMap;

/**
 * Notifications pending intent for reply object storage linked hash map
 */
public class MaxSizeHashMap <K, V> extends LinkedHashMap<K, V> {

    private final int MAX_SIZE;

    public MaxSizeHashMap(int i) {
        this.MAX_SIZE = i;
    }

    public boolean removeEldestEntry(Entry<K, V> entry) {
        return size() > this.MAX_SIZE;
    }
}