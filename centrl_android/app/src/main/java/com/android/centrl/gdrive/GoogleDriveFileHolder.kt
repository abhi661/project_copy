package com.android.centrl.gdrive

import com.google.api.client.util.DateTime

/**
 * A utility for performing read/write operations on Drive files via the REST API and opening a
 * file picker UI via Storage Access Framework.
 */
class GoogleDriveFileHolder {
    lateinit var id: String
    lateinit var name: String
    lateinit var modifiedTime: DateTime
    var size: Long = 0
    lateinit var createdTime: DateTime
    var starred: Boolean = false
    lateinit var mimeType: String

}