package com.android.centrl.notificationservice

import android.service.notification.StatusBarNotification

/**
 * Notification validation listener
 */
interface NotificationCatchServicePresenter {
    fun isNotiCancel(str: String): Boolean
    fun parseNotification(
        statusBarNotification: StatusBarNotification,
        packCode: Int
    ): NotificationData?

    fun shouldInsert(statusBarNotification: StatusBarNotification): Boolean
}