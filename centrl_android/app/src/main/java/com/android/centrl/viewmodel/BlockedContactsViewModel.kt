package com.android.centrl.viewmodel

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.android.centrl.persistence.datasource.BlockedContactsDataSource
import com.android.centrl.persistence.entity.BlockedContactsEntity
import com.android.centrl.persistence.repositry.BlockedContactRepo
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.plusAssign
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import timber.log.Timber

/**
 * View Model for the [@BlockedContactListActivity]
 */
class BlockedContactsViewModel(
    application: Application,
    private val mDataSource: BlockedContactsDataSource
) : BaseViewModel(application) {

    private val blockedContactRepo: BlockedContactRepo = BlockedContactRepo(application)
    private val getBlockedContactsList = MutableLiveData<ArrayList<BlockedContactsEntity>>()
    private val insertStatus = MutableLiveData<Boolean>()
    /**
     *  Set insert status to view
     */
    val mInsertStatus: LiveData<Boolean>
        get() = insertStatus
    /**
     *  Set blocked contacts list to view
     */
    val mBlockedContactsList: LiveData<ArrayList<BlockedContactsEntity>>
        get() = getBlockedContactsList
    /**
     * @return the blocked contacts details from the table
     */
    fun getBlockedContacts(groupId: String): BlockedContactsEntity{
        return blockedContactRepo.getBlockedDetails(groupId)
    }
    /**
     * @return the blocked contacts details list from the table
     */
    fun getAllBlockedContactsList(): ArrayList<BlockedContactsEntity>{
        return blockedContactRepo.getAllBlockedList()
    }
    /**
     * Get the blocked contacts details from the table. Since for simplicity we only have one blocked contacts in the database,
     */
    fun getAllBlockedList(){
        disposables += mDataSource.getAllBlockedList()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { blockedList ->
                getBlockedContactsList.value = blockedList as ArrayList<BlockedContactsEntity>?
            }
    }
    /**
     * Insert a blocked contacts details in the database. If the blocked contacts details already exists, replace it.
     *
     * @param blockedContactsEntity the blocked contacts details to be inserted.
     */
    fun insertBlockedContacts(blockedContactsEntity: BlockedContactsEntity){
        disposables += mDataSource.insertBlockedContacts(blockedContactsEntity)
            .subscribeOn(Schedulers.io())
            .subscribeBy(onComplete = {
                insertStatus.postValue(true)
            }, onError = {
                Timber.e(it)
                insertStatus.postValue(false)
            })
    }
    /**
     * Delete all blocked contacts details.
     */
    fun deleteBlockedContact(groupId: String){
        blockedContactRepo.deleteBlockedContact(groupId)
    }
}