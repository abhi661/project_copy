package com.android.centrl.ui.activity

import android.net.Uri
import android.os.Bundle
import android.view.KeyEvent
import androidx.fragment.app.Fragment
import com.android.centrl.interfaces.PatternDrawInterface
import com.android.centrl.R
import com.android.centrl.ui.fragment.PatternLockFirstFragment
import com.android.centrl.ui.fragment.PatternLockSecondFragment

/**
 * Pattern lock setup.
 */
class PatternLockActivity : BaseActivity(), PatternDrawInterface {

    private var isResetPattern: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pattern_lock)
        isResetPattern = false
        val intent = intent
        val data: Uri? = intent.data
        if(data != null && data.toString() == "https://www.centrl.com/forgot/pattern"){
            isResetPattern = true
        }
        val fragmentFirstPattern: Fragment = PatternLockFirstFragment()
        val fragmentTransaction = supportFragmentManager.beginTransaction()
        fragmentTransaction.add(R.id.pattern_container, fragmentFirstPattern)
        fragmentTransaction.commit()
    }
    /**
     * Redirect to re-confirm pattern screen.
     */
    override fun firstDrawPattern(pattern: String) {
        PatternLockSecondFragment.newInstance(pattern, isResetPattern)
        val fragmentSecondPattern: Fragment = PatternLockSecondFragment()
        val ft = supportFragmentManager.beginTransaction()
        ft.replace(R.id.pattern_container, fragmentSecondPattern)
        ft.addToBackStack(null)
        ft.commit()
    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            finish()
            return true
        }
        return super.onKeyDown(keyCode, event)
    }

    companion object {
        const val TAG = "PinLockView"
    }
}