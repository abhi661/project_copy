package com.android.centrl.ui.activity

import android.annotation.SuppressLint
import android.app.Activity
import android.app.NotificationManager
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.provider.ContactsContract
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.transition.Explode
import android.util.Patterns
import android.view.KeyEvent
import android.view.MenuItem
import android.view.View
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.widget.PopupMenu
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.viewpager.widget.ViewPager
import androidx.viewpager.widget.ViewPager.OnPageChangeListener
import com.android.centrl.R
import com.android.centrl.adapter.ViewPagerAdapter
import com.android.centrl.databinding.ActivityInteractionsBinding
import com.android.centrl.interfaces.ChatContactSelectedListener
import com.android.centrl.interfaces.MultiSelectChatList
import com.android.centrl.notificationservice.NotificationData
import com.android.centrl.notificationservice.PendingIntentManager
import com.android.centrl.notificationservice.PrefManager
import com.android.centrl.persistence.entity.ChatsEntity
import com.android.centrl.ui.dialog.DialogSignInEmail
import com.android.centrl.ui.fragment.ChatFragment
import com.android.centrl.ui.fragment.ContactFragment
import com.android.centrl.ui.fragment.MailFragment
import com.android.centrl.utils.*
import com.android.centrl.utils.DrawableClickListener.RightDrawableClickListener
import com.android.centrl.viewmodel.SearchViewModel
import com.google.android.gms.ads.AdListener
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.InterstitialAd
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.drive.Drive
import java.util.*
import java.util.concurrent.Executors
import java.util.concurrent.ScheduledExecutorService
import java.util.concurrent.TimeUnit

/**
 * Home screen activity.
 */
class InteractionsActivity : BaseActivity(), PopupMenu.OnMenuItemClickListener, MultiSelectChatList, ChatContactSelectedListener, OnPageChangeListener,
    DialogSignInEmail.DialogSignInEmailListener {

    private val worker: ScheduledExecutorService = Executors.newSingleThreadScheduledExecutor()
    private var account: GoogleSignInAccount? = null
    private lateinit var binding: ActivityInteractionsBinding
    private var animation: Animation? = null
    private var searchViewModel: SearchViewModel? = null
    private var chatFragment: ChatFragment? = null
    private var mailFragment: MailFragment? = null
    private var contactFragment: ContactFragment? = null
    private lateinit var mInterstitialAd: InterstitialAd

    private var mChatGroupIdList: ArrayList<ChatsEntity>? = null
    private var mIsMuteList = ArrayList<String>()
    private var mNumbersList = ArrayList<String>()
    private var isShowedMultiSelect = false
    private var mPhoneNumber = ""
    private var mPackCode = 0
    private var mPageNumber = 0
    private var isOnlyMultiContact = false
    private var isContactPage = false
    private var isSearchOpen = false
    private var isBlockedContactSelected = false
    private var isSameChannelSelected = false
    private var isGroupChatSelected = false
    private var mSearchedTxt = ""

    @SuppressLint("ClickableViewAccessibility")
    @RequiresApi(api = Build.VERSION_CODES.O)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.enterTransition = Explode()
        binding = DataBindingUtil.setContentView(this, R.layout.activity_interactions)
        binding.handler = ClickHandler()
        account = GoogleSignIn.getLastSignedInAccount(applicationContext)

        searchViewModel = ViewModelProvider(this).get(SearchViewModel::class.java)
        searchViewModel!!.init()
        PrefManager.getInstance(this)!!.saveBoolean(AppConstant.IS_CHAT_SCREEN, false)
        PrefManager.getInstance(this)!!.saveBoolean(PrefManager.IS_INIT, true)
        PrefManager.getInstance(this)!!.saveBoolean(PrefManager.MAIN_SCREEN_PRESENT, true)
        PendingIntentManager.getInstance(this).refreshKeyBoardApps()
        animation = AnimationUtils.loadAnimation(this, R.anim.search_right_to_left)

        binding.viewPager.addOnPageChangeListener(this)
        binding.interactionTitleBar.visibility = View.VISIBLE
        binding.searchLl.visibility = View.GONE
        binding.multiselectTitleBar.visibility = View.GONE
        binding.multiselectTitleBarCheckbox.visibility = View.GONE
        binding.viewPager.offscreenPageLimit = 3
        setupViewPager(binding.viewPager)
        binding.tabs.setupWithViewPager(binding.viewPager)
        binding.tabs.badgeTruncateAt = TextUtils.TruncateAt.START

        binding.searchEditTxt.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {
                // Before text changed
            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                mSearchedTxt = s.toString().trim { it <= ' ' }
                when (binding.viewPager.currentItem) {
                    0 -> {
                        searchViewModel!!.sendChatData("" + s.toString().trim { it <= ' ' })
                    }
                    1 -> {
                        searchViewModel!!.sendMailData("" + s.toString().trim { it <= ' ' })
                    }
                    2 -> {
                        searchViewModel!!.sendContactData("" + s.toString().trim { it <= ' ' })
                    }
                }
            }

            override fun afterTextChanged(s: Editable) {}
        })
        binding.searchEditTxt.setOnTouchListener(object :
            RightDrawableClickListener(binding.searchEditTxt) {
            override fun onDrawableClick(): Boolean {
                binding.searchEditTxt.setText("")
                when (binding.viewPager.currentItem) {
                    0 -> {
                        searchViewModel!!.sendChatData("")
                    }
                    1 -> {
                        searchViewModel!!.sendMailData("")
                    }
                    2 -> {
                        searchViewModel!!.sendContactData("")
                    }
                }
                binding.searchEditTxt.hint = resources.getString(R.string.search)
                searchViewModel!!.refreshPage(mPageNumber)
                return false
            }
        })

        binding.searchEditTxt.setOnEditorActionListener { _: TextView?, actionId: Int, _: KeyEvent? ->
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                searchViewModel!!.sendChatData("" + mSearchedTxt)
                val imm = (getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager)
                imm.hideSoftInputFromWindow(binding.searchBack.windowToken, 0)
                return@setOnEditorActionListener true
            }
            false
        }

        mInterstitialAd = InterstitialAd(this)
        mInterstitialAd.adUnitId = resources.getString(R.string.admob_interstitial_unit_id)
        mInterstitialAd.loadAd(AdRequest.Builder().build())

        mInterstitialAd.adListener = object : AdListener() {
            override fun onAdClosed() {
                mInterstitialAd.loadAd(AdRequest.Builder().build())
            }
        }
        val showedDate = PrefManager.getInstance(this)!!.loadStringValue(
            AppConstant.AD_SHOWED,
            null
        )
        if(showedDate != null){
            if(showedDate == DateUtil.getCurrentDate(this@InteractionsActivity)){
                PrefManager.getInstance(this)!!.saveIntegerValue(AppConstant.LAUNCH_COUNT, 0)
            } else {
                countAppLaunch()
            }
        } else {
            countAppLaunch()
        }

        val runnable = Runnable {
            runOnUiThread {
                if(PrefManager.getInstance(this)!!.loadIntegerValue(AppConstant.LAUNCH_COUNT, 0) == 3 && mInterstitialAd.isLoaded){
                    mInterstitialAd.show()
                    PrefManager.getInstance(this)!!.saveIntegerValue(AppConstant.LAUNCH_COUNT, 0)
                    PrefManager.getInstance(this)!!.saveStringValue(
                        AppConstant.AD_SHOWED, DateUtil.getCurrentDate(
                            this@InteractionsActivity
                        )
                    )
                }
            }
        }
        worker.schedule(runnable, 3, TimeUnit.SECONDS)
    }

    private fun countAppLaunch(){
        try{
            var totalCount = PrefManager.getInstance(this)!!.loadIntegerValue(
                AppConstant.LAUNCH_COUNT,
                0
            )
            totalCount++
            PrefManager.getInstance(this)!!.saveIntegerValue(AppConstant.LAUNCH_COUNT, totalCount)
        }catch (e: Exception){
            e.printStackTrace()
        }
    }

    override fun onResume() {
        super.onResume()
        try{
            if(account == null){
                if(!PrefManager.getInstance(this)!!.isEnabled(AppConstant.GOOGLE_SIGN_SKIP, false)){
                    googleSignIn()
                }
            } else {
                LogUtil.mLOGE("Interactions", "Unique ID..." + CentrlUtil.getUUID())
                LogUtil.mLOGE("Interactions", "Mail ID..." + account!!.email)
            }
        }catch (e: Exception){
            e.printStackTrace()
        }
    }

    /**
     * Google signIn  dialog.
     */
    private fun googleSignIn() {
        val dialogAddNewChannels = DialogSignInEmail()
        val bundle = Bundle()
        dialogAddNewChannels.arguments = bundle
        dialogAddNewChannels.show(supportFragmentManager, DialogSignInEmail.TAG)
    }
    /**
     * View onClick handler class.
     */
    inner class ClickHandler{
        /**
         * View onClick handler.
         */

        fun onClick(view: View){
            when (view.id) {
                R.id.home_menu -> if (isContactPage) {
                    val popupMenu = PopupMenu(this@InteractionsActivity, binding.homeMenu)
                    popupMenu.menuInflater.inflate(R.menu.contact_home_menu, popupMenu.menu)
                    popupMenu.setOnMenuItemClickListener(this@InteractionsActivity)
                    popupMenu.show()
                } else {
                    val popupMenu = PopupMenu(this@InteractionsActivity, binding.homeMenu)
                    popupMenu.menuInflater.inflate(R.menu.home_menu, popupMenu.menu)
                    popupMenu.setOnMenuItemClickListener(this@InteractionsActivity)
                    popupMenu.show()
                }
                R.id.home_search -> try {
                    isSearchOpen = true
                    binding.searchLl.startAnimation(animation)
                    binding.searchLl.visibility = View.VISIBLE
                    binding.interactionTitleBar.visibility = View.GONE
                    binding.searchEditTxt.requestFocus()
                    binding.searchEditTxt.isCursorVisible = true
                    (Objects.requireNonNull(getSystemService(Context.INPUT_METHOD_SERVICE)) as InputMethodManager).showSoftInput(
                        binding.searchEditTxt,
                        InputMethodManager.SHOW_FORCED
                    )
                } catch (e: Exception) {
                    e.printStackTrace()
                }
                R.id.search_back -> try {
                    isSearchOpen = false
                    binding.interactionTitleBar.startAnimation(animation)
                    binding.interactionTitleBar.visibility = View.VISIBLE
                    binding.searchLl.visibility = View.GONE
                    binding.searchEditTxt.setText("")
                    when (mPageNumber) {
                        0 -> {
                            searchViewModel!!.sendChatData("")
                        }
                        1 -> {
                            searchViewModel!!.sendMailData("")
                        }
                        2 -> {
                            searchViewModel!!.sendContactData("")
                        }
                    }
                    binding.searchEditTxt.hint = resources.getString(R.string.search)
                    searchViewModel!!.refreshPage(mPageNumber)
                    (Objects.requireNonNull(getSystemService(Context.INPUT_METHOD_SERVICE)) as InputMethodManager).hideSoftInputFromWindow(
                        binding.searchBack.windowToken,
                        0
                    )
                } catch (e: Exception) {
                    e.printStackTrace()
                }
                R.id.select_close -> refresh()
                R.id.select_delete -> try {
                    if (mChatGroupIdList!!.size != 0) {
                        var i = 0
                        while (i < mChatGroupIdList!!.size) {
                            val groupKey = mChatGroupIdList!![i].groupID
                            val chat = mChatGroupIdList!![i]
                            mChatsViewModel!!.deleteChat(groupKey)
                            mGroupChannelsViewModel!!.deleteGroup(groupKey)
                            mMutiChannelViewModel!!.deleteCount(chat.groupID)
                            mMutiChannelViewModel!!.deleteGroupAllPackageCode(chat.groupID)
                            i++
                        }
                        Thread.sleep(AppConstant.SLEEP)
                        refresh()
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
                R.id.select_merge -> try { // Merge selected contacts
                    if (mChatGroupIdList!!.size >= 2) {
                        if (isSameChannelSelected) {
                            CentrlSnackBar.showSnackBar(
                                binding.interactionSnackar.snackbarCl, resources.getString(
                                    R.string.same_channel
                                )
                            )
                            return
                        }
                        if (isBlockedContactSelected) {
                            CentrlSnackBar.showSnackBar(
                                binding.interactionSnackar.snackbarCl, resources.getString(
                                    R.string.blocked_contact_selected
                                )
                            )
                            return
                        }
                        if (isGroupChatSelected) {
                            CentrlSnackBar.showSnackBar(
                                binding.interactionSnackar.snackbarCl, resources.getString(
                                    R.string.group_contact_selected
                                )
                            )
                            return
                        }
                        if (isOnlyMultiContact) {
                            showAddContact()
                        } else {
                            // create instance of Random class
                            val rand = Random()
                            // Generate random integers in range 0 to 999
                            var groupKey = rand.nextInt(10000).toString()
                            var title = ""
                            run {
                                var i = 0
                                while (i < mChatGroupIdList!!.size) {
                                    val chat = mGroupChannelsViewModel!!.getGroupDetails(
                                        mChatGroupIdList!![i].groupID
                                    )
                                    val getGroupList = mGroupChannelsViewModel!!.getGroupList(
                                        mChatGroupIdList!![i].groupID
                                    )
                                    if (getGroupList.size > 1) {
                                        groupKey = chat.groupID
                                        title = chat.groupTitle
                                    }
                                    i++
                                }
                            }
                            if (title.isEmpty()) {
                                var i = 0
                                while (i < mChatGroupIdList!!.size) {
                                    val chat = mGroupChannelsViewModel!!.getGroupDetails(
                                        mChatGroupIdList!![i].groupID
                                    )
                                    if (!isValidMobile(chat.title)) {
                                        title = chat.groupTitle
                                    }
                                    i++
                                }
                            }
                            var i = 0
                            while (i < mChatGroupIdList!!.size) {
                                val chat = mGroupChannelsViewModel!!.getGroupDetails(
                                    mChatGroupIdList!![i].groupID
                                )
                                val getGroupList = mGroupChannelsViewModel!!.getGroupList(
                                    mChatGroupIdList!![i].groupID
                                )
                                if (getGroupList.size > 1) {
                                    for (chats in getGroupList) {
                                        mGroupChannelsViewModel!!.mergeGroup(
                                            groupKey,
                                            title,
                                            chats.chatKey
                                        )
                                        mChatsViewModel!!.updateChat(chats.chatKey, groupKey)
                                        mMutiChannelViewModel!!.deleteCount(chats.groupID)
                                        mMutiChannelViewModel!!.deleteGroupAllPackageCode(chats.groupID)
                                    }
                                } else {
                                    mGroupChannelsViewModel!!.mergeGroup(
                                        groupKey,
                                        title,
                                        chat.chatKey
                                    )
                                    mChatsViewModel!!.updateChat(chat.chatKey, groupKey)
                                    mMutiChannelViewModel!!.deleteCount(chat.groupID)
                                }
                                i++
                            }
                            Thread.sleep(AppConstant.SLEEP)
                            refresh()
                        }
                    } else {
                        CentrlSnackBar.showSnackBar(
                            binding.interactionSnackar.snackbarCl, resources.getString(
                                R.string.select_contacts
                            )
                        )
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
                R.id.merge_checkbox_btn -> try { // Merge selected contacts using check box
                    if (mChatGroupIdList!!.size >= 2) {
                        if (isSameChannelSelected) {
                            CentrlSnackBar.showSnackBar(
                                binding.interactionSnackar.snackbarCl, resources.getString(
                                    R.string.same_channel
                                )
                            )
                            return
                        }
                        if (isBlockedContactSelected) {
                            CentrlSnackBar.showSnackBar(
                                binding.interactionSnackar.snackbarCl, resources.getString(
                                    R.string.blocked_contact_selected
                                )
                            )
                            return
                        }
                        if (isGroupChatSelected) {
                            CentrlSnackBar.showSnackBar(
                                binding.interactionSnackar.snackbarCl, resources.getString(
                                    R.string.group_contact_selected
                                )
                            )
                            return
                        }
                        if (isOnlyMultiContact) {
                            showAddContact()
                        } else {
                            // create instance of Random class
                            val rand = Random()
                            // Generate random integers in range 0 to 999
                            var groupKey = rand.nextInt(10000).toString()
                            var title = ""
                            run {
                                var i = 0
                                while (i < mChatGroupIdList!!.size) {
                                    val chat = mGroupChannelsViewModel!!.getGroupDetails(
                                        mChatGroupIdList!![i].groupID
                                    )
                                    val getGroupList = mGroupChannelsViewModel!!.getGroupList(
                                        mChatGroupIdList!![i].groupID
                                    )
                                    if (getGroupList.size > 1) {
                                        groupKey = chat.groupID
                                        title = chat.groupTitle
                                    }
                                    i++
                                }
                            }
                            if (title.isEmpty()) {
                                var i = 0
                                while (i < mChatGroupIdList!!.size) {
                                    val chat = mGroupChannelsViewModel!!.getGroupDetails(
                                        mChatGroupIdList!![i].groupID
                                    )
                                    if (!isValidMobile(chat.title)) {
                                        title = chat.groupTitle
                                    }
                                    i++
                                }
                            }
                            var i = 0
                            while (i < mChatGroupIdList!!.size) {
                                val chat = mGroupChannelsViewModel!!.getGroupDetails(
                                    mChatGroupIdList!![i].groupID
                                )
                                val getGroupList = mGroupChannelsViewModel!!.getGroupList(
                                    mChatGroupIdList!![i].groupID
                                )
                                if (getGroupList.size > 1) {
                                    for (chats in getGroupList) {
                                        mGroupChannelsViewModel!!.mergeGroup(
                                            groupKey,
                                            title,
                                            chats.chatKey
                                        )
                                        mChatsViewModel!!.updateChat(chats.chatKey, groupKey)
                                        mMutiChannelViewModel!!.deleteCount(chats.groupID)
                                        mMutiChannelViewModel!!.deleteGroupAllPackageCode(chats.groupID)
                                    }
                                } else {
                                    mGroupChannelsViewModel!!.mergeGroup(
                                        groupKey,
                                        title,
                                        chat.chatKey
                                    )
                                    mChatsViewModel!!.updateChat(chat.chatKey, groupKey)
                                    mMutiChannelViewModel!!.deleteCount(chat.groupID)
                                }
                                i++
                            }
                            Thread.sleep(AppConstant.SLEEP)
                            refreshCheckboxSelect()
                        }
                    } else {
                        CentrlSnackBar.showSnackBar(
                            binding.interactionSnackar.snackbarCl, resources.getString(
                                R.string.select_contacts
                            )
                        )
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
                R.id.select_mute -> try {
                    if (mChatGroupIdList!!.size != 0) {
                        var i = 0
                        while (i < mChatGroupIdList!!.size) {
                            mGroupChannelsViewModel!!.updateMuteChat(
                                1,
                                mChatGroupIdList!![i].groupID
                            )
                            i++
                        }
                        refresh()
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
                R.id.unmute_group_ll -> try {
                    if (mChatGroupIdList!!.size != 0) {
                        var i = 0
                        while (i < mChatGroupIdList!!.size) {
                            mGroupChannelsViewModel!!.updateMuteChat(
                                0,
                                mChatGroupIdList!![i].groupID
                            )
                            i++
                        }
                        refresh()
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
                R.id.select_close_checkbox -> refreshCheckboxSelect()
            }
        }
    }


    override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
    }
    /**
     * Page selected from tabs.
     */
    override fun onPageSelected(position: Int) {
        binding.viewPager.setCurrentItem(position, false)
        mPageNumber = position
        isContactPage = position == 2
        if (isSearchOpen) {
            isSearchOpen = false
            binding.searchEditTxt.setText("")
            binding.searchEditTxt.hint = resources.getString(R.string.search)
            binding.interactionTitleBar.startAnimation(animation)
            binding.interactionTitleBar.visibility = View.VISIBLE
            binding.searchLl.visibility = View.GONE
            searchViewModel!!.sendChatData("")
            (Objects.requireNonNull(getSystemService(Context.INPUT_METHOD_SERVICE)) as InputMethodManager).hideSoftInputFromWindow(
                binding.searchBack.windowToken,
                0
            )
        }
    }

    override fun onPageScrollStateChanged(state: Int) {}

    /**
     * Set up view pager.
     */
    private fun setupViewPager(viewPager: ViewPager?) {
        val adapter = ViewPagerAdapter(supportFragmentManager)
        chatFragment = ChatFragment()
        mailFragment = MailFragment()
        contactFragment = ContactFragment()
        adapter.addFragment(chatFragment!!, "Chats")
        adapter.addFragment(mailFragment!!, "Mails")
        adapter.addFragment(contactFragment!!, "Contacts")
        viewPager!!.adapter = adapter
    }
    /**
     * This is use to add contact to device.
     */
    private fun showAddContact() {
        val builder = AlertDialog.Builder(this)
        builder.setTitle(resources.getString(R.string.add_to_contact))
        builder.setMessage(resources.getString(R.string.select_number))
        builder.setCancelable(true)
        builder.setNegativeButton(resources.getString(R.string.cancel)) { dialog: DialogInterface, _: Int ->
            dialog.dismiss()
            refresh()
            refreshCheckboxSelect()
        }
        builder.setPositiveButton(resources.getString(R.string.add_btn)) { _: DialogInterface?, _: Int ->
            val intentInsertEdit = Intent(Intent.ACTION_INSERT_OR_EDIT)
            intentInsertEdit.type = ContactsContract.Contacts.CONTENT_ITEM_TYPE
            intentInsertEdit.putExtra(ContactsContract.Intents.Insert.PHONE, mPhoneNumber)
            startActivityForResult(intentInsertEdit, 1)
        }
        val dialog = builder.create()
        dialog.show()
        dialog.getButton(AlertDialog.BUTTON_POSITIVE).isAllCaps = false
        dialog.getButton(AlertDialog.BUTTON_NEGATIVE).isAllCaps = false
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            dialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(
                resources.getColor(
                    R.color.continue_color,
                    null
                )
            )
            dialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(
                resources.getColor(
                    R.color.continue_color,
                    null
                )
            )
        } else {
            dialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(Color.parseColor("#4FC3B8"))
            dialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(Color.parseColor("#4FC3B8"))
        }
    }
    /**
     * This is use to refresh the UI
     */
    private fun refresh() {
        try {
            searchViewModel!!.refreshPage(mPageNumber)
            isShowedMultiSelect = false
            binding.interactionTitleBar.startAnimation(animation)
            binding.interactionTitleBar.visibility = View.VISIBLE
            binding.multiselectTitleBar.visibility = View.GONE
            isOnlyMultiContact = false
            if (mChatGroupIdList != null) {
                mChatGroupIdList!!.clear()
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }
    /**
     * This is use to refresh the UI
     */
    private fun refreshCheckboxSelect() {
        try {
            searchViewModel!!.refreshPage(mPageNumber)
            searchViewModel!!.setMerge(0)
            binding.selectCountTxtCheckbox.text = "0"
            isShowedMultiSelect = false
            binding.interactionTitleBar.startAnimation(animation)
            binding.interactionTitleBar.visibility = View.VISIBLE
            binding.multiselectTitleBarCheckbox.visibility = View.GONE
            isOnlyMultiContact = false
            if (mChatGroupIdList != null) {
                mChatGroupIdList!!.clear()
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }
    /**
     * Menu options onClick handler
     */
    override fun onMenuItemClick(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_mark_all -> {
                try {
                    if (mPageNumber == 0) {
                        for (allChat in mChatsViewModel!!.getAllChatsList(AppConstant.CHAT_SCREEN)) {
                            for (chat in mMutiChannelViewModel!!.getAllCountID()) {
                                if (allChat.groupID.equals(chat.groupID, ignoreCase = true)) {
                                    mMutiChannelViewModel!!.deleteCount(chat.groupID)
                                }
                            }
                            for (chat in mMutiChannelViewModel!!.getAllGroupMultiChat()) {
                                if (allChat.groupID.equals(chat.groupID, ignoreCase = true)) {
                                    mMutiChannelViewModel!!.deleteGroupAllPackageCode(chat.groupID)
                                }
                            }
                        }
                    } else if (mPageNumber == 1) {
                        for (allChat in mChatsViewModel!!.getAllChatsList(AppConstant.MAIL_SCREEN)) {
                            for (chat in mMutiChannelViewModel!!.getAllCountID()) {
                                if (allChat.groupID.equals(chat.groupID, ignoreCase = true)) {
                                    mMutiChannelViewModel!!.deleteCount(chat.groupID)
                                }
                            }
                            for (chat in mMutiChannelViewModel!!.getAllGroupMultiChat()) {
                                if (allChat.groupID.equals(chat.groupID, ignoreCase = true)) {
                                    mMutiChannelViewModel!!.deleteGroupAllPackageCode(chat.groupID)
                                }
                            }
                        }
                    }
                    Thread.sleep(AppConstant.SLEEP)
                    searchViewModel!!.refreshPage(mPageNumber)
                    val manager =
                        getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
                    manager.cancel(0)
                } catch (e: Exception) {
                    e.printStackTrace()
                }
                true
            }
            R.id.action_home_merge -> {
                searchViewModel!!.setMerge(1)
                binding.multiselectTitleBarCheckbox.startAnimation(animation)
                binding.multiselectTitleBarCheckbox.visibility = View.VISIBLE
                binding.interactionTitleBar.visibility = View.GONE
                true
            }
            R.id.action_home_settings, R.id.action_home_contact_settings -> {
                startActivity(Intent(this@InteractionsActivity, SettingsActivity::class.java))
                true
            }
            R.id.action_home_contact_refresh -> {
                searchViewModel!!.refreshPage(2)
                true
            }
            else -> false
        }
    }
    /**
     * This is use to multi select for merge, delete, mute, unmute
     */
    @SuppressLint("DefaultLocale", "SetTextI18n")
    override fun onMultiSelect(
        multiSelect: Boolean,
        checkboxMultiSelect: Boolean,
        chatArrayList: ArrayList<ChatsEntity>,
        blockedContactSelected: Boolean,
        sameChannelSelected: Boolean,
        groupChatsSelected: Boolean
    ) {
        try {
            isBlockedContactSelected = blockedContactSelected
            isSameChannelSelected = sameChannelSelected
            isGroupChatSelected = groupChatsSelected
            if (checkboxMultiSelect) {
                binding.selectCountTxtCheckbox.text = String.format("%d", chatArrayList.size)
            }
            binding.selectCountTxt.text = String.format("%d", chatArrayList.size)
            mChatGroupIdList = chatArrayList
            mIsMuteList.clear()
            mNumbersList.clear()
            val isMute: Boolean
            if (chatArrayList.size >= 1) {
                if (!isShowedMultiSelect) {
                    isShowedMultiSelect = true
                    if (!checkboxMultiSelect) {
                         binding.multiselectTitleBar.startAnimation(animation)
                         binding.multiselectTitleBar.visibility = View.VISIBLE
                         binding.interactionTitleBar.visibility = View.GONE
                    }
                }
                for (chat in chatArrayList) {
                    mPhoneNumber = chat.title
                    mPackCode = chat.packageCode
                    val mute = chat.muteChat.toString()
                    mIsMuteList.add(mute)
                    if (isValidMobile(chat.title)) {
                        mNumbersList.add(chat.title)
                    }
                }
                isOnlyMultiContact = if (mNumbersList.size == mChatGroupIdList!!.size) {
                    mNumbersList.size > 1
                } else {
                    false
                }
                val unMute = "0"
                isMute = !mIsMuteList.contains(unMute)
                if (isMute) {
                    binding.unmuteGroupLl.visibility = View.VISIBLE
                    binding.selectMute.visibility = View.GONE
                } else {
                    binding.unmuteGroupLl.visibility = View.GONE
                    binding.selectMute.visibility = View.VISIBLE
                }
            } else {
                chatArrayList.size
                isShowedMultiSelect = false
                isOnlyMultiContact = false
                if (!checkboxMultiSelect) {
                     binding.interactionTitleBar.startAnimation(animation)
                     binding.interactionTitleBar.visibility = View.VISIBLE
                     binding.multiselectTitleBar.visibility = View.GONE
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }
    /**
     * Validate the phone number
     */
    private fun isValidMobile(phone: String): Boolean {
        return Patterns.PHONE.matcher(phone).matches()
    }
    /**
     * This is use to set badge counts on tabs
     */
    override fun onReceived(position: Int, chatArrayList: ArrayList<String>) {
        if (chatArrayList.size == 0) {
            binding.tabs.setBadgeText(position, null)
        } else {
            binding.tabs.setBadgeText(position, "" + chatArrayList.size)
        }
    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            when {
                isShowedMultiSelect -> {
                    refresh()
                    refreshCheckboxSelect()
                }
                isSearchOpen -> {
                    isSearchOpen = false
                    binding.searchEditTxt.setText("")
                    binding.searchEditTxt.hint = resources.getString(R.string.search)
                    binding.interactionTitleBar.startAnimation(animation)
                    binding.interactionTitleBar.visibility = View.VISIBLE
                    binding.searchLl.visibility = View.GONE
                    searchViewModel!!.sendChatData("")
                    (Objects.requireNonNull(getSystemService(Context.INPUT_METHOD_SERVICE)) as InputMethodManager).hideSoftInputFromWindow(
                        binding.searchBack.windowToken,
                        0
                    )
                }
                else -> {
                    val intent = Intent(this@InteractionsActivity, FinishActivity::class.java)
                    intent.action = Intent.ACTION_MAIN
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                    startActivity(intent)
                    finish()
                }
            }
            return true
        }
        return super.onKeyDown(keyCode, event)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_CODE_SIGN_IN) {
            handleSignInResult(data)
        } else if (requestCode == 1) {
            if (resultCode == Activity.RESULT_OK) {
                fetchAllContact()
                Toast.makeText(
                    this,
                    resources.getString(R.string.contact_added),
                    Toast.LENGTH_SHORT
                ).show()
            }
            if (resultCode == Activity.RESULT_CANCELED) {
                if (isShowedMultiSelect) {
                    refresh()
                    refreshCheckboxSelect()
                }
                Toast.makeText(
                    this,
                    resources.getString(R.string.contact_cancelled),
                    Toast.LENGTH_SHORT
                ).show()
            }
        }
    }
    /**
     * Fetch added contact details and update to database
     */
    private fun fetchAllContact() {
        try {
            val contact = CentrlUtil.getContactDetailsByNumber(mPhoneNumber, this)
            val uniqueKey = NotificationData.md5(contact.name + "@" + mPackCode)
            var groupKey = contact.id
            for (lastChats in mChatsViewModel!!.getAllChatsList(AppConstant.CHAT_SCREEN)) {
                if (uniqueKey.equals(lastChats.chatKey, ignoreCase = true)) {
                    groupKey = lastChats.groupID
                }
            }
            mChatsViewModel!!.updateTitleChat(groupKey!!, contact.name!!, uniqueKey, mPhoneNumber)
            mGroupChannelsViewModel!!.updateTitleGroup(
                contact.name!!,
                groupKey,
                uniqueKey,
                mPhoneNumber
            )
            searchViewModel!!.refreshPage(1)
            if (isShowedMultiSelect) {
                refresh()
                refreshCheckboxSelect()
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }
    /**
     * This is use to refresh the UI when selected contact
     */
    override fun chatContactSelected() {
        try {
            if (isSearchOpen) {
                isSearchOpen = false
                binding.interactionTitleBar.visibility = View.VISIBLE
                binding.searchLl.visibility = View.GONE
                binding.searchEditTxt.setText("")
                searchViewModel!!.sendChatData("")
                binding.searchEditTxt.hint = resources.getString(R.string.search)
                val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                imm.hideSoftInputFromWindow(binding.searchBack.windowToken, 0)
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }


    /**
     * Open google sing in option
     */
    private fun signIn() {
        val mGoogleSignInClient = buildGoogleSignInClient()
        startActivityForResult(
            mGoogleSignInClient.signInIntent,
            REQUEST_CODE_SIGN_IN
        )
    }

    /**
     * Config the google sing in
     */
    private fun buildGoogleSignInClient(): GoogleSignInClient {
        val signInOptions = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestScopes(Drive.SCOPE_FILE)
            .requestEmail()
            .build()
        return GoogleSignIn.getClient(applicationContext, signInOptions)
    }

    /**
     * Helper method to trigger retrieving the server auth code if we've signed in.
     */
    private fun handleSignInResult(result: Intent?) {
        GoogleSignIn.getSignedInAccountFromIntent(result)
            .addOnSuccessListener { googleSignInAccount ->
                //googleSignInAccount.email
            }
            .addOnFailureListener {

            }
    }

    companion object {
        private const val REQUEST_CODE_SIGN_IN = 100
    }

    override fun onNext() {
        signIn()
    }
}