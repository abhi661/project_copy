package com.android.centrl.persistence.repositry

import android.content.Context
import com.android.centrl.persistence.dao.ChatsCountDao
import com.android.centrl.persistence.dao.MultiChannelsDao
import com.android.centrl.persistence.database.CentrlDatabase
import com.android.centrl.persistence.entity.ChatsCountsEntity
import com.android.centrl.persistence.entity.MultiChatChannelsEntity
import io.reactivex.Completable
import io.reactivex.CompletableObserver
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import org.jetbrains.anko.doAsyncResult

/**
 * Access point for managing multi channel and chats count.
 */
class MultiChannelCountRepository(context: Context) {

    private var multiChannelsDao: MultiChannelsDao
    private var chatsCountDao: ChatsCountDao

    init {
        val centrlDatabase = CentrlDatabase.getInstance(context)
        multiChannelsDao = centrlDatabase.multiChannelDao()
        chatsCountDao = centrlDatabase.chatsCountDao()
    }

    /**
     * Insert a multi channel details in the database. If the multi channel details already exists, replace it.
     *
     * @param multiChatChannelsEntity the multi channel details to be inserted.
     */
    fun insertMultiChatIdentity(multiChatChannelsEntity: MultiChatChannelsEntity) {
        try {
            Completable.fromAction {
                multiChannelsDao.insertMultiChatIdentity(multiChatChannelsEntity)
            }.observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(object : CompletableObserver {
                    override fun onSubscribe(d: Disposable) {}
                    override fun onComplete() {}
                    override fun onError(e: Throwable) {}
                })
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    /**
     * @return the multi channel details from the table
     */
    fun getAllGroupMultiChat(): ArrayList<MultiChatChannelsEntity> {
        return getGroupAllAsyncTask()
    }

    /**
     * @return the package code from the table
     */
    fun getPackageCode(packCode: Int, groupId: String): Int {
        return getAllPackCodeAsyncTask(packCode, groupId)
    }

    /**
     * @return the all group package code from the table
     */
    fun getGroupAllPackageCode(groupId: String): ArrayList<MultiChatChannelsEntity> {
        return getGroupDetailsAsyncTask(groupId)
    }

    /**
     * Delete group package code details.
     */
    fun deleteGroupAllPackageCode(groupId: String) {
        try {
            Completable.fromAction {
                multiChannelsDao.deleteGroupAllPackageCode(groupId)
            }.observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(object : CompletableObserver {
                    override fun onSubscribe(d: Disposable) {}
                    override fun onComplete() {}
                    override fun onError(e: Throwable) {}
                })
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    /**
     * Insert a chats count in the database. If the chats count already exists, replace it.
     *
     * @param chatsCountsEntity the chats count to be inserted.
     */
    fun insertCount(chatsCountsEntity: ChatsCountsEntity) {
        try {
            Completable.fromAction {
                chatsCountDao.insertCount(chatsCountsEntity)
            }.observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(object : CompletableObserver {
                    override fun onSubscribe(d: Disposable) {}
                    override fun onComplete() {}
                    override fun onError(e: Throwable) {}
                })
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    /**
     * Update a chats count in the database.
     */
    fun updateCount(count: Int, groupId: String) {
        try {
            Completable.fromAction {
                chatsCountDao.updateCount(count, groupId)
            }.observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(object : CompletableObserver {
                    override fun onSubscribe(d: Disposable) {}
                    override fun onComplete() {}
                    override fun onError(e: Throwable) {}
                })
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    /**
     * @return the chats count from the table
     */
    fun getAllCountID(): ArrayList<ChatsCountsEntity> {
        return getAllCountIDAsyncTask()
    }

    /**
     * @return the chats count from the table
     */
    fun getCount(groupId: String): Int {
        return getCountAsyncTask(groupId)
    }

    /**
     * @return the chats count from the table
     */
    fun getAllCount(): ArrayList<ChatsCountsEntity> {
        return getAllCountAsyncTask()
    }

    /**
     * Delete chats count.
     */
    fun deleteCount(groupId: String) {
        try {
            Completable.fromAction {
                chatsCountDao.deleteCount(groupId)
            }.observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(object : CompletableObserver {
                    override fun onSubscribe(d: Disposable) {}
                    override fun onComplete() {}
                    override fun onError(e: Throwable) {}
                })
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    /**
     * AsyncTask for get all groups from database
     */
    private fun getGroupAllAsyncTask(): ArrayList<MultiChatChannelsEntity> {
        val result= doAsyncResult(null,{
            multiChannelsDao.getAllGroupMultiChat() as ArrayList<MultiChatChannelsEntity>
        })
        return result.get()
    }

    /**
     * AsyncTask for get package code from database
     */
    private fun getAllPackCodeAsyncTask(packCode: Int, groupId: String): Int {
        val result = doAsyncResult(null,{
            multiChannelsDao.getPackageCode(packCode, groupId)
        })
        if(result.get() == null){
            return 0
        }
        return result.get()
    }

    /**
     * AsyncTask for get group details from database
     */
    private fun getGroupDetailsAsyncTask(groupId: String): ArrayList<MultiChatChannelsEntity> {
        val result = doAsyncResult(null,{
            multiChannelsDao.getGroupAllPackageCode(groupId) as ArrayList<MultiChatChannelsEntity>
        })
        return result.get()
    }

    /**
     * AsyncTask for get count id from database
     */
    private fun getAllCountIDAsyncTask(): ArrayList<ChatsCountsEntity> {
        val result = doAsyncResult(null,{
            chatsCountDao.getAllCountID() as ArrayList<ChatsCountsEntity>
        })
        return result.get()
    }

    /**
     * AsyncTask for get count from database
     */
    private fun getCountAsyncTask(groupId: String): Int {
        val result = doAsyncResult(null,{
            chatsCountDao.getCount(groupId)
        })
        if(result.get() == null){
            return 0
        }
        return result.get()
    }

    /**
     * AsyncTask for get counts list from database
     */
    private fun getAllCountAsyncTask(): ArrayList<ChatsCountsEntity> {
        val result = doAsyncResult(null,{
            chatsCountDao.getAllCount() as ArrayList<ChatsCountsEntity>
        })
        return result.get()
    }

}