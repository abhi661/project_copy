package com.android.centrl.utils

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Canvas
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.net.Uri
import java.io.*

/**
 * Image converter
 */
class ImageUtility {

    companion object {
        /**
         * decode uri file
         */
        @Throws(FileNotFoundException::class)
        fun decodeUri(c: Context, uri: Uri?, requiredSize: Int): Bitmap? {
            val o = BitmapFactory.Options()
            o.inJustDecodeBounds = true
            BitmapFactory.decodeStream(c.contentResolver.openInputStream(uri!!), null, o)
            var widthTmp = o.outWidth
            var heightTmp = o.outHeight
            var scale = 1
            while (true) {
                if (widthTmp / 2 < requiredSize || heightTmp / 2 < requiredSize) break
                widthTmp /= 2
                heightTmp /= 2
                scale *= 2
            }
            val o2 = BitmapFactory.Options()
            o2.inSampleSize = scale
            return BitmapFactory.decodeStream(
                c.contentResolver.openInputStream(uri), null, o2
            )
        }
        /**
         * Image convert to drawable to bitmap
         */
        fun drawableToBitmap(drawable: Drawable): Bitmap {
            if (drawable is BitmapDrawable) {
                return drawable.bitmap
            }
            var width = if (!drawable.bounds.isEmpty) drawable.bounds.width() else drawable.intrinsicWidth
            var height = if (!drawable.bounds.isEmpty) drawable.bounds.height() else drawable.intrinsicHeight
            if (width <= 0) {
                width = 1
            }
            if (height <= 0) {
                height = 1
            }
            val createBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888)
            val canvas = Canvas(createBitmap)
            drawable.setBounds(0, 0, canvas.width, canvas.height)
            drawable.draw(canvas)
            return createBitmap
        }
    }
}