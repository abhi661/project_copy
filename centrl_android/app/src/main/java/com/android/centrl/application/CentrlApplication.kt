package com.android.centrl.application

import android.content.Context
import androidx.multidex.MultiDexApplication

/**
 * Centrl application class
 */
class CentrlApplication : MultiDexApplication(){

    companion object {
        lateinit var appContext: Context
    }

    override fun onCreate() {
        super.onCreate()
       appContext = applicationContext
        registerActivityLifecycleCallbacks(AppLifecycleTracker())
    }

}
