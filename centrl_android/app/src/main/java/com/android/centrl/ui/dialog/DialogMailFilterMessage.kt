package com.android.centrl.ui.dialog

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.centrl.interfaces.SelectChannel
import com.android.centrl.R
import com.android.centrl.adapter.KeyBoardChannelListAdapter
import com.android.centrl.databinding.DialogFilterMessageBinding
import com.android.centrl.persistence.repositry.KeyBoardAppsRepository
import java.util.*

/**
 * Filter channels for mail dialog
 */
class DialogMailFilterMessage : DialogFragment(), SelectChannel {

    private var dialogFilterMessageListener: DialogFilterMessageListener? = null

    override fun channelSelected(packCode: Int) {
        dialogFilterMessageListener!!.onMessageFilter(packCode)
        dismiss()
    }
    /**
     * Filter channels interface
     */
    interface DialogFilterMessageListener {
        fun onMessageFilter(packCode: Int)
    }

    private lateinit var binding: DialogFilterMessageBinding
    private val messageList = ArrayList<Int>()
    private var chatRecyclerAdapter: KeyBoardChannelListAdapter? = null
    private var selectIdentity: SelectChannel? = null
    private var keyBoardAppsRepository: KeyBoardAppsRepository? = null
    private var isFilterMSG = false

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = Dialog(requireActivity(), R.style.DialogBlackTransparent)
        binding = DataBindingUtil.inflate(dialog.layoutInflater,R.layout.dialog_filter_message, null, false)
        dialog.setContentView(binding.root)
        selectIdentity = this
        keyBoardAppsRepository = KeyBoardAppsRepository(requireActivity())
        isFilterMSG = requireArguments().getBoolean("isfilter")
        if (isFilterMSG) {
            val packCodeList = keyBoardAppsRepository!!.getAllKeyboardAppTrayPackCode()
            for (i in packCodeList.indices) {
                when (val packCode = packCodeList[i]) {
                    0 ->{
                        //
                    }
                    30, 31 -> {
                        messageList.add(packCode)
                    }
                    else -> {
                        //
                    }
                }
            }
            messageList.add(40)
        } else {
            val packCodeList = keyBoardAppsRepository!!.getAllKeyboardAppTrayPackCode()
            for (i in packCodeList.indices) {
                when (val packCode = packCodeList[i]) {
                    0 ->{
                        //
                    }
                    30, 31 -> {
                        messageList.add(packCode)
                    }
                    else -> {
                        //
                    }
                }
            }
        }

        val layoutManager = LinearLayoutManager(activity, RecyclerView.HORIZONTAL, false)
        binding.channelsRecyclerViewDialog.layoutManager = layoutManager
        chatRecyclerAdapter = KeyBoardChannelListAdapter(messageList,
            selectIdentity as DialogMailFilterMessage
        )
        binding.channelsRecyclerViewDialog.adapter = chatRecyclerAdapter
        binding.closeFilterChannel.setOnClickListener { dialog.dismiss() }
        return dialog
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        dialogFilterMessageListener = activity as DialogFilterMessageListener?
    }

    companion object {
        var TAG = "DialogMailFilterMessage"
    }
}