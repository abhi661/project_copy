package com.android.centrl.utils

import android.widget.LinearLayout
import androidx.coordinatorlayout.widget.CoordinatorLayout
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.snackbar.Snackbar.SnackbarLayout

/**
 * Custom snackbar
 */
object CentrlSnackBar {
    fun showSnackBar(coordinatorLayout: CoordinatorLayout, message: String) {
        try {
            val snackbar = Snackbar.make(coordinatorLayout, message, Snackbar.LENGTH_LONG)
            val snackBarLayout = snackbar.view as SnackbarLayout
            for (i in 0 until snackBarLayout.childCount) {
                val parent = snackBarLayout.getChildAt(i)
                if (parent is LinearLayout) {
                    parent.rotation = 180f
                    break
                }
            }
            snackbar.show()
        } catch (e: Exception) {
        }
    }
}