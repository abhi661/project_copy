package com.android.centrl.notificationservice;

import android.content.Context;
import android.os.Build;
import android.service.notification.StatusBarNotification;

import androidx.annotation.RequiresApi;

import org.jetbrains.annotations.NotNull;

import static androidx.core.app.NotificationCompat.EXTRA_PROGRESS;
import static androidx.core.app.NotificationCompat.EXTRA_PROGRESS_MAX;

/**
 * Centrl notification catch service presenter
 */
public class NotificationCatchServicePresenterImpl implements NotificationCatchServicePresenter {


    private NotificationCatchServiceModel notificationCatchServiceModel;

    public NotificationCatchServicePresenterImpl(Context context) {
        notificationCatchServiceModel = new NotificationCatchServiceModel(context);
    }

    public boolean isNotiCancel(@NotNull String str) {
        return false;
    }

    /**
     * Parse notification
     */
    @RequiresApi(api = Build.VERSION_CODES.KITKAT_WATCH)
    public NotificationData parseNotification(@NotNull StatusBarNotification statusBarNotification, int packCode) {
        return this.notificationCatchServiceModel.getParserNotification(statusBarNotification, packCode);
    }

    /**
     * Check notification allowed to read in centrl
     */
    public boolean shouldInsert(@NotNull StatusBarNotification statusBarNotification) {
        return statusBarNotification.getNotification() != null && statusBarNotification.getNotification().extras != null && statusBarNotification.getNotification().extras.getInt(EXTRA_PROGRESS) == 0 && statusBarNotification.getNotification().extras.getInt(EXTRA_PROGRESS_MAX) == 0 && this.notificationCatchServiceModel.shouldInsertValidate(statusBarNotification.getPackageName());
    }
}
