package com.android.centrl.persistence.dao

import androidx.room.*
import com.android.centrl.persistence.entity.ChatBackupEntity
import io.reactivex.Completable
import io.reactivex.Flowable

/**
 * Data Access Object for the chat backup table.
 */
@Dao
interface ChatBackupDao {
    /**
     * Insert a backup details in the database.
     *
     * @param chatBackupEntity the backup details to be inserted.
     */
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertChatBackupDetails(chatBackupEntity: ChatBackupEntity): Completable

    /**
     * Update a backup details in the database.
     */
    @Query("UPDATE chatBackup SET lastBackupTime = :lastDate, userName = :userName, backupType = :mBackupTime, backupId = :newFileId, dbSize = :mDataBaseSize WHERE  emailID = :emailId")
    fun updateChatBackupDetails(lastDate: String, emailId: String, userName: String, mBackupTime: String, newFileId: String, mDataBaseSize: String): Completable
    /**
     * @return the chatBackup details from the table
     */
    @Query("SELECT * FROM chatBackup")
    fun getBackupDetails(): Flowable<ChatBackupEntity>

    /**
     * @return the chatBackup details from the table
     */
    @Query("SELECT * FROM chatBackup")
    fun getBackupsDetails(): ChatBackupEntity

    /**
     * Delete all backup details.
     */
    @Query("DELETE FROM chatBackup WHERE backupId = :fileId")
    fun deleteAllBackupDetails(fileId: String)

    /**
     * Insert a backup details in the database. If the backup details already exists, replace it.
     *
     * @param chatBackupEntity the backup details to be inserted.
     */
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertBackupDetails(chatBackupEntity: ChatBackupEntity)

    /**
     * Update a backup details in the database.
     */
    @Query("UPDATE chatBackup SET lastBackupTime = :lastDate, userName = :userName, backupType = :mBackupTime, backupId = :newFileId, dbSize = :mDataBaseSize WHERE  emailID = :emailId")
    fun updateChatBackupDetailsBG(lastDate: String, emailId: String, userName: String, mBackupTime: String, newFileId: String, mDataBaseSize: String)
}