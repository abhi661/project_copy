package com.android.centrl.ui.fragment

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.android.centrl.R
import com.android.centrl.databinding.FragmentPatternLockSecondBinding
import com.android.centrl.ui.activity.InteractionsActivity
import com.android.centrl.ui.views.patternlockview.PatternLockView
import com.android.centrl.ui.views.patternlockview.listener.PatternLockViewListener
import com.android.centrl.ui.views.patternlockview.utils.PatternLockUtils
import com.android.centrl.ui.views.patternlockview.utils.ResourceUtils
import com.android.centrl.utils.AppConstant

/**
 * Confirm pattern lock fragment
 */
class PatternLockSecondFragment : Fragment() {

    private lateinit var binding: FragmentPatternLockSecondBinding
    private var editor: SharedPreferences.Editor? = null
    private var mDrawPattern: String? = null
    /**
     * Pattern lock listener
     */
    private val mPatternLockViewListener: PatternLockViewListener = object : PatternLockViewListener {
            override fun onStarted() {}
            override fun onProgress(progressPattern: List<PatternLockView.Dot>) {}
            @RequiresApi(Build.VERSION_CODES.M)
            override fun onComplete(pattern: List<PatternLockView.Dot>) {
                mDrawPattern = PatternLockUtils.patternToString(binding.patterLockViewSec, pattern)
                if (mDrawPattern.equals(mFirstDrawPattern, ignoreCase = true)) {
                    binding.pattenChangeTxtSec.text = resources.getString(R.string.new_pattern)
                    binding.pattenChangeTxtSec.setTextColor(resources.getColor(R.color.security_text, null))
                    binding.patternConfirm.setBackgroundResource(R.drawable.allow_btn_bg)
                } else {
                    binding.pattenChangeTxtSec.text = resources.getString(R.string.pattern_not_match)
                    binding.pattenChangeTxtSec.setTextColor(resources.getColor(R.color.security_error, null))
                    binding.patternConfirm.setBackgroundResource(R.drawable.disallow_btn_bg)
                    binding.patterLockViewSec.clearPattern()
                }
            }

            override fun onCleared() {
                binding.patternConfirm.setBackgroundResource(R.drawable.disallow_btn_bg)
            }
        }

    @SuppressLint("CommitPrefEdits")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
       val preferences = requireActivity().getSharedPreferences(AppConstant.PREF_NAME, Context.MODE_PRIVATE)
        editor = preferences!!.edit()
        if (arguments != null) {
            mFirstDrawPattern = requireArguments().getString(ARG_PARAM1)
            isRestPattern = requireArguments().getBoolean(ARG_PARAM2)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_pattern_lock_second, container, false)
        val view = binding.root

        binding.patterLockViewSec.dotCount = 3
        binding.patterLockViewSec.dotNormalSize = ResourceUtils.getDimensionInPx(
            requireActivity(),
            R.dimen.pattern_lock_dot_size
        ).toInt()
        binding.patterLockViewSec.dotSelectedSize = ResourceUtils.getDimensionInPx(
            requireActivity(),
            R.dimen.pattern_lock_dot_selected_size
        ).toInt()
        binding.patterLockViewSec.pathWidth = ResourceUtils.getDimensionInPx(
            requireActivity(),
            R.dimen.pattern_lock_path_width
        ).toInt()
        binding.patterLockViewSec.isAspectRatioEnabled = true
        binding.patterLockViewSec.aspectRatio = PatternLockView.AspectRatio.ASPECT_RATIO_HEIGHT_BIAS
        binding.patterLockViewSec.setViewMode(PatternLockView.PatternViewMode.CORRECT)
        binding.patterLockViewSec.dotAnimationDuration = 150
        binding.patterLockViewSec.pathEndAnimationDuration = 100
        binding.patterLockViewSec.correctStateColor = ResourceUtils.getColor(requireActivity(), R.color.pattern_drawing)
        binding.patterLockViewSec.isInStealthMode = false
        binding.patterLockViewSec.isTactileFeedbackEnabled = true
        binding.patterLockViewSec.isInputEnabled = true
        binding.patterLockViewSec.addPatternLockListener(mPatternLockViewListener)

        binding.patternCancelSec.setOnClickListener { requireActivity().finish() }
        binding.patternConfirm.setOnClickListener {
            if (mDrawPattern != null && mDrawPattern!!.isNotEmpty()) {
                editor!!.putString(AppConstant.PATTERN_LOCK, mDrawPattern)
                editor!!.putInt(AppConstant.LOCK_TYPE, AppConstant.PATTERN_LOCK_CODE)
                editor!!.commit()
                if(isRestPattern){
                    val intent = Intent(requireActivity(), InteractionsActivity::class.java)
                    startActivity(intent)
                }
                requireActivity().finish()
            }
        }
        return view
    }

    companion object {
        private const val ARG_PARAM1 = "param1"
        private const val ARG_PARAM2 = "param2"
        private var mFirstDrawPattern: String? = null
        private var isRestPattern: Boolean = false
        fun newInstance(pattern: String?, resetPattern: Boolean): PatternLockSecondFragment {
            mFirstDrawPattern = pattern
            isRestPattern = resetPattern
            val fragment = PatternLockSecondFragment()
            val args = Bundle()
            args.putString(ARG_PARAM1, pattern)
            args.putBoolean(ARG_PARAM2, resetPattern)
            fragment.arguments = args
            return fragment
        }
    }
}