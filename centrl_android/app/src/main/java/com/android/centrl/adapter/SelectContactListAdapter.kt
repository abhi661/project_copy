package com.android.centrl.adapter

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.android.centrl.R
import com.android.centrl.persistence.entity.ChatsEntity
import com.android.centrl.utils.CentrlUtil
import java.util.*

/**
 * Select contact to block adapter
 */
class SelectContactListAdapter(context: Context, arrayList: ArrayList<ChatsEntity>?, filterSearchInterface: FilterSearchInterface) : RecyclerView.Adapter<SelectContactListAdapter.ChatAppMsgViewHolder>(),
    Filterable {
    private val mFilterSearchInterface: FilterSearchInterface
    var chatArrayList: ArrayList<ChatsEntity>? = null
    private var chatArrayListCopy: ArrayList<ChatsEntity>
    var context: Context
    private var isTxtEmpty = false

    init {
        chatArrayList = arrayList
        chatArrayListCopy = arrayList!!
        this.context = context
        mFilterSearchInterface = filterSearchInterface
    }
    // Inflates the appropriate layout according to the ViewType.
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ChatAppMsgViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val view = layoutInflater.inflate(R.layout.select_contact_list_item, parent, false)
        return ChatAppMsgViewHolder(view)
    }
    // Passes the message object to a ViewHolder so that the contents can be bound to UI.
    override fun onBindViewHolder(holder: ChatAppMsgViewHolder, position: Int) {
        try {
            val chat = chatArrayList!![position]
            // If the message is a received message.
            holder.chatTitleTxt!!.text = chat.groupTitle
            if(chat.groupIconPath!!.isEmpty()){
                holder.profileImageView!!.setImageResource(R.drawable.default_user_image)
            } else {
                val profileImage: Bitmap = BitmapFactory.decodeFile(chat.groupIconPath)
                holder.profileImageView!!.setImageBitmap(profileImage)
            }
            if(chat.packageCode != 0){
                holder.chatTypeImage!!.setBackgroundResource(CentrlUtil.getChannelsLogo()[chat.packageCode]!!)
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun getItemCount(): Int {
        return chatArrayList!!.size
    }

    class ChatAppMsgViewHolder(itemView: View?) : RecyclerView.ViewHolder(itemView!!) {
        var chatTitleTxt: TextView? = null
        var profileImageView: ImageView? = null
        var chatTypeImage: ImageView? = null

        init {
            if (itemView != null) {
                chatTitleTxt = itemView.findViewById<View>(R.id.chat_title_text_view_sc) as TextView
                profileImageView = itemView.findViewById<View>(R.id.chat_profile_image_sc) as ImageView
                chatTypeImage = itemView.findViewById<View>(R.id.chat_type_view_sc) as ImageView
            }
        }
    }
    /**
     * This will update the list
     */
    fun updateList(chatList: ArrayList<ChatsEntity>?) {
        chatArrayList = chatList
        chatArrayListCopy = chatList!!
        //now, tell the adapter about the update
        notifyDataSetChanged()
    }
    /**
     * Search filter interface
     */
    interface FilterSearchInterface {
        fun filterResult(size: Int)
    }

    /**
     * This is use to search particular name
     */
    override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(charSequence: CharSequence): FilterResults {
                val charString = charSequence.toString()
                if (charString.isEmpty()) {
                    isTxtEmpty = true
                    chatArrayList = chatArrayListCopy
                } else {
                    isTxtEmpty = false
                    val filteredList = ArrayList<ChatsEntity>()
                    for (row in chatArrayListCopy) {
                        if (row.groupTitle!!.toLowerCase(Locale.getDefault()).contains(charString.toLowerCase(Locale.getDefault()))) {
                            filteredList.add(row)
                        }
                    }
                    chatArrayList = filteredList
                }
                val filterResults = FilterResults()
                filterResults.values = chatArrayList
                return filterResults
            }

            override fun publishResults(charSequence: CharSequence, filterResults: FilterResults) {
                chatArrayList = filterResults.values as ArrayList<ChatsEntity>
                if (isTxtEmpty) {
                    mFilterSearchInterface.filterResult(-1)
                } else {
                    mFilterSearchInterface.filterResult(chatArrayList!!.size)
                }
                notifyDataSetChanged()
            }
        }
    }
}