package com.android.centrl.utils

import android.content.Context
import android.os.Environment
import java.io.File
import java.io.FileOutputStream
import java.io.IOException

/**
 * Data base back and import
 */
object DataBaseExportImport {
    val TAG = DataBaseExportImport::class.java.name

    /**
     * Replaces current database with the IMPORT_FILE if
     * import database is valid and of the correct type
     */
    fun restoreDb(context: Context?): Boolean {
        if (!sdIsPresent()) return false
        val exportFile = context!!.getDatabasePath(AppConstant.DATA_BASE)
        val importFilePath = File(CentrlUtil.getRootDirPath(context), AppConstant.DATA_BASE)
        if (!importFilePath.exists()) {
            return false
        }
        return try {
            exportFile.createNewFile()
            copyDataFromOneToAnother(importFilePath.absolutePath, exportFile.absolutePath)
            true
        } catch (e: IOException) {
            e.printStackTrace()
            false
        }
    }

    private fun copyDataFromOneToAnother(fromPath: String, toPath: String) {
        val inStream = File(fromPath).inputStream()
        val outStream = FileOutputStream(toPath)

        inStream.use { input ->
            outStream.use { output ->
                input.copyTo(output)
            }
        }
    }
    /**
     * Returns whether an SD card is present and writable
     */
    private fun sdIsPresent(): Boolean {
        return Environment.getExternalStorageState() == Environment.MEDIA_MOUNTED
    }
}